const insert = require('../mysqlUtils/insert')

exports.handler = async (error, values) => {
  try {
    const parsed = error
    await insert({
      db: process.env.DB_NAME,
      IDENTICORP_DB: process.env.IDENTICORP_DB,
      table: 'error',
      values: {
        user: values.username,
        code: parsed.code,
        extracted_values: JSON.stringify(values),
        error: JSON.stringify(e.message)
      }
    })
  } catch (e) {
    console.error(e)
  }
}
