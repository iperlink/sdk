module.exports = (previous, className) => {
  const els = document.getElementsByClassName(previous)
  if (els && els.length > 0) {
    for (let i = 0; i < els.length; i++) {
      const el = els[i]
      el.setAttribute('class', className)
    }
  }

}