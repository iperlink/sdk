module.exports = (id, className) => {
  const els = document.getElementById(id).childNodes
  for (let i = 0; i < els.length; i++) {
    const el = els[i]
    const currentClass = el.getAttribute('class')
    if (currentClass.indexOf(className) === -1) {
      const newClass = ` ${currentClass} ${className}`
      el.setAttribute('class', newClass)
    }
    else {
      const splitted = currentClass.split(className)
      const newClass = splitted[0]
      el.setAttribute('class', newClass)
    }
  }
}