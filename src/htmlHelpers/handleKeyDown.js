module.exports = ({ k, options, ulId, selected, elClass }) => {
  const { keyCode } = k
  const ul = document.getElementById(ulId)
  const elems = ul.getElementsByTagName('li')
  const L = elems.length
  for (let i = 0; i < L; i++) {
    elems[i].setAttribute('class', elClass)
  }
  switch (keyCode) {
    case 38:
      if (selected > 0) {
        selected -= 1
        elems[selected].setAttribute('class', `${elClass} typeAheadSelected`)
        return selected
      }
      break

    case 40:
      if (selected !== L - 1) {
        selected += 1
        elems[selected].setAttribute('class', `${elClass} typeAheadSelected`)
        return selected
      }
    case 13:
      k.preventDefault()
      return {
        value: options[selected],
        selected,
      }
    default:
      return selected
  }


}