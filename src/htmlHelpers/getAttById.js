module.exports = ({ id, att }) => {
  if (!document.getElementById(id)) {
    throw new Error(`element ${id} is not defined in DOM`)
  }
  const el = document.getElementById(id)[att]
  return el
}