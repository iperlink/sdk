module.exports = (id, className, remove) => {
  const el = document.getElementById(id)
  if (!el) {
    throw new Error(`element with id ${id} does not exist`)
  }
  const currentClass = el.getAttribute('class') || ''
  if (currentClass.indexOf(className) === -1) {
    const replaced = currentClass.replace(remove, '')
    const newClass = ` ${replaced} ${className}`
    el.setAttribute('class', newClass)
  } else {
    const replaced = currentClass.replace(className, '')
    const newClass = ` ${replaced} ${remove}`
    el.setAttribute('class', newClass)
  }
}