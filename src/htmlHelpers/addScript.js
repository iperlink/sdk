const byId = require('./byId')

module.exports = ({ src, id }) => {
  const body = document.getElementsByTagName('body')[0]
  const already = byId(id)
  if (already) return
  const script = document.createElement('script')
  script.src = src
  script.id = id
  body.appendChild(script)
}