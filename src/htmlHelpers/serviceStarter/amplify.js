const Amplify = require('aws-amplify').default

module.exports = (config) => {
  const {
    REGION,
    userPoolId,
    userPoolWebClientId,
    GRAPHQL_URL,
    AUTHENTICATION_TYPE
  } = config.AWS
  Amplify.configure({
    Auth: {
      region: REGION,
      userPoolId,
      userPoolWebClientId,
    },
    aws_appsync_graphqlEndpoint: GRAPHQL_URL,
    aws_appsync_region: REGION,
    aws_appsync_authenticationType: AUTHENTICATION_TYPE
  })
}