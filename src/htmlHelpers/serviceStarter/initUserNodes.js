const keyBy = require('../../objMapper/keyBy')

module.exports = () => {
  const user = localStorage.getItem('userData')
  const parsed = JSON.parse(user)
  const { nodes } = parsed
  USER_NODES = JSON.parse(nodes)
}
