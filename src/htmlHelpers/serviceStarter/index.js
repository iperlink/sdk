const amplify = require('./amplify')
const devError = require('../../objMapper/devError')
const initUserNodes = require('./initUserNodes')
const initConfig = require('./initConfig')

module.exports = async (client_config) => {
  try {
    amplify(client_config)
    initUserNodes()
    initConfig(client_config);
  } catch (e) {
    devError(e.message)
  }
}

