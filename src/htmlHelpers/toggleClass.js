const replace = (previous, className, run) => {
  const els = document.getElementsByClassName(previous)
  const previousels = document.getElementsByClassName(className)
  const replaceArr = (els && els.length === 0)
    ? previousels
    : els
  const setClass = (els && els.length === 0)
    ? previous
    : className
  for (let i = 0; i < replaceArr.length; i++) {
    const el = replaceArr[i]
    el.setAttribute('class', setClass)
  }
}

module.exports = replace