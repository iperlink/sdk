module.exports = (id, className) => {
  const el = document.getElementById(id)
  if (!el) {
    throw new Error(`element with id ${id} does not exist`)
  }
  el.setAttribute('class', className)
}