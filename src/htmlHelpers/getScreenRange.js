const ranges = require('./screenRanges')

const getInRange = (width) => {
  let res = ''
  ranges.forEach(range => {
    if (range.bottom <= width && range.top >= width) {
      res = range.name
    }
  })
  return res
}

module.exports = (id = 'app') => {
  const app = document.getElementById(id)
  const width = app.clientWidth
  const inRange = getInRange(width)
  return inRange
}
