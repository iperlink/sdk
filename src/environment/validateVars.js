module.exports = vars => {
  vars.forEach(v => {
    if (!process.env[v]) {
      throw new Error(JSON.stringify({
        code: 'envVarNotDefined',
        message: `${v} is not defined in env`
      }))
    }
  })
}
