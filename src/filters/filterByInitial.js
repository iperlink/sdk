module.exports = ({ options, value }) => {
  const L = value.length
  const filtered = options.filter(x => {
    return x.substr(0, L).toLowerCase() === value.toLowerCase()
  })
  return filtered
}