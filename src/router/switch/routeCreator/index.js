const childRoutes = require('./childRoutes')
const masterRoute = require('./masterRoute')

module.exports = () => {
  const masterR = masterRoute()
  const childR = childRoutes()
  const concated = [masterR].concat(childR)

  return concated
}