const createElement = require('../../../renderer')
const { Route } = require('react-router-dom')
const componentConnector = require('./childRoutes/foundRoute/componentConnector')

module.exports = () => {
  const {
    master,
  } = CLIENT_CONFIG
  const {
    path,
    component
  } = master
  const result = createElement({
    type: Route,
    props: {
      id: path,
      path: path,
      exact: true,
      component: componentConnector(component)
    },

  })
  return result
}