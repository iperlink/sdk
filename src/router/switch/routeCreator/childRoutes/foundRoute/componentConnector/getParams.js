
module.exports = (fromRouter) => {
  if (fromRouter && fromRouter.match && fromRouter.match.params) {
    return fromRouter.match.params
  }
  return {}
}