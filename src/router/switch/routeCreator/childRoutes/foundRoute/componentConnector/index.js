const getParams = require('./getParams')
const connect = require('../../../../../../apolloPreprocessor/connect')

module.exports = (component) => {
  const {
    type,
    props,
  } = component
  const rendered = (fromRouter) => {
    const params = getParams(fromRouter)
    const concated = {
      ...params,
      ...props,
      type
    }
    if (props.queryName) {
      const connected = connect(concated)
      return connected
    }
    return null
  }
  return rendered
}