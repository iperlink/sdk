const createElement = require('../../../../../renderer')
const { Route } = require('react-router-dom')
const componentConnector = require('./componentConnector')

module.exports = (route) => {
  const {
    path,
    component,
  } = route
  const connected = componentConnector(component)
  const found = createElement({
    type: Route,
    props: {
      id: path,
      path,
      component: connected,
    },
  })
  return found
}