const foundRoute = require('./foundRoute')

module.exports = () => {
  const {
    childs,
  } = CLIENT_CONFIG
  const createdRoutes = childs.map(route => {
    return foundRoute(route)
  })
  return createdRoutes
}