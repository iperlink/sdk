const { Switch } = require('react-router-dom')
const createElement = require('../../renderer')
const routeCreator = require('./routeCreator')

module.exports = () => {
  const r = routeCreator()
  const switched = createElement({
    type: Switch,
    props: { id: 'switch' },
    children: r
  })
  return switched
}

