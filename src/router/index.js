const WithAppsyncProvider = require('../appSync/withProvider')
const createElement = require('../renderer')
const { BrowserRouter } = require('react-router-dom')
const switcher = require('./switch')
const overlay = require('../reactComponents/overlay')
const sideMenu = require('../reactComponents/sideMenu')

module.exports = token => {
  const switched = switcher()
  const Menu = sideMenu()
  const Overlay = overlay()
  const main = createElement({
    type: WithAppsyncProvider,
    props: { token, id: 'mainRoute' },
    children: [Overlay, switched, Menu]
  })
  const router = createElement({
    type: BrowserRouter,
    props: { id: 'router' },
    children: main,
  })
  return router
}
