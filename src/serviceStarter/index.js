const serviceStarter = require('./helpers/serviceStarter')
let services = {}
let started = false
const AWS = require('aws-sdk')

exports.start = async ({ servicesList, dependencies, fromTest }) => {
  if (process.env.resetServices) services = {}
  if (!fromTest) {
    console.info('included services', servicesList)
  }
  await serviceStarter({ serviceArray: servicesList, services, dependencies })
  if (Object.values(services).indexOf('false') !== -1) return false
  started = true
  return true
}

exports.getServices = () => {
  if (!started) {
    throw new Error(`
    services where not started,
    Must call serviceStarter
    `)
  }
  return services
}
