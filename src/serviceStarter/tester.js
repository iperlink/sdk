const { start } = require('./index')

module.exports = async (params) => {
  const { views, findTemplates } = params;
  const dependencies = {
    AWS: require('aws-sdk'),
    MongoClient: require('mongodb').MongoClient,
    Redis: require('ioredis'),
    elasticsearch: require('elasticsearch'),
    mysql: require('mysql2/promise'),
    nodemailer: require('nodemailer'),
    views,
    findTemplates,
  }

  const servicesList = [
    'cognito',
    'findTemplates',
    'lambda',
    'mysql',
    'mongo',
    'redis',
    's3',
    'secrets',
    'ses',
    'sns',
    'views',
  ]
  await start({ servicesList, dependencies, fromTest: true })
}
