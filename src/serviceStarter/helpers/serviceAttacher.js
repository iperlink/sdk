const serviceProviders = require('../serviceProviders')

module.exports = async ({ key, services, dependencies }) => {
  const service = key.service || key
  const serviceName = key.name || key
  if (!serviceProviders[service]) throw new Error(`service ${service} is not defined check serviceProviders`)
  const result = await serviceProviders[service](dependencies)
  services[serviceName] = result
}