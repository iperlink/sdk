const serviceAtacher = require('./serviceAttacher')

module.exports = async ({ serviceArray, services, dependencies }) => {
  if (dependencies.AWS) {
    dependencies.AWS.config.region = process.env.REGION
  }
  const promiseArray = serviceArray.map((key) => {
    return serviceAtacher({ key, services, dependencies })
  })
  await Promise.all(promiseArray)
}
