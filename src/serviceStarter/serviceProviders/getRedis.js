const createRedisObj = (string) => {
  const splitted = string.split('|')
  const result = {
    host: splitted[1],
    password: splitted[2],
    retryStrategy: function () {
      const delay = Math.min(1 * 50, 2000)
      return delay
    },
    port: splitted[3],
    maxRetriesPerRequest: 2
  }

  return result
}

module.exports = async ({ Redis }) => {
  if (global.cachedRedis) {
    console.info('=> cached redis')
    return global.cachedRedis
  }
  const redisObj = process.env.REDIS_CONN
    ? createRedisObj(process.env.REDIS_CONN)
    : {
      host: 'localhost',
      retryStrategy: function () {
        const delay = Math.min(1 * 50, 2000)
        return delay
      },
      maxRetriesPerRequest: 2
    }
  const host = process.env.REDIS_CONN
    ? process.env.REDIS_CONN.split('|')[0]
    : 'localhost'
  console.info('connecting to redis @ ', host)
  const result = new Redis(redisObj)
  global.cachedRedis = result
  return result
}
