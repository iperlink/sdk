
module.exports = async ({ MongoClient }) => {
  const dbName = process.env.DB_NAME
  if (global.cachedDb && global.cachedDb.db(dbName).serverConfig.isConnected()) {
    console.info('=> cached connection')
    return global.cachedDb.db(dbName)
  }

  console.info('=> connecting to mongo database')
  const uri = process.env.MONGO_URL
  const db = await MongoClient.connect(uri, { useNewUrlParser: true })
  global.cachedDb = db
  return db.db(dbName)
}
