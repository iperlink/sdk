
module.exports = async ({ elasticsearch }) => {
  return new elasticsearch.Client({
    host: [
      {
        host: process.env.ELASTICSEARCH_URL,
        auth: `${process.env.ELASTICSEARCH_USER}:${process.env.ELASTICSEARCH_PASSWORD}`,
        protocol: 'https',
        port: 9243
      }
    ]

  })
}
