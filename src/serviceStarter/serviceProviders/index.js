const getElasticSearch = require('./getElasticSearch')
const getRedis = require('./getRedis')
const getMongo = require('./getMongo')
const getmailer = require('./getmailer')
const getMysql = require('./getMysql')
const getSecretManager = require('./getSecretManager')
const getViews = require('./getViews')
const getFindTemplates = require('./getFindTemplates')


module.exports = {
  awsCreds: ({ AWS }) => new AWS.EnvironmentCredentials('AWS'),
  s3: ({ AWS }) => new AWS.S3(),
  lambda: ({ AWS }) => new AWS.Lambda(),
  cognito: ({ AWS }) => {
    AWS.config.update({
      accessKeyId: 'AKIAJOXC5N4Y6ODAKQJA',
      secretAccessKey: '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',
      region: 'us-east-1',
    })
    return new AWS.CognitoIdentityServiceProvider({ region: process.env.AWS_REGION })
  },
  elasticSearchEndpoint: getElasticSearch,
  mongo: getMongo,
  mysql: getMysql,
  redis: getRedis,
  secrets: getSecretManager,
  ses: getmailer,
  views: getViews,
  findTemplates: getFindTemplates,
  sns: ({ AWS }) => new AWS.SNS({ region: process.env.AWS_REGION })
}
