
module.exports = ({ views }) => {
  if (global.views) return global.views;
  if (!views) return {};
  global.views = views;
  return views;
};
