const get = require('../../secret/get')

module.exports = async ({ mysql }) => {
  if (global.cachedMysql) {
    console.info('=> cached mysql connection')
    return global.cachedMysql
  }
  const account = process.env.ACCOUNT
  if (!account) {
    throw new Error(JSON.stringify({
      code: 'noMysqAccount',
      message: 'must configure account name to connect to mysql'
    }))
  }
  const params = (account !== 'local')
    ? JSON.parse(await get(`${account}_mysql`))
    : {
      host: process.env.MYSQL_HOST,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
    }
  const {
    host,
    password,
    user
  } = params
  console.info('connecting to mysql @', host)
  const connection = await mysql.createConnection({
    host,
    password,
    user
  })
  global.cachedMysql = connection
  return connection
}
