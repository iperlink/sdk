
module.exports = ({ AWS }) => {
  return new AWS.SecretsManager({
    endpoint: process.env.SECRETS_ENDPOINT,
    region: process.env.REGION
  })
}
