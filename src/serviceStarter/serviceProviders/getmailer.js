module.exports = ({ nodemailer, AWS }) => {
  const transporter = nodemailer.createTransport({
    SES: new AWS.SES({ region: 'us-east-1' })
  })
  return transporter
}
