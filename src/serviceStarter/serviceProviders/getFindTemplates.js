
module.exports = ({ findTemplates }) => {
  if (global.findTemplates) return global.findTemplates;
  if (!findTemplates) return {};
  global.findTemplates = findTemplates;
  return findTemplates;
};
