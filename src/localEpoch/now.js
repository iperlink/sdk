const moment = require('moment-timezone')

module.exports = () => {
  return Math.floor(moment().valueOf() / 1000)
}