
module.exports = ({ date, time }) => {
  const newDateTime = new Date(`${date}T${time}:00Z`).getTime() / 1000
  return newDateTime
}