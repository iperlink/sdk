const moment = require('moment-timezone')

module.exports = (date) => {
  if (date) {
    const valid = moment(date).isValid()
    if (!valid) throw new Error(JSON.stringify({
      code: 'invaidDate',
      message: `suplyed date is invalid ${date}`
    }))
  }
  return Math.floor(moment(date).startOf('day').valueOf() / 1000)
}