const moment = require('moment-timezone')

module.exports = (time) => {
  const splittedTime = time.split(':')
  const mom = moment('1970-01-01').startOf('day')
  mom.set({
    'hour': splittedTime[0],
    'minute': splittedTime[1]
  })
  return Math.floor(mom.valueOf() / 1000)
}