
module.exports = (data, error) => {
  const statusCode = error
    ? 400
    : 200
  const allow = process.env.ALLOW_ORIGIN || '*'
  const response = {
    statusCode,
    headers: {
      'Access-Control-Allow-Origin': allow,
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify(data),
  };
  return response
}