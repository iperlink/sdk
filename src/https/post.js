const https = require('https');

module.exports = () => {
  return new Promise((resolve, reject) => {
    https.post('https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY', (resp) => {
      let data = '';

      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        data += chunk;
      });

      // The whole response has been received. Print out the result.
      resp.on('end', () => {
        console.info(JSON.parse(data).explanation);
      });

    }).on("error", (err) => {
      console.error("Error: " + err.message);
    })
  })
}
