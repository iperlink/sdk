module.exports = (string) => {
  return string.normalize('NFD').replace(/ /g, '').replace(/[\u0300-\u036f]/g, '').toUpperCase()
}
