const createElement = require('../../renderer')
const icons = require('../icons')


module.exports = ({ className, id, onClick, message, icon, submit, disabled }) => {
  const Message = message || 'enviar'
  const type = submit
    ? 'submit'
    : 'button'
  const content = icon
    ? icons({ name: icon })
    : Message
  const btn = createElement({
    type: 'button',
    props: {
      className: `${className}`,
      id,
      type,
      onClick,
      disabled,
    },
    children: content
  })

  const btnContainer = createElement({
    props: {
      className: 'btnContainer btnTooltip',
      id: `${id}_btn_container`,
    },
    children: btn
  })
  return btnContainer
}
