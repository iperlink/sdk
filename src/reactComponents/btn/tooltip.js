const createElement = require('../../renderer')

module.exports = (props) => {
  const span = createElement({
    type: 'div',
    props: {
      className: 'tooltip',
      id: props.id
    },
    children: props.message
  })
  return span
}