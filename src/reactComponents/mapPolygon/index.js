const { Component } = require('react')
const createElement = require('../../renderer')
const initMap = require('./initMap')
const button = require('../../reactComponents/btn')

const mapPolygon = class mapPolygon extends Component {
  state = {
    polygon: {},
    showMap: false,
  }

  componentDidMount = async () => {
    const {
      mapData
    } = this.props
    console.log(mapData)
    initMap(mapData)
  }


  render() {
    const btn = button({
      className: 'editBtn',
      id: `back`,
      message: 'Volver',
      onClick: () => {
        this.props.changeComp()
      }
    })
    const titleDiv = createElement({
      type: 'div',
      props: {
        id: 'mapPointTitle',
        className: 'formTitle',
      },
      children: `Mision: ${this.props.mission}`
    })
    const areaDiv = createElement({
      type: 'div',
      props: {
        id: 'mapPointTitleArea',
        className: 'formTitle',
      },
      children: `Area: ${this.props.area}`
    })
    const mapEl = createElement({
      props: {
        id: 'map'
      }
    })
    const container = createElement({
      type: 'div',
      props: {
        id: `mapPointContainer`,
      },
      children: [titleDiv, areaDiv, mapEl, btn]
    })
    return container
  }
}

module.exports = (props) => {
  console.log(props)
  const result = createElement({
    type: mapPolygon,
    props: {
      ...props,
      id: 'mapPolygon'
    }
  })
  return result
}