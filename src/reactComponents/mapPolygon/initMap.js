
module.exports = (mapInitialCords) => {
  const {
    cords,
    zoom,
    center,
  } = JSON.parse(mapInitialCords)
  console.log(center)
  map = new google.maps.Map(document.getElementById('map'), {
    center,
    zoom,
    disableDefaultUI: true
  });
  map.data.add({
    geometry: new google.maps.Data.Polygon([cords])
  })
  map.data.setStyle({
    editable: false,
    draggable: false
  });
}
