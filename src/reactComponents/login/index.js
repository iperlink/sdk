const login = require('../../identicorp/cognito/login/login')
const form = require('../../reactComponents/formCreator')
const createElement = require('../../renderer')
const button = require('../../reactComponents/btn')
const resetPAssword = require('../../identicorp/cognito/changePassword')

module.exports = ({ onSuccess, onFail, addReset }) => {
  const onSubmit = async response => {
    await login(response, onSuccess, onFail)
  }
  const fields = {
    username: {
      type: 'text',
    },
    password: {
      type: 'password'
    },
  }

  const Form = form({
    className: 'formCentered',
    fields: fields,
    id: 'loginForm',
    message: 'send',
    onSubmit
  })
  const reset = button({
    className: 'editBtn',
    id: 'passwordReset',
    message: 'Resetear la password',
    // onClick: () => key.fn(data)
  })
  const resetText = createElement({
    props: { id: 'resetTextDiv' },
    children: 'Deseas Resetear la password'
  })
  const resetDiv = createElement({
    props: { id: 'resetPassDiv' },
    children: [resetText, reset]
  })
  const formDiv = createElement({
    props: { id: 'loginFormDiv' },
    children: Form
  })
  const formWithReset = addReset
    ? [formDiv, reset]
    : formDiv
  const div = createElement({
    props: { id: 'loginFormCont' },
    children: formWithReset
  })
  return div
}
