const icons = require('react-icons/fa')
const createElement = require('../../renderer')

module.exports = ({ name, size }) => {
  const iconFn = icons[name]
  if (!iconFn) {
    throw new Error(`Icon ${name} is not defined`)
  }
  const iconEl = iconFn(size)

  return createElement({
    type: 'div',
    props: {
      id: `${name}_icon`,
      className: 'iconCont',
    },
    children: iconEl
  })
}