const { Component } = require('react')
const createElement = require('../../renderer')
const componentRenderer = require('../index')

const Connector = class Connector extends Component {
  state = {
    component: this.props.type,
    componentProps: null
  }

  shouldComponentUpdate(nextProps) {
    return true
  }

  changeComp = (newComp) => {
    console.log(newComp)
    const componentProps = newComp
      ? newComp.newProps
      : this.props
    const component = newComp
      ? newComp.type
      : this.props.type
    this.setState({
      component,
      componentProps
    })
  }

  render() {
    const { component } = this.state
    const currentComponent = componentRenderer(component)
    const componentProps = this.state.componentProps || this.props
    const propsWithChange = {
      ...componentProps,
      changeComp: this.changeComp
    }
    const result = currentComponent(propsWithChange)
    return result
  }
}

module.exports = (props) => {
  const { mutationNames } = props
  const mutations = mutationNames.reduce((acc, curr) => {
    acc[curr] = props[curr]
    return acc
  }, {})
  const { data } = props
  const newProps = {
    ...props,
    data: {
      ...data,
      ...mutations
    }
  }
  return createElement({
    type: Connector,
    props: newProps
  })
}