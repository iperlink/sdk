const sweetalert = require('sweetalert')

module.exports = (error) => {
  if (error) {
    const { errors } = dataSourceTemplate
    try {
      const parsed = JSON.parse(error.message)
      const { code } = parsed
      sweetalert(errors[code])
    } catch (e) {
      console.log(error)
      const msg = errors[error.message] || errors.generic
      sweetalert(msg)
    }
  }
}