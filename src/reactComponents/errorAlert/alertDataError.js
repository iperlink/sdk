const errorAlert = require('./index')

module.exports = (data) => {
  const errorContainer = Array.isArray(data)
    ? data[0]
    : data
  console.log({ errorContainer })
  if (errorContainer.error) {
    errorAlert(errorContainer.error)
  }
}