const createElement = require('../../renderer')

module.exports = ({ id, className, children }) => {
  const classN = className || 'simpleDiv'
  const container = createElement({
    type: 'div',
    props: {
      id,
      className: classN,
    },
    children,
  })
  return container
}
