const byId = require('../../htmlHelpers/byId')

module.exports = ({ src }) => {
    const exists = byId(src)
    if(exists) return
    const script = document.createElement("script")
    script.src = src
    script.id = src
    document.body.appendChild(script)
    return
}

