
module.exports = (mapInitialCords) => {
  const {
    zoom,
    cords,
  } = JSON.parse(mapInitialCords)
  map = new google.maps.Map(document.getElementById('map'), {
    center: cords,
    zoom,
    disableDefaultUI: true
  });
  new google.maps.Marker({
    position: cords,
    map: map,
    title: 'Hello World!'
  });

}
