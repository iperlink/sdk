const { Component } = require('react')
const createElement = require('../../renderer')
const initMap = require('./initMap')
const button = require('../../reactComponents/btn')

const KmlPoint = class KmlPoint extends Component {
  componentDidMount = async () => {
    const {
      mapData
    } = this.props
    initMap(mapData)
  }


  render() {
    const btn = button({
      className: 'editBtn',
      id: `back`,
      message: 'Volver',
      onClick: () => {
        this.props.changeComp()
      }
    })
    const titleDiv = createElement({
      type: 'div',
      props: {
        id: 'mapPointTitle',
        className: 'formTitle',
      },
      children: `Mision: ${this.props.mission}`
    })
    const areaDiv = createElement({
      type: 'div',
      props: {
        id: 'mapPointTitle',
        className: 'formTitle',
      },
      children: `Area: ${this.props.area}`
    })
    const mapEl = createElement({
      props: {
        id: 'map'
      }
    })
    const container = createElement({
      type: 'div',
      props: {
        id: `mapPointContainer`,
      },
      children: [titleDiv, areaDiv, mapEl, btn]
    })
    return container
  }
}

module.exports = (props) => {
  return createElement({
    type: KmlPoint,
    props: {
      ...props,
      id: 'mapPoint'
    }
  })
}