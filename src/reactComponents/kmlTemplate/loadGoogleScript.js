const addScript = require('../../htmlHelpers/addScript')

module.exports = () => {
  const src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAgQwOq-2uyPRnX_402FJ2P4nW-1lesB-o&libraries=drawing&callback=initMap'
  addScript({ src: '/kmlTemplate.js', id: 'kmlTemplate' })
  addScript({ src, id: 'googleMapJs' })
}