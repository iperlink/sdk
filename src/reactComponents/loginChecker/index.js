const loginChecker = require('../../identicorp/cognito/login/checkLogin')
const loginForm = require('../login')
const { Component } = require('react')
const createElement = require('../../renderer')
const router = require('../../router')
const errorAlert = require('../errorAlert')

const LoginChecker = class LoginChecker extends Component {
  state = {
    token: false,
    failed: false
  }

  handleSucess = (token) => {
    this.setState({ token: token })
  }

  componentWillMount = async () => {
    const endpoint = this.props.endpoint
    const token = await loginChecker(endpoint)
    this.setState({
      token
    })
  }

  render() {
    const { token } = this.state
    if (token) {
      return router(token)
    } else {

      const onSuccess = (token) => { this.handleSucess(token) }
      const onFail = () => {
        this.setState({ failed: true })
        errorAlert({ message: 'loginError' })
      }
      const form = loginForm({
        onSuccess,
        onFail,
        addReset: this.state.failed
      })
      return form
    }
  }
}

module.exports = () => {
  return createElement({
    type: LoginChecker,
    props: {
      id: 'loguinCheckerMaster'
    }
  })
}