const createElement = require('../../renderer')

module.exports = (props) => {
  const update = (updated) => {
    console.info({ updated })
    console.info('this is update')
  }
  const { mutate } = props
  const onClick = (props) => {
    mutate({
      variables: { name: `${new Date().getSeconds()}`, nodeId: '2,13', parent_id: 1 },
      update,
    })
  }
  const div = createElement({
    props: {
      id: 'dummyClick',
      onClick
    },
    children: 'Click me',
  })
  return div
}
