
const { Component, createElement } = require('react')

module.exports = class Form extends Component {
  state = {
    count: 1
  }

  increment = () => {
    this.setState({
      count: this.state.count + 1
    })
  }
  render() {
    const { component } = this.props
    const toWrap = component(
      {
        ...this.props.args,
        state: this.state
      },
    )
    const div = createElement(
      'div',
      {
        //       onClick: this.increment,
        id: 'dasdasdas'
      },
      [toWrap, this.state.count]
    )
    return div
  }
}
