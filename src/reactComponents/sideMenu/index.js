const sideBar = require('../sideBar')
const menu = require('../menu')

module.exports = (props) => {
  const menuDiv = menu(props)
  const sideBarMenu = sideBar(menuDiv)
  return sideBarMenu
}