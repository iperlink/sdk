
module.exports = (data) => {
  const hasDates = data.filter_date
  const fieldsWithDates = hasDates
    ? {
      startDate: {
        type: 'calendar',
        finish: false,
        id: 'filterStart',
      },
      endDate: {
        type: 'calendar',
        finish: true,
        id: 'fiterEnd',
      },
    }
    : {}
  return fieldsWithDates
}