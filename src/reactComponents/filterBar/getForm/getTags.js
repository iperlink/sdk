
module.exports = ({ fieldsWithDates, options }) => {
  const fields = options
    ? {
      ...fieldsWithDates,
      tags: {
        type: 'typeAhead',
        name: 'etiquetas',
        options,
        value: '',
        id: 'filterTags',
      }
    }
    : fieldsWithDates
  return fields
}