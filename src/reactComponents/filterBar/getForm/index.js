const getDates = require('./getDates')
const getTags = require('./getTags')
const form = require('../../formCreator')

module.exports = ({ firstData, options, submit }) => {
  const fieldsWithDates = getDates(firstData)
  const fields = getTags({ fieldsWithDates, options })
  const formProps = {
    className: 'formCentered_horizontal',
    fields,
    id: 'tableFilterForm',
    recursive: true,
    hideSent: true,
    onSubmit: (test) => { submit(test) },
  }
  const rendered = Object.keys(fields).length === 0
    ? false
    : form(formProps)
  return rendered
}