const createElement = require('../../renderer')
const getForm = require('./getForm')
const getButton = require('./getButton')


module.exports = ({
  changeComp,
  data,
  firstData,
  id,
  options,
  submit,
}) => {
  const form = getForm({ firstData, options, submit })
  const btn = getButton({ id, changeComp, data });
  const container = createElement({
    props: { id: 'filterBarContainer' },
    children: [form, btn]
  })
  return container
}