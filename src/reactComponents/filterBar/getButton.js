const button = require('../btn')
const formConnector = require('../formConnector')

module.exports = ({ id, changeComp, data }) => {
  const {
    tables,
  } = CLIENT_CONFIG
  const { createForm } = tables[id].template
  if (!createForm) return null
  const formProps = {
    id: createForm.name,
    data,
  }
  const btn = button({
    className: 'filterBtn',
    id: `filterBarBtn`,
    message: 'editar',
    icon: 'FaEdit',
    onClick: () => {
      const newProps = formConnector(formProps)
      changeComp({ newProps, type: 'form' })
    }
  })
  return btn
}