const createElement = require('../../renderer')
const hide = require('./hide')

module.exports = () => {
  const menu = createElement({
    props: {
      id: 'sideNavIcon',
      style: {
        backgroundImage: `url(${LOGO_URL})`
      },
      onClick: hide,
    },
  })
  return menu

}