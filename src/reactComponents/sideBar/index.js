const createElement = require('../../renderer')
const icon = require('./icon')
const sideNav = require('./sideNav')

module.exports = (content) => {
  const Icon = icon('app')
  const SideNav = sideNav(content)
  const menu = createElement({
    props: {
      id: 'sideNavContainer',
    },
    children: [Icon, SideNav]
  })
  return menu

}