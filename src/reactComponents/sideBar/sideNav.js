const createElement = require('../../renderer')

module.exports = (content) => {
  const menu = createElement({
    props: {
      id: 'sideNav',
      className: 'sideNavInvisible',
    },
    children: [content]
  })
  return menu
}