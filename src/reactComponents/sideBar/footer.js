const createElement = require('../../renderer')
const close = require('./closeIcon')

module.exports = () => {
  const closeIcon = close(9)
  const foot = createElement({
    props: {
      id: 'footer',
    },
    children: closeIcon
  })
  return foot
}