const setClass = require('../../htmlHelpers/setClass')
const addClass = require('../../htmlHelpers/addClass')
const replaceClass = require('../../htmlHelpers/replaceClass')

module.exports = () => {
  setClass('sideNavIcon', 'visible')
  setClass('sideNav', 'sideNavInvisible')
  addClass('overlay', 'overlayVisible', 'overlayInvisible')
  replaceClass('menuSectionUlDeployed', 'menuSectionUl')
}
