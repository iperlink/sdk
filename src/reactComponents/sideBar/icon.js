const createElement = require('../../renderer')
const setClass = require('../../htmlHelpers/setClass')
const addClass = require('../../htmlHelpers/addClass')

const showMenu = () => {
  addClass('sideNav', 'sideNavVisible')
  addClass('overlay', 'overlayInvisible', 'overlayVisible')
  setClass('sideNavBtn', 'invisible')
}

module.exports = () => {
  const lines = ['1', '2', '3'].map(x => {
    const line = createElement({
      props: {
        className: 'menuLine',
        id: `menuLine_${x}`,
      }
    })
    return line
  })
  const menu = createElement({
    props: {
      id: 'sideNavBtn',
      onClick: () => showMenu(),
      id: 'sideNavIcon'
    },
    children: lines
  })
  return menu

}