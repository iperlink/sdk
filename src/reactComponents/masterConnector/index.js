const tableConnector = require('../tableConnector')
const controlTable = require('../fullTable')
const formConnector = require('../formConnector')

module.exports = (props) => {
  const { data, form, table, dataSourceTemplate, mutationName } = props
  const mutate = props[mutationName]
  // const formProps = formConnector({
  //   data,
  //   id: form,
  //   justProps: true,
  //   dataSourceTemplate,
  //   mutate,
  //   mutationName,
  // })
  const tableProps = tableConnector({
    data,
    id: table,
    dataSourceTemplate
  })
  if (data.loading || data.error) {
    return null
  }
  const fullProps = {
    form: formProps,
    table: tableProps
  }
  const rendered = controlTable(fullProps)
  return rendered
}