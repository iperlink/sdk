const { Component } = require('react')
const createElement = require('../../renderer')
const btn = require('../btn')
const loadGoogleScript = require('./loadGoogleScript')

module.exports = class Kml extends Component {
  state = {
    loaded: false,
    polygon: []
  }

  componentDidMount = async () => {
    this.setState({
      loaded: true
    })
    loadGoogleScript()
  }
  render() {
    const map = createElement({
      type: 'div',
      props: {
        id: 'map'
      }
    })
    return map
  }
}
