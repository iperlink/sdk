const { Component } = require('react')
const getOptions = require('../../dataMappers/getOptions')
const filterBar = require('../filterBar')
const createElement = require('../../renderer')
const groupBy = require('../../objMapper/groupBy')
const getWistiaGrouped = require('../wistia/wistiaGrouped')
const getScreenRange = require('../../htmlHelpers/getScreenRange')
const div = require('../div')
const take = require('../../objMapper/take')

const fullTable = class FullTable extends Component {
  state = {
    showing: this.props.showing,
    filterBy: false
  }

  submitFilter = (filterBy) => {
    this.setState({
      filterBy
    })
  }

  showMore = () => {
    const newShowing = this.state.showing + this.props.showing
    this.setState({
      showing: newShowing
    })

  }

  render() {
    const { id, data, groupedBy } = this.props
    const {
      filterBy,
      showing,
    } = this.state

    const chunk = take({
      array: data,
      amount: 3
    })
    const obj = groupBy({
      array: chunk,
      key: groupedBy
    })
    const filter = filterBar({
      data: data[0],
      options: getOptions({ data }),
      submit: this.submitFilter,
    });
    const range = getScreenRange()
    const wistia = getWistiaGrouped({
      title: 'testTitle',
      className: 'galleryCard',
      range,
      obj,
      id,
    })
    const container = div({
      id: `fullgalleryCont${id}`,
      children: [filter, wistia]
    });
    return container
  }
}

module.exports = (props) => {
  return createElement({
    type: fullTable,
    props: props
  })
}