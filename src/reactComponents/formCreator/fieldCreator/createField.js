const types = require('./types')
const updateWraper = require('../../hocs/shouldUpdate')
const createElement = require('../../../renderer')

module.exports = ({ field, key, onChange, masterId, changed }) => {
  const { type, updater } = field
  if (!type) {
    throw new Error(`field ${key}does not have a type`)
  }
  const name = field.name || `${key}`
  const fieldProps = {
    ...field,
    id: `${masterId}_${key}_${type}`,
    name,
  }
  const componentProps = {
    ...fieldProps,
    onChange: (e) => onChange({ target: e.target, field: fieldProps, fromTemplate: onChange, fieldKey: key }),
    updater: updater || ['value'],
    changed,
  }
  const currentType = types[type]
  if (!currentType) throw new Error(`type ${type} does not exist in types`)
  return createElement({
    type: updateWraper,
    props: {
      component: currentType,
      componentProps,
      id: `componentProps${fieldProps.id}`
    }
  })
}