const btn = require('../../../../btn')
const createRow = require('./createRow')

module.exports = ({ showMapState, showMap, remove, saveMap }) => {
  const showMapMsg = showMapState
    ? 'ocultar mapa'
    : 'mostrar mapa'
  const saveButton = btn({
    id: 'saveMap',
    onClick: saveMap,
    message: 'guardarMapa'
  })
  const showButton = btn({
    id: 'showMap',
    onClick: showMap,
    message: showMapMsg
  })
  const removeButton = btn({
    id: 'removeMapFeatures',
    onClick: remove,
    message: 'remove'
  })

  const result = showMapState
    ? [showButton, saveButton, removeButton]
    : showButton

  const container = createRow({
    children: result
  })
  return container
}