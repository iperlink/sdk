const sweetalert = require('sweetalert')
const { Component } = require('react')
const createButtons = require('./createButtons')
const createElement = require('../../../../../renderer')
const createLabel = require('../textInput/createLabel')
const createRow = require('../textInput/createRow')
const toggleClass = require('../../../../../htmlHelpers/toggleClass')
const getPointsFromPoly = require('../../../../../googleMaps/getCordsFromPoly')
const initMap = require('./initMap')

function remove() {
  map.data.forEach(function (feature) {
    map.data.remove(feature);
  })
}

const KmlPolygon = class KmlPolygon extends Component {
  state = {
    polygon: {},
    showMap: false,
  }

  componentDidUpdate(prevProps, prevState) {
    initMap(this.props.initialCords)
  }
  componentDidMount = async () => {
    initMap(this.props.initialCords)
  }

  showMap = () => {
    toggleClass('map_container_hidden', 'map_container')
    toggleClass('btn_map_container_hidden', 'btn_map_container')
    this.setState({ showMap: !this.state.showMap })
  }

  saveMap = async () => {
    if (!polygon.type) {
      sweetalert(`debe marcar un punto antes de guardar`)
    } else {
      const cords = getPointsFromPoly(polygon)
      const poly = {
        cords: cords,
        zoom: map.getZoom(),
        center: map.getCenter(),
      }
      this.setState = {
        polygon: poly
      }
      this.props.onChange({ target: poly })
    }
  }

  render() {
    const { id, name } = this.props
    const buttons = createButtons({
      showMapState: this.state.showMap,
      showMap: this.showMap,
      remove: remove,
      saveMap: this.saveMap
    })
    const mapEl = createElement({
      props: {
        id: 'map'
      }
    })
    const mapContainer = createElement({
      props: {
        className: 'map_container_hidden',
        id: `mapContainer${id}`
      },
      children: mapEl
    })
    const label = createLabel({ type: 'text', id: 'kmlPlanner', name })
    const container = createElement({
      props: {
        id: 'btn_map_container',
        className: 'btn_map_container_hidden',
      },
      children: [buttons, mapContainer]
    })
    const row = createRow({
      id: 'kmlPlanner',
      label,
      input: container,
    })
    return row
  }
}

module.exports = (props) => {
  const result = createElement({
    type: KmlPolygon,
    props,
  })
  return result
}