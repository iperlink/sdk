const bindListeners = require('../../../../../googleMaps/bindListeners')

module.exports = (initial) => {
  if (!initial) { throw new Error(`initial coordinates are not defined`) }
  const parsed = JSON.parse(initial)
  const {
    center,
    zoom,
  } = parsed
  map = new google.maps.Map(document.getElementById('map'), {
    center,
    zoom,
    mapTypeControl: true
  });

  map.data.setControls(['Polygon']);
  map.data.setStyle({
    editable: true,
    draggable: true
  });
  bindListeners(map.data)
}
