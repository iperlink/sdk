const createElement = require('../../../../../renderer')

module.exports = ({ id, onChange, value, name }) => {
  const input = createElement({
    type: 'input',
    props: {
      className: 'inputClass',
      id: `${id}input`,
      type: 'file',
      onChange,
      multiple: 'multiple',
      name,
    }
  })
  return input
}