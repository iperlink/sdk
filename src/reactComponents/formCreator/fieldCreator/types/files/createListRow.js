const createElement = require('../../../../../renderer')

module.exports = ({ id, list }) => {
  const className = ' fieldGridCont grid30_30_40 '
  const empty = createElement({
    props: {
      className,
      id: `${id}Row_containerDiv`,
    },
    children: ''
  })
  const row = createElement({
    props: {
      className,
      id: `${id}Row_containerDiv`,
    },
    children: [empty, list]
  })
  return row
}