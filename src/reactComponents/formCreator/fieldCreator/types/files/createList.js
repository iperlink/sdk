const createElement = require('../../../../../renderer')

module.exports = ({ elements, id }) => {
  if (!elements) return null
  const lis = elements.map((element, i) => {
    const option = createElement({
      type: 'li',
      props: {
        id: `option${id}${i}`,
        className: 'fileTag'
      },
      children: element.split('fakepath')[1]
    })
    return option
  })

  const ul = createElement({
    type: 'ul',
    props: {
      id: `optionul${id}`,
      className: 'fileTagUl'
    },
    children: lis
  })
  return ul
}