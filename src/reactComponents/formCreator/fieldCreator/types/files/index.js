const createInput = require('./createInput')
const createLabel = require('../textInput/createLabel')
const createRow = require('./createRow')
const createList = require('./createList')
const createListRow = require('./createListRow')

module.exports = props => {
  const {
    id,
    name,
    onChange,
    value,
  } = props
  const input = createInput({
    id,
    onChange,
    name
  })
  const label = createLabel({ type: 'text', id, name })
  const list = createList({ id, elements: value })
  const row = createRow({ id, label, input })
  const listRow = createListRow({ id, list })
  return [row, listRow]
}


