const createElement = require('../../../../../renderer')

module.exports = ({ id, label, input }) => {
  const className = ' fieldGridCont grid30_30_40 '
  const row = createElement({
    props: {
      className,
      id: `${id}_containerDiv`,
    },
    children: [label, input]
  })
  return row
}