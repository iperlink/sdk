const createElement = require('../../../../../renderer')

module.exports = ({ id, name, type }) => {
  const label = createElement({
    type: 'label',
    props: {
      className: `formLabel ${type}Label`,
      id: `${id}textlabel`,
      htmlFor: id,
    },
    children: name
  })
  return label
}