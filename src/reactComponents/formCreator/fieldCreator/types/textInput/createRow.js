const createElement = require('../../../../../renderer')

module.exports = ({ id, label, input, invalidDiv }) => {
  const className = 'fieldGridCont grid30_70 '
  const row = createElement({
    type: 'div',
    props: {
      className,
      id: `${id}_containerDiv`,
    },
    children: [label, input, invalidDiv]
  })
  return row
}