const createElement = require('../../../../../renderer')

module.exports = ({ id, onChange, value, name }) => {
  const Value = value || ''
  const input = createElement({
    type: 'input',
    props: {
      className: 'inputClass',
      id: `${id}input`,
      onChange,
      value: Value,
      name,
      autoComplete: 'off'
    }
  })
  return input
}