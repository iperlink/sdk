const makeInvalidDiv = require('../../../elements/invalid')
const createInput = require('./createInput')
const createLabel = require('./createLabel')
const createRow = require('./createRow')

module.exports = props => {
  const {
    id,
    name,
    onChange,
    invalid,
    value,
    invalidMsg
  } = props
  const input = createInput({
    id,
    onChange,
    value,
    name
  })
  const label = createLabel({ type: 'text', id, name })
  const invalidDiv = makeInvalidDiv({ id, invalidMsg, invalid })
  const row = createRow({ id, label, input, invalidDiv })
  return row
}


