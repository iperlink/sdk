const list = require('./list')
const createLabel = require('../textInput/createLabel')
const createSelect = require('./createSelect')
const createRow = require('../textInput/createRow')

module.exports = props => {
  const {
    id,
    name,
    onChange,
    value,
    options
  } = props
  const label = createLabel({ type: 'select', id, name })
  const elements = list({
    className: 'itemsCl',
    elements: options,
    id,
    value,
  })
  const input = createSelect({
    id,
    onChange,
    value,
    elements
  })
  const row = createRow({ id, label, input })
  return row
}


