const createElement = require('../../../../../renderer')

module.exports = ({ elements, id }) => {
  const options = elements.map((element, i) => {
    const { label, value } = element
    const option = createElement({
      type: 'option',
      props: {
        value: value,
        id: `${id}${i}`,
      },
      children: label
    })
    return option
  })

  return options
}