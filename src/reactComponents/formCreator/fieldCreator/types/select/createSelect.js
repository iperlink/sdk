const createElement = require('../../../../../renderer')

module.exports = props => {
  const {
    elements,
    id,
    onChange,
    value
  } = props
  const input = createElement({
    type: 'select',
    props: {
      className: 'selectInput',
      id: `${id}input`,
      onChange,
      value,
    },
    children: elements
  })
  return input
}