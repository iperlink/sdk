const createElement = require('../../../../../../../renderer')

module.exports = ({ elements, className, id, L, amount }) => {
  const rest = elements.length - amount
  const pushed = createElement({
    type: 'li',
    props: {
      className,
      id: `listEl${id}${L + 1}`,
    },
    children: `... + ${rest}`
  })
  return pushed
}