const createElement = require('../../../../../../../renderer')
const pushed = require('./over8')

module.exports = ({ elements, id, onClick }) => {
  const rendered = []
  const amount = 8
  const over8 = elements.length > amount
  const L = over8
    ? amount
    : elements.length
  for (let i = 0; i < L; i++) {
    const onClickEl = () => { onClick(elements[i]) }
    rendered.push(createElement({
      type: 'li',
      props: {
        className: 'typeaheadListElement',
        id: `${id}${i}`,
        onClick: onClickEl,
      },
      children: elements[i]
    })
    )
  }
  if (over8) {
    rendered.push(pushed({ id, L, elements, amount }))
  }
  return rendered
}