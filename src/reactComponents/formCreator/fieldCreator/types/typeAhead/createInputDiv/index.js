const createElement = require('../../../../../../renderer')
const createInput = require('../../textInput/createInput')
const createUl = require('./createUl')

module.exports = ({ id, onChange, options, value, showList }) => {
  const input = createInput({
    id,
    onChange,
    value
  })
  const ul = createUl({ value, options, id, onChange, showList })
  const row = createElement({
    props: {
      className: 'typeAhead',
      id: `${id}_containerInputDiv`,
    },
    children: [input, ul]
  })
  return row
}