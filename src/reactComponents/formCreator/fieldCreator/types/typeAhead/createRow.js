const createElement = require('../../../../../renderer')
const onKeyDown = require('./onKeyDown')

module.exports = ({ label, id, onChange, inputDiv, ul, options }) => {
  let selected = 0
  const row = createElement({
    props: {
      className: 'fieldGridCont row',
      id: `${id}_containerDiv`,
      onKeyDown: k => {
        const key = onKeyDown({ k, id, onChange, options, selected })
        if (key) {
          selected = key
        }
      },
    },
    children: [label, inputDiv, ul]
  })
  return row
}