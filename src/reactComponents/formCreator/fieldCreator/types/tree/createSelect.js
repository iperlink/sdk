const createElement = require('../../../../../renderer')
const list = require('./list')

module.exports = ({ id, onChange, value, options }) => {
  const elements = list(options)
  const input = createElement({
    type: 'select',
    props: {
      className: 'inputClass',
      id,
      onChange,
      value,
    },
    children: elements
  })
  return input
}