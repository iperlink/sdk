const createElement = require('../../../../../renderer')

module.exports = ({ ancestorsDiv, select, id }) => {
  const container = createElement({
    props: {
      id: `${id}inner_containerDiv`,
    },
    children: [ancestorsDiv, select]
  })
  return container
}