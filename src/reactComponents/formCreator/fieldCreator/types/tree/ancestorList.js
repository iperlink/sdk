const createElement = require('../../../../../renderer')

module.exports = (elements) => {
  const options = elements.map((element, i) => {
    const option = createElement({
      type: 'div',
      props: {
        className: `ancestorElement`,
        id: `ancestors${element}${i}`,
      },
      children: element
    })
    return option
  })

  return options
}