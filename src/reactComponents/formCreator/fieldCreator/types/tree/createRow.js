const createElement = require('../../../../../renderer')

module.exports = ({ id, label, container }) => {
  const row = createElement({
    type: 'div',
    props: {
      className: ' fieldGridContDouble grid30_30_40 ',
      id: `${id}_containerDiv`,
    },
    children: [label, container]
  })
  return row
}