const createLabel = require('../textInput/createLabel')
const createSelect = require('./createSelect')
const createAncestorDiv = require('./createAncestorDiv')
const createContainer = require('./createContainer')
const createRow = require('./createRow')

module.exports = props => {
  const {
    id,
    name,
    onChange,
    value,
    options,
    ancestors = [],
  } = props
  const label = createLabel({ id, name, type: 'tree' })
  const currentOptions = options[value].direct
  const select = createSelect({ id, onChange, value, options: currentOptions })
  const ancestorsDiv = createAncestorDiv({ id, ancestors, onChange })
  const container = createContainer({
    ancestorsDiv, select, id
  })
  const row = createRow({ id, label, container })
  return row
}


