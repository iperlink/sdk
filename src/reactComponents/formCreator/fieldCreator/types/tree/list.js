const createElement = require('../../../../../renderer')

module.exports = (elements) => {
  const padded = [''].concat(elements)
  const options = padded.map((element, i) => {
    const { name, id } = element
    const option = createElement({
      type: 'option',
      props: {
        id: `option${id}${i}`,
        value: id,
      },
      children: name
    })
    return option
  })

  return options
}