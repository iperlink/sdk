const createElement = require('../../../../../renderer')
const ancestorList = require('./ancestorList')
const resetDiv = require('./resetDiv')

module.exports = ({ id, ancestors, onChange }) => {
  const ancestorArray = ancestorList(ancestors)
  const ancestorsListRendered = (ancestorArray.length > 0)
    ? [resetDiv({ onChange, name })].concat(ancestorArray)
    : []
  const ancestorsDiv = createElement({
    props: {
      className: `ancestorsDiv`,
      id: `${id}ancestorsDiv`,
    },
    children: ancestorsListRendered
  })
  return ancestorsDiv
}