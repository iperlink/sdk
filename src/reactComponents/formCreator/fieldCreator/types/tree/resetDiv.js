const button = require('../../../../btn')

module.exports = ({ onChange, name }) => {
  const Btn = button({
    className: 'resetTreeAncestorBtn',
    id: `formBtnresetTreeAncestorBtn`,
    icon: 'FaBackward',
    onClick: () => onChange(name),
  })
  return Btn
}