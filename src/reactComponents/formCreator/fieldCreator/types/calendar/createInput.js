const createElement = require('../../../../../renderer')
const moment = require('moment')
const today = moment().format('YYYY-MM-DD')

module.exports = ({ id, name, onChange, value }) => {
  const input = createElement({
    type: 'input',
    props: {
      id: `${id}input`,
      onChange,
      name,
      value: value || today,
      type: 'date'
    }
  })
  return input

}

