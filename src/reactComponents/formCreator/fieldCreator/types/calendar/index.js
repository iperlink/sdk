const createRow = require('./createRow')
const createInput = require('./createInput')
const createIcon = require('./createIcon')

module.exports = props => {
  const {
    finish,
    id,
    name,
    onChange,
    value,
  } = props
  const input = createInput({
    id,
    onChange,
    name,
    value
  })
  const calendarIcon = createIcon(finish)
  const row = createRow({ id, calendarIcon, input })
  return row
}

