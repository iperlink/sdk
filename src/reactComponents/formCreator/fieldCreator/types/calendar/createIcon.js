const icon = require('../../../../icons')

module.exports = (finish) => {
  const iconName = finish
    ? 'FaArrowRight'
    : 'FaCalendar'
  const calendarIcon = icon({ name: iconName })
  return calendarIcon
}