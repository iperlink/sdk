const createElement = require('../../../../../renderer')

module.exports = ({ id, calendarIcon, input }) => {
  const row = createElement({
    type: 'div',
    props: {
      className: 'fieldGridCont grid30_70 row calendarRow',
      id: `${id}_containerDiv`,
    },
    children: [calendarIcon, input]
  })
  return row
}