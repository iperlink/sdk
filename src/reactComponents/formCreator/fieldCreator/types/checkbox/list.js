const createElement = require('../../../../../renderer')

module.exports = ({ elements, id, value = [] }) => {
  const options = elements.map((element, i) => {
    const { label } = element
    const optionProps = {
      id: `option${id}${i}`,
      selected: value.includes(element.value),
      type: 'checkbox',
      value: element.value,
    }
    const option = createElement({
      type: 'input',
      props: optionProps
    })
    const span = createElement({
      type: 'span',
      props: {
        id: `optionSpan${id}${i}`,
        className: `checkBoxSpan`,
      },
    })
    const lbl = createElement({
      type: 'label',
      props: { id: `option${id}${i}label` },
      children: [option, span, label]
    })
    return lbl
  })

  return options
}