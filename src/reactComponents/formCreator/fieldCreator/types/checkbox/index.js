const createElement = require('../../../../../renderer')
const createLabel = require('../textInput/createLabel')
const createCont = require('./createCont')

module.exports = props => {
  const {
    id,
    onChange,
    options,
    value,
  } = props
  if (!options) {
    throw new Error(`checkbox ${id} must contain options`)
  }
  const label = createLabel({ type: 'checkbox', id, name })
  const cont = createCont({
    id,
    onChange,
    options,
    value,
  })
  const row = createElement({
    type: 'div',
    props: {
      className: 'checkboxCont row',
      id: `${id}_containerDiv`,
    },
    children: [label, cont]
  })
  return row
}


