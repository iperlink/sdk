const createElement = require('../../../../../renderer')
const list = require('./list')

module.exports = props => {
  const {
    id,
    onChange,
    options,
    value,
  } = props
  const elements = list({
    elements: options,
    id,
    value
  })
  const container = createElement({
    type: 'div',
    props: {
      className: 'checkBoxContainer',
      id: `${id}checbox`,
      onChange,
    },
    children: elements
  })
  return container
}