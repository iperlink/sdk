const createInputDiv = require('./createInputDiv')
const createRow = require('./createRow')
const createLabel = require('../textInput/createLabel')

module.exports = props => {
  const {
    id,
    name,
    onChange,
    value,
    options,
    tokens,
    showList,
  } = props
  const inputDiv = createInputDiv({ id, onChange, options, value,showList })
  const label = createLabel({ type: 'text', id, name })
  const row = createRow({
    id,
    inputDiv,
    label,
    onChange,
    options,
    tokens,
    value,
  })
  return row
}


