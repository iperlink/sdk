const createElement = require('../../../../../../renderer')
const list = require('./list')
const hide = require('../../../../../../htmlHelpers/hide')

module.exports = ({ value, options, id, onChange, showList }) => {
  const onClick = val => {
    onChange({ target: { value: val, clicked: true } })
    hide(`${id}_containerUl`)
  }

  const elements = list({
    elements: options,
    id,
    onClick,
  })
  const mustShow = (value === '' || elements.length < 1 || !showList)
  const ulClass = mustShow
    ? ' typeAheadUlhidden'
    : ' typeAheadUl'
  const ul = createElement({
    type: 'ul',
    props: {
      className: ulClass,
      id: `${id}_containerUl`,
    },
    children: elements
  })
  return ul
}