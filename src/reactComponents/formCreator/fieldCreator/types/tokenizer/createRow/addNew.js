const button = require('../../../../../btn')

module.exports = ({ id, onChange, value }) => {
  const btn = button({
    className: 'addNewBtn',
    id: `addNewBtn${id}`,
    onClick: () => onChange({ target: { value, clicked: true } }),
    icon: 'FaBackward'
  })
  return btn
}