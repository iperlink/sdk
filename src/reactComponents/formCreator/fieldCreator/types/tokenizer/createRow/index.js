const createElement = require('../../../../../../renderer')
const onKeyDown = require('./onKeyDown')
const tokenList = require('./tokenList')
const addNew = require('./addNew')

module.exports = ({
  id,
  inputDiv,
  label,
  onChange,
  options,
  tokens,
  ul,
  value,
}) => {
  let selected = 0
  const tokenRendered = tokenList({ id, tokens })
  const newBtn = addNew({
    id,
    onChange,
    value,
  })
  const row = createElement({
    props: {
      className: 'gridCont3 row',
      id: `${id}_containerDiv`,
      onKeyDown: k => {
        const key = onKeyDown({ k, id, onChange, options, selected })
        if (key) {
          selected = key
        }
      },
    },
    children: [label, inputDiv, newBtn, tokenRendered, ul]
  })
  return row
}