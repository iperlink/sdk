const handleKeyDown = require('../../../../../../htmlHelpers/handleKeyDown')

module.exports = ({ k, id, onChange, options, selected }) => {
  const keyed = handleKeyDown({
    k,
    options,
    ulId: `${id}_containerUl`,
    elClass: 'itemsCl',
    selected
  })
  if (keyed.value) {
    onChange({
      target: { value: keyed.value }
    })
    return keyed
  }
  else {
    return keyed
  }
}