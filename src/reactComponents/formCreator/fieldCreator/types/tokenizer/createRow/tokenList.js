const createElement = require('../../../../../../renderer')

module.exports = ({ id, tokens }) => {
  const elements = tokens.map((token) => {
    const res = createElement({
      type: 'li',
      props: { className: 'tokenLi', id: `${token}Li` },
      children: token,
    })
    return res
  })
  const ul = createElement({
    type: 'ul',
    props: {
      className: 'tokensUl',
      id: `${id}_tokenscontainerUl`,
    },
    children: elements
  })
  return ul
}