const createElement = require('../../../../renderer')

const createRow = require('./textInput/createRow')
const createLabel = require('./textInput/createLabel')

module.exports = props => {
  const {
    id,
    name,
  } = props
  const input = createElement({
    type: 'input',
    props: {
      id: `${id}input`,
      name: 'password',
      type: 'password',
      autoComplete: 'current-password'
    }
  })
  const label = createLabel({ type: 'text', id, name })
  const row = createRow({ id, label, input, })

  return row
}

