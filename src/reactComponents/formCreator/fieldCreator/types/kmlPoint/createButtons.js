const btn = require('../../../../btn')
const createRow = require('./createRow')

module.exports = ({ showMapState, showMap, saveMap }) => {
  const showMapMsg = showMapState
    ? 'ocultar mapa'
    : 'mostrar mapa'
  const saveButton = btn({
    id: 'saveMap',
    onClick: saveMap,
    message: 'guardarMapa'
  })
  const showButton = btn({
    id: 'showMap',
    onClick: showMap,
    message: showMapMsg
  })

  const result = showMapState
    ? [showButton, saveButton]
    : showButton

  const container = createRow({
    children: result
  })
  return container
}