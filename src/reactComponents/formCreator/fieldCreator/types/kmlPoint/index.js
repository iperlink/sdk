const sweetalert = require('sweetalert')
const { Component } = require('react')
const createElement = require('../../../../../renderer')
const toggleClass = require('../../../../../htmlHelpers/toggleClass')
const createRow = require('../textInput/createRow')
const createLabel = require('../textInput/createLabel')
const createButtons = require('./createButtons')
const initMap = require('./initMap')
const getPointsFromPoly = require('../../../../../googleMaps/getCordsFromPoly')

module.exports = class KmlPoint extends Component {
  state = {
    polygon: {},
    showMap: false,
  }

  componentDidMount = async () => {
    initMap(this.props.initialCords)
  }

  showMap = () => {
    toggleClass('map_container_hidden', 'map_container')
    toggleClass('btn_map_container_hidden', 'btn_map_container')
    this.setState({ showMap: !this.state.showMap })
  }

  saveMap = async () => {
    if (!polygon.type) {
      sweetalert(`debe marcar un punto antes de guardar`)
    } else {
      const cords = getPointsFromPoly(polygon)
      const poly = {
        zoom: map.getZoom(),
        center: cords,
      }
      this.setState = {
        polygon: poly
      }
      this.props.onChange({ target: poly })
    }
  }

  render() {
    const buttons = createButtons({
      showMapState: this.state.showMap,
      showMap: this.showMap,
      saveMap: this.saveMap
    })
    const mapEl = createElement({
      type: 'div',
      props: {
        id: 'map'
      }
    })
    const mapContainer = createElement({
      type: 'div',
      props: {
        className: 'map_container_hidden',
        id: 'map_container',
      },
      children: mapEl
    })
    const label = createLabel({ type: 'text', id: 'kmlPlanner', name: 'mapa' })
    const container = createElement({
      type: 'div',
      props: {
        id: 'btn_map_container',
        className: 'btn_map_container_hidden',
      },
      children: [buttons, mapContainer]
    })
    const row = createRow({
      id: 'kmlPlanner',
      label,
      input: container,
    })
    return row
  }
}
