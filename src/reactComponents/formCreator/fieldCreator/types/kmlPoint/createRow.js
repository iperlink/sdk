const createElement = require('../../../../../renderer')

module.exports = ({ id, children }) => {
  const row = createElement({
    props: {
      className: `gridContMap row`,
      id: `mapPlanner_containerDiv`,
    },
    children
  })
  return row
}