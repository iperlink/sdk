const bindListeners = require('../../../../../googleMaps/bindListeners')

module.exports = (mapInitialCords) => {
  if (!mapInitialCords) { throw new Error(`initial coordinates are not defined`) }
  const parsed = JSON.parse(mapInitialCords)
  map = new google.maps.Map(document.getElementById('map'), {
    center: parsed.center,
    zoom: parsed.zoom,
    mapTypeControl: true
  });

  map.data.setControls(['Point']);
  map.data.setStyle({
    editable: true,
    draggable: true
  });
  bindListeners(map.data)
}
