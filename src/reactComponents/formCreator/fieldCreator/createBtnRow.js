const button = require('../../btn')
const createElement = require('../../../renderer')

module.exports = (masterId) => {
  const Btn = button({
    className: `formSubmitter`,
    id: `formBtn${masterId}`,
    message: 'enviar',
    submit: true,
  })
  const div = createElement({
    props: {
      id: `formBtn${masterId}Cont`,
      className: 'fieldGridCont grid30_70',
    },
    children: Btn
  })
  return div
}
