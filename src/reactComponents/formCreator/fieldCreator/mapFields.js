const createElement = require('../../../renderer')
const types = require('./types')
const updateWraper = require('../../hocs/shouldUpdate')

module.exports = props => {
  const { fields, key, onChange, changed } = props
  if (!fields) {
    throw new Error('fields are not defined')
  }
  const fieldValues = Object.values(fields)
  const result = fieldValues.map((field, i) => {
    const { type, id, name } = field
    const componentProps = {
      ...field,
      name: name || id,
      onChange: (e) => onChange({ e, field, fromTemplate: props.onChange, fields }),
      updater: 'value',
      changed,
    }
    return createElement({
      type: updateWraper,
      props: {
        component: types[type],
        componentProps,
        id: `${id}updater`
      }
    })

  })
  return createElement({
    type: 'div',
    props: { key },
    children: result
  })
}
