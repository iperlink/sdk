const createElement = require('../../../renderer')
const mapValues = require('../../../objMapper/mapValues')
const createField = require('./createField')
const createBtnRow = require('./createBtnRow')
const button = require('../../btn')

module.exports = props => {
  const { fields, onChange, changed, masterId, hSubmitFirst } = props
  if (!fields) {
    throw new Error('fields are not defined')
  }
  const result = mapValues({
    obj: fields,
    fn: (field, key) => createField({ field, key, onChange, masterId, changed })
  })
  const Btn = hSubmitFirst
    ? button({
      className: `formSubmitter`,
      id: `formBtn${masterId}`,
      message: 'enviar',
      submit: true,
    })
    : createBtnRow(masterId)
  const children = Object.values(result).concat([Btn])
  const rendered = hSubmitFirst
    ? createElement({
      props: { id: masterId, className: 'fieldContainerH' },
      children: [Btn].concat(Object.values(result))
    })
    : createElement({
      props: { id: masterId, className: 'fieldContainer' },
      children
    })
  return rendered
}
