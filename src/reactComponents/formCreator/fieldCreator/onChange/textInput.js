module.exports = ({ changeProps }) => {
  const { field, target } = changeProps
  const { value } = target
  const newField = {
    ...field,
    value
  }
  return newField
}