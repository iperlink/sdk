const checkbox = require('./checkbox')
const kmlPolygon = require('./kmlPolygon')
const select = require('./select')
const text = require('./textInput')
const tree = require('./tree')
const files = require('./files')
const typeAhead = require('./typeAhead')
const tokenizer = require('./tokenizer')

module.exports = {
  calendar: text,
  date: text,
  checkbox,
  files,
  kmlPoint: kmlPolygon,
  kmlPolygon,
  password: text,
  select,
  text,
  time: text,
  tree,
  typeAhead,
  tokenizer,

}