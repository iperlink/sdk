const find = require('lodash/find')

module.exports = async ({ changeProps, originalProps }) => {
  const { field, target } = changeProps
  const { name } = field
  const original = originalProps.fields[name]
  if (!target) {
    return original
  }
  const index = target.options[target.selectedIndex]
  const value = index.value
  const optionName = index.label
  const newTree = find(field.options, { id: parseInt(value) })
  const ancestors = field.ancestors || []
  const newAncestors = ancestors.concat(optionName)
  const newField = (newTree.direct)
    ? {
      ...field,
      value,
      ancestors: newAncestors
    }
    : {
      ...field,
      value
    }
  return newField
}