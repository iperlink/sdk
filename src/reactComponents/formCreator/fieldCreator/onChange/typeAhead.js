const filterByInitial = require('../../../../filters/filterByInitial')
const mapKeys = require('../../../../objMapper/mapKeys')

module.exports = ({ changeProps, originalProps }) => {
  const { field, target } = changeProps
  const { fields } = originalProps
  const mappedFields = mapKeys({
    obj: fields,
    fn: (key, value) => {
      return value.name
    }
  })
  const currentField = mappedFields[field.name]
  if (!currentField) {
    throw new error(`field ${name} is not defined`)
  }
  const { options, onClick } = currentField
  const { value, clicked } = target
  clicked && onClick && onClick(value)
  const showList = clicked
    ? false
    : true
  const filtered = filterByInitial({ options, value })
  const newField = {
    ...field,
    options: filtered,
    value,
    showList
  }
  return newField
}