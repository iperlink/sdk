const filterByInitial = require('../../../../filters/filterByInitial')
const simplePush = require('../../../../objMapper/simplePush')
const mapKeys = require('../../../../objMapper/mapKeys')

module.exports = ({ changeProps, originalProps }) => {
  const { field, target } = changeProps
  const { fields } = originalProps
  const { tokens } = field
  const mappedFields = mapKeys({
    obj: fields,
    fn: (key, value) => {
      return value.name
    }
  })
  const { options } = mappedFields[field.name]
  const { clicked, value } = target
  const newTokens = clicked
    ? simplePush({ array: tokens, value })
    : tokens
  const filtered = clicked
    ? []
    : filterByInitial({ options, value })
  const showList = clicked
    ? false
    : true
  const newField = {
    ...field,
    options: filtered,
    tokens: newTokens,
    value,
    showList,
  }
  return newField
}