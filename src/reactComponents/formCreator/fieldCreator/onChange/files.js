const togglePush = require('../../../../objMapper/togglePush')
const checkAllowed = require('../../../../aws/s3/put/checkAllowed')
const extractFileName = require('../../../../fs/extractFilename')

module.exports = async ({ changeProps }) => {
  const { field, target } = changeProps
  const { value, files } = target
  const selectedFiles = field.selectedFiles || []
  const newSelectedFiles = selectedFiles.concat(files[0])
  const filename = extractFileName(value);
  const allowed = checkAllowed({
    allowed: field.allowed,
    name: filename,
    front: true,
  })
  if (allowed) {
    const oldValue = field.value || []
    const newValue = togglePush({
      array: oldValue,
      value: value
    })
    const newField = {
      ...field,
      selectedFiles: newSelectedFiles,
      value: newValue
    }
    return newField
  }
  return field
}