module.exports = ({ changeProps }) => {
  const { field, target } = changeProps
  const newField = {
    ...field,
    value: JSON.stringify(target)
  }
  return newField
}