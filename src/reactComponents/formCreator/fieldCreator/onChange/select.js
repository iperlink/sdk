module.exports = async ({ changeProps }) => {
  const { field, target } = changeProps
  const index = target.options[target.selectedIndex]
  const value = index.value
  const newField = {
    ...field,
    value
  }
  return newField
}