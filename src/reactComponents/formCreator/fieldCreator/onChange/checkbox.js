import togglePush from '../../../../objMapper/togglePush'

module.exports = async ({ changeProps }) => {
  const { field, target } = changeProps
  const { value } = target
  const oldValue = field.value || []
  const newValue = togglePush({
    array: oldValue,
    value: parseInt(value)
  })
  const newField = {
    ...field,
    value: newValue
  }
  return newField
}