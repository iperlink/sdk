const { Component } = require('react')
const createElement = require('../../renderer')
const { handleOnChange, handleSumbit } = require('./handlers')
const fieldCreator = require('./fieldCreator')
const spinner = require('../loader/loading')

const Form = class Form extends Component {

  state = {
    fields: this.props.fields,
    status: 'waiting',
  }

  onSubmit = async (e) => {
    e.preventDefault()
    this.setState({
      status: 'submiting'
    })
    const submitedRes = await handleSumbit({
      e,
      state: this.state,
      onSubmit: this.props.onSubmit,
      formId: this.props.id,
      objId: this.props.objId,
    })
    this.setState({
      submitedRes,
      status: 'submited',
    })
    this.props.changeComp && this.props.changeComp()
  }

  onChange = async (changeProps) => {
    const { fields } = this.state
    const changed = await handleOnChange({
      changeProps,
      fields,
      originalProps: this.props
    })
    this.setState({
      fields: changed
    })
  }

  render() {
    const { className, id, message, icon, title } = this.props
    const { fields, status } = this.state
    if (status === 'submiting') {
      return spinner()
    }
    const renderedFields = fieldCreator({
      fields,
      id: `form${id}Fields`,
      masterId: id,
      onChange: this.onChange,
      icon,
      message,
      hSubmitFirst: this.props.hSubmitFirst
    })
    const titleDiv = title
      ? createElement({
        props: { className: 'formTitle', id: 'formTitle' },
        children: title
      })
      : null
    const result = createElement({
      type: 'form',
      props: {
        className: ` ${className} formContainer`,
        id,
        autoComplete: "off",
        action: "/loginProcess",
        onSubmit: this.onSubmit,
      },
      children: [titleDiv, renderedFields]
    })
    return result
  }
}

module.exports = (props) => {
  const result = createElement({
    type: Form,
    props,
  })
  return result
}