
module.exports = (props) => {
  const additional = props.attach(props)
  const result = {
    ...props.fields,
    ...additional
  }
  return result
}