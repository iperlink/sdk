const createElement = require('../../../renderer')

module.exports = ({ invalid, invalidMsg, id }) => {
  const result = invalid ?
    createElement({
      type: 'div',
      props: {
        className: 'textFieldInvalid',
        id: `${id}_invalidDiv`,
      },
      children: invalidMsg
    })
    : null
  return result
}