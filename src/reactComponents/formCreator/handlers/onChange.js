const onChange = require('../fieldCreator/onChange')

module.exports = async ({ changeProps, fields, originalProps }) => {
  const { field, fieldKey } = changeProps
  const currentField = field
  const { type } = currentField
  const onChangeByType = onChange[type]
  if (!onChangeByType) throw new Error(`${type} is not defined in onChange`)
  const changedPrimary = await onChangeByType({ changeProps, fields, originalProps })
  const newFields = field.onChange
    ? await customFunctions[field.onChange]({ changeProps, fields, changedPrimary })
    : {
      ...fields,
      [fieldKey]: changedPrimary
    }
  return newFields
}