const fileUpload = require('./fileUpload')
const random = require('../../../../strings/random')

module.exports = async ({ fields, extracted }) => {
  const keys = Object.keys(fields)
  const uploaders = Object.values(fields).filter(filter => filter.type === 'files')
  if (uploaders.length) {
    const promises = []
    const newExtracted = Object.assign(
      {},
      extracted
    )
    keys.forEach((key, i) => {
      const names = []
      const randomName = random()
      const currentField = fields[key]
      if (currentField.type === 'files') {
        const files = currentField.selectedFiles
        files.forEach((file, j) => {
          const ext = file.name.split('.')[1]
          const currentName = `${file.name}|${randomName}${i}${j}.${ext}`
          names.push(currentName)
          promises.push(fileUpload({ data: file, randomName: currentName, ext }))
        })
        newExtracted[key] = names.join(',')
      }
    })

    await Promise.all(promises)
    return newExtracted
  }
  return extracted

}














//   const uploaders = Object.values(fields).filter(filter => filter.type === 'files')
//   if (uploaders.length) {
//     const names = []
//     const randomName = random()
//     const promises = []
//     uploaders.forEach((uploader, i) => {
//       const files = document.getElementById(`${uploader.id}input`).files
//       files.forEach((file, j) => {
//         const currentName = `${randomName}${i}${j}`
//         names.push(currentName)
//         promises.push(fileUpload({ data: file, name: currentName }))
//       })
//     })
//     await Promise.all(promises)
//   }
// }