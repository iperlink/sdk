const getBase64 = require('../../../../fs/getBase64')
const checkAllowed = require('../../../../aws/s3/put/checkAllowed')

module.exports = async ({ url, data, randomName, ext }) => {
  const base = await getBase64(data)

  allowedFileExtensions && checkAllowed({
    allowed: allowedFileExtensions,
    name: randomName,
    front: true
  })
  const name = randomName
  try {
    await fetch('https://00es1lyteh.execute-api.us-east-1.amazonaws.com/beta/upload', {
      method: 'POST',
      body: JSON.stringify({ base, name, ext }),
    })
  } catch (e) {
    console.error(e)
    console.error(' error uploading file ', data)
  }
}