const extractValues = require('./utils/extractValues')
const upFiles = require('./withFiles')
const errorAlert = require('../../errorAlert')

module.exports = async ({
  state,
  onSubmit,
  formId,
  objId,
}) => {
  try {
    if (!state.fields) {
      throw new Error('must send state.fields')
    }
    const extracted = extractValues({ fields: state.fields, formId })
    if (formValidations[formId]) {
      formValidations[formId](extracted)
    }
    const withFiles = await upFiles({ fields: state.fields, extracted })
    const variables = objId
      ? {
        ...withFiles,
        id: objId,
      }
      : withFiles
    await onSubmit(variables)
    return extracted
  } catch (e) {
    errorAlert(e)
    return {}
  }
}