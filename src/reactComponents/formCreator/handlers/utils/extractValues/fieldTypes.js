const getAttById = require('../../../../../htmlHelpers/getAttById')

module.exports = ({ field, formId, key }) => {
  const {
    type,
    value,
    tokens,
  } = field
  const types = {
    tree: () => value,
    checkbox: () => value,
    kmlPolygon: () => value,
    password: () => value,
    kmlPoint: () => value,
    tokenizer: () => tokens,
    files: () => getAttById({ id: `${formId}_${key}_${type}input`, att: 'value' }),
    select: () => getAttById({ id: `${formId}_${key}_${type}input`, att: 'value' }),
    text: () => getAttById({ id: `${formId}_${key}_${type}input`, att: 'value' }),
    date: () => getAttById({ id: `${formId}_${key}_${type}input`, att: 'value' }),
    calendar: () => getAttById({ id: `${formId}_${key}_${type}input`, att: 'value' }),
    time: () => getAttById({ id: `${formId}_${key}_${type}input`, att: 'value' }),
    typeAhead: () => getAttById({ id: `${formId}_${key}_${type}input`, att: 'value' }),
  }
  const result = types[type]()
  return result
}

