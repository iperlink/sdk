const mapValues = require('../../../../../objMapper/mapValues')
const fieldTypes = require('./fieldTypes')

module.exports = ({ fields, formId }) => {
  const values = mapValues({
    obj: fields, fn: (field, key) => {
      const result = fieldTypes({ formId, field, key })
      return result
    }
  })
  return values
}