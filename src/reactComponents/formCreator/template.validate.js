

module.exports = template => {
  const values = Object.values(template.fields)
  let invalid = false
  values.forEach(field => {
    const { type, options, id } = field
    if (['checkbox', 'select', 'radio'].includes(type)) {
      if (!options) throw new Error(`Field  ${id}  must contain options`)
    }
    if (!type) {
      throw new Error('field does not have type', JSON.stringify(field, null, 2))
    }
  })
  const result = invalid || template
  return result
}