const createElement = require('../../renderer')

module.exports = (error) => {
  const errorDiv = createElement({
    type: 'div',
    props: {
      id: 'error'
    },
    children: null
  })
  return errorDiv
}