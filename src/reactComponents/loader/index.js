const loadingComp = require('./loading')
const errorComp = require('./error')
const errorAlert = require('../errorAlert')

module.exports = (props) => {
  if (!props) {
    throw new Error('component does not have props')
    return loadingComp()
  }
  const {
    loading,
    error
  } = props
  if (loading) {
    return loadingComp()
  }
  if (error) {
    errorAlert(JSON.stringify({ code: 'generic' }))
    return errorComp(error)
  }
  return false
}
