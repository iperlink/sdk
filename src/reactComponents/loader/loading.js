const createElement = require('../../renderer')
const Spinner = require('react-spinkit')

module.exports = () => {
  const spin = createElement({
    type: Spinner,
    props: {
      name: 'double-bounce',
      id: 'spinner',
      style: {
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '60px',
        height: '60px',
      }
    }
  })
  const Loading = createElement({
    props: {
      className: 'spinerCont',
      id: 'loadingComp'
    },
    children: spin
  })
  return Loading
}