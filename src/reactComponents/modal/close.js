const toggleClass = require('../../htmlHelpers/toggleClass')


module.exports = () => {
  toggleClass('modal', 'modalShow')
}