const createElement = require('../../renderer')
const btn = require('../btn')
const closeModal = require('./close')

module.exports = props => {
  const {
    children,
    id
  } = props
  const modal = createElement({
    type: 'div',
    props: {
      id: `modal`,
      className: `modalContent`,
    },
    children
  })

  const background = createElement({
    type: 'div',
    props: {
      id: `modal`,
      className: `modal`,
      onClick: closeModal
    },
    children: modal
  })
  return background
}
