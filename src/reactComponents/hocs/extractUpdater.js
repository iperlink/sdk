
module.exports = (props) => {
  const { updater } = props
  const prev = updater.reduce((acc, curr) => {
    const currentValue = props[curr]
    if (!currentValue) return acc
    const result = Object.values(currentValue)
      ? JSON.stringify(currentValue)
      : currentValue
    return `${acc}${result}`
  }, '')
  return prev
}