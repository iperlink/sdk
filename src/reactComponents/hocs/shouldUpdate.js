const { Component, createElement } = require('react')
const extractUpdater = require('./extractUpdater')

module.exports = class Form extends Component {
  shouldComponentUpdate(nextProps) {
    const updater = extractUpdater(this.props.componentProps)
    const next = extractUpdater(nextProps.componentProps)
    const compared = (updater !== next)
    return compared
  }

  render() {
    const {
      component,
      componentProps,
    } = this.props
    const row = createElement(
      component,
      componentProps,
      null
    )
    return row
  }
}


