const createElement = require('../../renderer')
const getEmail = require('../../localStorage/getEmail')
const button = require('../btn')

module.exports = () => {
  const email = getEmail()
  const emailDiv = createElement({
    props: {
      id: 'emailDiv',
      className: 'userBarElement'
    },
    children: email
  })
  const exit = button({
    id: 'exitBtn',
    className: 'exitBtn',
    icon: 'FaSignOutAlt'
  })
  const userCard = createElement({
    props: {
      id: 'userCard',
      className: 'userCard'
    },
    children: [emailDiv, exit]
  })
  return userCard
} 