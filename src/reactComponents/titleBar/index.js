const createElement = require('../../renderer')
const userBar = require('./userBar')

module.exports = (props) => {
  const title = createElement({
    props: { id: 'titleText' },
    children: props.title
  })
  const userBarRendered = userBar()
  const fullDiv = createElement({
    props: { id: 'titleDiv' },
    children: [title, userBarRendered]
  })
  return fullDiv
}