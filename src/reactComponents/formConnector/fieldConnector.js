const mapValues = require('../../objMapper/mapValues')
const mapper = require('./mapper')

module.exports = ({ withInitial, data }) => {
  const mapped = mapValues({
    obj: withInitial,
    fn: (field, key) => {
      return mapper({ field, data })
    }
  })
  return mapped
}