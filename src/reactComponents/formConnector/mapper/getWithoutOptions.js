
module.exports = ({ value, field }) => {
  const result = value
    ? {
      ...field,
      value,
    }
    : field
  return result
}