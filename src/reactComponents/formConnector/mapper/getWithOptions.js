module.exports = ({ initialValue, field, optionsFromData, dataSource }) => {
  const withOptions = initialValue
    ? {
      ...field,
      value: initialValue,
      data: dataSource,
      options: optionsFromData
    }
    : {
      ...field,
      data: dataSource,
      options: optionsFromData
    }
  return withOptions
}