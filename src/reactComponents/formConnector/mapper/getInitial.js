
module.exports = ({ data, initial }) => {
  if (!initial) return null
  const [queryName, val, local] = initial
  if (local) {
    if (!localStorageFunctions[queryName]) {
      throw new Error(`localStorageFunction ${queryName} is not defined`)
    }
    const initialLocal = localStorageFunctions[queryName](val)
    return initialLocal
  }
  const intialData = data[queryName][0][val]
  return intialData
}