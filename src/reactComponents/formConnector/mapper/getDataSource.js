
module.exports = ({ data, options }) => {
  if (Array.isArray(options)) return options
  const { from, local } = options
  if (!from && !local) {
    throw new Error(` must define from param in options`)
  }
  const dataSource = local
    ? localStorageFunctions[from] && localStorageFunctions[from]()
    : data[from]
  if (!dataSource) {
    throw new Error(`datasource ${from} is not defined`)
  }
  return dataSource
}