const getDataSource = require('./getDataSource')
const getInitial = require('./getInitial')
const getOptionsFromData = require('./getOptionsFromData')
const getWithOptions = require('./getWithOptions')
const getWithoutOptions = require('./getWithoutOptions')
const userCan = require('../../../localStorage/userCan')

module.exports = ({ field, data }) => {
  const { type, options, initial, value, privilege } = field
  if (privilege) {
    const can = userCan(privilege)
    if (!can) return null
  }
  if (!options) {
    return getWithoutOptions({ value, field })
  }
  const initialValue = getInitial({ data, initial })
  const dataSource = getDataSource({ data, options })
  const optionsFromData = getOptionsFromData({ type, dataSource, options })
  const withOptions = getWithOptions({ initialValue, field, optionsFromData, dataSource })
  return withOptions
}