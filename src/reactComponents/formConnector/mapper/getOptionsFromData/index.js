const getOptionsFrom = require('./getOptionsFrom')
const uniqBy = require('../../../../objMapper/uniqBy')

module.exports = ({ type, dataSource, options }) => {
  if (Array.isArray(options)) return options
  const { value, label } = options
  const Label = label || 'name'
  const optionsFromData = ['select', 'tree', 'typeAhead'].includes(type)
    ? getOptionsFrom[type]({ label, dataSource, value })
    : dataSource.map(source => {
      const res = source[Label]
      return res
    })
  const uniques = Array.isArray(optionsFromData)
    ? uniqBy({ array: optionsFromData, by: 'value' })
    : optionsFromData
  return uniques
}