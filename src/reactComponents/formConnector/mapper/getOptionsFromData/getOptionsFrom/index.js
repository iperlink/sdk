
const getOptionsFromSelect = require('./getOptionsFromSelect')
const getOptionsFromTree = require('./getOptionsFromTree')
const getOptionsFromTypeAhead = require('./getOptionsFromTypeAhead')

module.exports = {
  select: getOptionsFromSelect,
  typeAhead: getOptionsFromTypeAhead,
  tokenizer: getOptionsFromSelect,
  tree: getOptionsFromTree

}