const uniqBy = require('../../../../../objMapper/uniqBy')

module.exports = ({ label, dataSource, value }) => {
  const options = dataSource.map(source => {
    const Label = label || 'name'
    const Value = value || 'id'
    const res = {
      label: source[Label],
      value: source[Value]
    }
    return res
  })
  const uniqueOptions = uniqBy({ array: options, by: 'value' })
  const concated = [{
    label: '',
    value: 0
  }].concat(uniqueOptions)
  return concated
}