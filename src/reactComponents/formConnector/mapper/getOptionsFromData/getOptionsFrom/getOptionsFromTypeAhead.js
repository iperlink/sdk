const uniq = require('../../../../../objMapper/uniq')

module.exports = ({ label, dataSource, value }) => {
  const options = dataSource.map(source => {
    const Label = label || 'name'
    return source[Label]
  })
  const uniqueOptions = uniq(options)
  return uniqueOptions
}