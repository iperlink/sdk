const errorAlert = require('../errorAlert')
const gql = require('graphql-tag')

module.exports = ({ mutate, update, mutationName, id }) => {
  const {
    querys
  } = CLIENT_CONFIG
  const result = async (rawVariables) => {
    const variables = id
      ? { ...rawVariables, id }
      : rawVariables
    try {
      const res = await mutate({
        variables,
        refetchQuerys: () => {
          return [
            { query: gql`${querys.editMission.compiled}` }
          ]
        },
        awaitRefetchQuerys: true,
        optimisticResponse: false,
        update: (store, o) => update({ store, o })
      })
      if (!res.data[mutationName]) {
        errorAlert(JSON.stringify({ code: 'networkError' }))
      }
      if (res.data[mutationName].error) {
        errorAlert(res.data[mutationName].error)
      }
    } catch (e) {
      errorAlert(e)
    }
  }
  return result
}