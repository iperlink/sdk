const mapValues = require('../../objMapper/mapValues')

module.exports = ({ fields, initialValues }) => {
  const result = mapValues({
    obj: fields,
    fn: (field, key) => {
      const inner = initialValues[key]
        ? {
          ...field,
          value: initialValues[key]
        }
        : field
      return inner
    }
  })
  return result
}