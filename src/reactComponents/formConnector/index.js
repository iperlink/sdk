const fieldConnector = require('./fieldConnector')
const appendInitialValues = require('./appendInitialValues')
const onSubmit = require('./onSubmit')

const update = ({ store, o }) => {

}

module.exports = (props) => {
  const { id, data, initialValues, title } = props
  const {
    forms,
  } = CLIENT_CONFIG
  const currentForm = forms[id]
  if (!currentForm) {
    throw new Error(`form ${id} is not defined in forms, check route props`)
  }
  const { mutationName, fields } = currentForm
  const mutate = data[mutationName]
  if (!mutate) {
    throw new Error(`mutate ${mutationName} is not in the queryList`)
  }
  const withInitial = initialValues
    ? appendInitialValues({ fields, initialValues })
    : fields
  const fieldsConnected = fieldConnector({ withInitial, data })
  const initialId = initialValues
    ? initialValues.id
    : null
  const formProps = {
    id,
    loaded: 'true',
    className: id,
    fields: fieldsConnected,
    title: title || currentForm.title,
    onSubmit: onSubmit({ mutate, update, mutationName, id: initialId })
  }
  return formProps
}