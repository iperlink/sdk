const div = require('../div')
const image = require('../image')

module.exports = () => {
  const img = image({
    id: 'lostDataImage',
    source: LOGO_URL
  })
  return img
}