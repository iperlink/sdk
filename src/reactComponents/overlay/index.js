const createElement = require('../../renderer')

module.exports = () => {
  const overlay = createElement({
    props: {
      id: 'overlay',
      className: 'overlayVisible'
    },
  })
  return overlay
}