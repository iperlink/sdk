const { Toggle } = require('office-ui-fabric-react/lib/Toggle')
const createElement = require('../../renderer')

module.exports = ({ switchData, checked, addSelected, iterator = 1 }) => {
  const tg = createElement({
    type: Toggle,
    props: {
      checked,
      id: `switch_${iterator}`,
      onChanged: () => addSelected(switchData),
    }
  })
  return tg
}
