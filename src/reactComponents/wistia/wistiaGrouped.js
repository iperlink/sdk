const wistiaRow = require('./wistiaRow')
const createElement = require('../../renderer')
const mapValues = require('../../objMapper/mapValues')

module.exports = props => {
  const {
    id,
    className,
    range,
    obj,
  } = props
  const mapped = mapValues({
    obj, fn: (value, key) => {
      const params = {
        list: value,
        range,
        className,
        id: `${id}${key}`,
        title: key,
      }
      return wistiaRow(params)
    }
  })
  const children = Object.values(mapped)
  const row = createElement({
    props: {
      id: `${id}_obj_container`,
      className: `${className}_obj_container`,
    },
    children
  })

  return row
}
