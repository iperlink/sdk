const createElement = require('../../renderer')
const script = require('../script')

const styleObj = {
  screeLarge: {
    w: 400
  },
  screeReg: {
    w: 200
  },
  celSmall: {
    w: 250
  }
}
module.exports = props => {
  const {
    iterator,
    src,
    id,
    range
  } = props
  const { w } = styleObj[range]
  script({
    src: 'https://fast.wistia.com/embed/medias/0fc2hrq1u5.jsonp'
  })
  script({
    src: 'https://fast.wistia.com/assets/external/E-v1.js'
  })
  const Img = createElement({
    type: 'span',
    props: {
      id: `wistia${id}${iterator}`,
      className: `wistia_vid wistia_embed wistia_async_${src} popover=true popoverAnimateThumbnail=true`,
      style: {
        display: 'inline-block',
        height: `${parseInt(w * 0.65)}px`,
        position: 'relative',
        width: `${parseInt(w)}px`,
      }
    }
  })
  return Img
}
