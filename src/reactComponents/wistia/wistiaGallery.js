const card = require('./wistiaCard')
const createElement = require('../../renderer')

module.exports = props => {
  const {
    list,
    id,
    className,
    range
  } = props
  const gallery = list.map((w, iterator) => {
    const element = {
      ...w,
      range,
      id,
      iterator,
      className,
    }
    const renderedCard = card(element)
    return renderedCard
  })
  const galleryDiv = createElement({
    props: {
      id: `${id}_container`,
      className: `${className}_container`,
    },
    children: gallery
  })

  return galleryDiv
}
