const card = require('../card')
const wistia = require('./index')

module.exports = props => {
  const {
    id,
    iterator,
    subTitles,
    title,
    className,
    range,
    src
  } = props
  const wistiaImg = wistia({ iterator, src, range, id })
  const cardDiv = card({
    body: wistiaImg,
    id: `${id}Card`,
    iterator,
    subTitles,
    title,
    className,
  })
  return cardDiv
}
