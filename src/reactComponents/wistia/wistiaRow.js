const gallery = require('./wistiaGallery')
const createElement = require('../../renderer')

module.exports = props => {
  const {
    list,
    title,
    id,
    className,
    range
  } = props

  const titleDiv = createElement({
    props: {
      id: `${id}_title`,
      className: `${className}_title`,
    },
    children: title
  })
  const galleryDiv = gallery({
    list,
    id,
    className,
    range
  })
  const row = createElement({
    props: {
      id: `${id}_container`,
      className: `${className}_row`,
    },
    children: [titleDiv, galleryDiv]
  })

  return row
}
