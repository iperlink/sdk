const { Component } = require('react')

const formCreator = require('../formCreator')
const tableCreator = require('../table')
const createElement = require('../../renderer')
const getProps = require('./getProps')
const button = require('../btn')

const userCan = () => true
const Editable = class Editable extends Component {
  state = {
    editing: false,
  }

  edit = (editingValues) => {
    this.setState({
      editing: true,
      editingValues
    })
  }

  showTable = () => {
    this.setState({
      editing: false,
      editingValues: {}
    })
  }

  render() {
    const {
      editing,
    } = this.state
    const canInsert = userCan()

    const btn = button({
      id: 'createElement',
      onClick: () => this.edit(),
      disabled: !canInsert,
      icon: 'FaFolderPlus'
    })
    const renderProps = getProps({
      props: this.props,
      state: this.state,
      fn: this.edit,
      afterSubmit: this.showTable
    })

    const el = editing
      ? formCreator
      : tableCreator
    const rendered = el(renderProps)
    return [btn, rendered]
  }
}

module.exports = (props) => {
  return createElement({
    type: Editable,
    props: props
  })
}