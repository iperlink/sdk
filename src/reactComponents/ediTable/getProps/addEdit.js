// const userCan = require('../../../localStorage/userCan')
const userCan = () => true

module.exports = ({ table, fn, filterBy, id }) => {
  const {
    editPrivilege,
    template,
  } = table
  const {
    keys
  } = template
  const can = userCan(editPrivilege)
  if (!can) {
    return table
  }
  const edit = {
    title: 'editar',
    hideMobile: true,
    specialRenderer: 'edit',
    fn
  }
  const newKeys = {
    ...keys,
    edit,
  }
  const newTemplate = {
    ...template,
    keys: newKeys,
  }
  const newTable = {
    ...table,
    filterBy,
    template: newTemplate,
    id: `editaTable${id}`
  }
  return newTable
}