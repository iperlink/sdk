const { Component } = require('react')
const getOptions = require('./getOptions')
const filterBar = require('../filterBar')
const createElement = require('../../renderer')
const div = require('../div')
const getTable = require('../table')

const fullTable = class FullTable extends Component {
  state = {
    editing: false,
    filterBy: false
  }

  submitFilter = (filterBy) => {
    this.setState({
      filterBy
    })
  }

  render() {
    const {
      id,
    } = this.props
    const {
      filterBy,
      selected,
    } = this.state
    const { table } = this.props
    if (!table) {
      throw new error('props table is required')
    }
    const firstData = table.data[0] || {}
    const filter = filterBar({
      data: firstData,
      options: getOptions(table),
      submit: this.submitFilter,
    });
    const editable = getTable({
      ...this.props,
      filterBy,
      addSelected: this.addSelected,
      selected
    })
    const container = div({
      id: `fulltableCont${id}`,
      className: 'fullTableContainer',
      children: [filter, editable]
    });
    return container
  }
}

module.exports = (props) => {
  return createElement({
    type: fullTable,
    props: props
  })
}