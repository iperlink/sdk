const createElement = require('../../renderer')

module.exports = ({ id, className, source }) => {
  const classN = className || 'simpleDiv'
  const container = createElement({
    type: 'img',
    props: {
      id,
      src: source,
      className: classN
    }
  })
  return container
}
