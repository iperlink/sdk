const createElement = require('../../renderer')
const section = require('./section')
const mapValues = require('../../objMapper/mapValues')
const closeMenu = require('../sideBar/closeIcon')

module.exports = (mainId = 'app') => {
  const obj = CLIENT_CONFIG.menu
  if (!obj) throw new Error('menu is not configured in client_config')
  const result = mapValues({
    obj, fn: (value, key, i) => {
      const inner = section({ head: key, content: value, mainId })
      return inner
    }
  })
  const menuChildren = [closeMenu()].concat(Object.values(result))
  const menu = createElement({
    props: {
      id: `${mainId}_menu`
    },
    children: menuChildren
  });
  return menu
}