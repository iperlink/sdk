const createList = require('./createList')
const createElement = require('../../../renderer')

module.exports = ({ content, head, test }) => {
  const list = createList({ content, test })
  const ul = createElement({
    type: 'ul',
    props: {
      className: 'menuSectionUl',
      id: `menuSectionUl_${head}`,
    },
    children: list
  })
  return ul
}