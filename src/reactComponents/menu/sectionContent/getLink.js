const createElement = require('../../../renderer')

module.exports = ({ el, elType, i }) => {
  const link = createElement({
    type: elType,
    props: {
      id: `menuSectionLiLink${el.title}${i}`,
      to: `/${el.path}`,
    },
    children: el.title
  })
  return link
}