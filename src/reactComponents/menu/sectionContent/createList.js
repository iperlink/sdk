const { Link } = require('react-router-dom')
const getLink = require('./getLink')
const getLi = require('./getli')

module.exports = ({ content, test }) => {
  const elType = test
    ? 'span'
    : Link
  const lis = content.map((el, i) => {
    const link = getLink({el, elType, i})
    const li = getLi({el, link , i})
    return li
  })
  return lis
}