const createElement = require('../../../renderer')
const hide = require('../../sideBar/hide')

module.exports = ({ el, i, link }) => {
  const li = createElement({
    type: 'li',
    props: {
      className: 'menuSectionLi',
      id: `menuSectionLi${el}${i}`,
      onClick: hide
    },
    children: link
  })
  return li
}