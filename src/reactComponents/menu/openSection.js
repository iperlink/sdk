const addClass = require('../../htmlHelpers/addClass')
const replaceClass = require('../../htmlHelpers/replaceClass')

module.exports = (id) => {
  replaceClass('menuSectionUlDeployed', 'menuSectionUl')
  addClass(id, 'menuSectionUlDeployed', 'menuSectionUl')
}
