const createElement = require('../../renderer')
const openSection = require('./openSection')

module.exports = ({ head }) => {
  const title = createElement({
    type: 'div',
    props: {
      className: 'menuSectionHead',
      id: `menuSectionHead_${head}`,
      onClick: () => openSection(`menuSectionUl_${head}`)
    },
    children: head
  })
  return title
}