const createElement = require('../../renderer')
const sectionHead = require('./sectionHead')
const sectionContent = require('./sectionContent/index.js')

module.exports = (section) => {
  const head = sectionHead(section)
  const content = sectionContent(section)

  const title = createElement({
    type: 'div',
    props: {
      className: 'menuSectionHead',
      id: `menuSectionHead_${section.head}`

    },
    children: [head, content]
  })
  return title
}