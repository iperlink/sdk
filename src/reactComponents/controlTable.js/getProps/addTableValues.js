const mapValues = require('../../../objMapper/mapValues')

module.exports = ({ editing, form, editingValues }) => {
  const result = editing
    ? mapValues({
      obj: form.fields, fn: (field, key) => {
        if (field.type === 'text') {
          return {
            ...field,
            value: editingValues[key]
          }
        }
        return field
      }
    })
    : {}
  return result
}