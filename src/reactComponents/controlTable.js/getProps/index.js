const addEdit = require('./addEdit')
const addTableValues = require('./addTableValues')

module.exports = ({ props, state, fn, afterSubmit }) => {
  const {
    form,
    editForm,
    id,
    table,
    filterBy,
  } = props
  const {
    editing,
    editingValues
  } = state
  if (editing) {
    const withTableValues = editingValues
      ? addTableValues({ editing, form, editingValues })
      : form.fields
    const objId = editingValues
      ? editingValues.id
      : null
    const newForm = {
      ...form,
      afterSubmit,
      fields: withTableValues,
      objId,
      id: `tableForm${form.id}`
    }
    return newForm
  }
  const tableProps = addEdit({ table, fn, filterBy, id })

  return tableProps
}


