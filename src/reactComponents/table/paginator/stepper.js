module.exports = ({ direction, current }) => {
  const newCurrent = direction === 1
    ? current + 1
    : current > 1
      ? current - 1
      : current
  return newCurrent
}
