const createElement = require('../../../renderer')
const btn = require('../../btn')

module.exports = props => {
  const { current, fn, id, filtered, chunk } = props
  const max = (current * chunk)
  const nextBtn = btn({
    className: ' table_paginator_rightArrow ',
    id: `${id}_arrow_right`,
    onClick: () => fn(1),
    icon: 'FaAngleRight',
    disabled: max >= filtered
  })
  const prevBtn = btn({
    className: ' table_paginator_leftArrow ',
    id: `${id}_arrow_left`,
    onClick: () => fn(0),
    icon: 'FaAngleLeft',
    disabled: max === chunk
  })
  const currentDiv = createElement({
    props: {
      className: 'table_paginator_current',
      id: `${id}_paginator_center`,
    },
    children: current
  })
  const cell = createElement({
    props: {
      className: 'table_paginator_flex',
      id,
    },
    children: [prevBtn, currentDiv, nextBtn]
  })
  const container = createElement({
    props: {
      className: 'table_paginator_container',
      id,
    },
    children: cell
  })
  return container
}