const createElement = require('../../../renderer')
const cell = require('./cell')

module.exports = props => {
  const {
    id,
    iterator,
    keys,
    rowData,
    tableClass,
    styleObjRange,
  } = props
  const style = { gridTemplateColumns: styleObjRange.row }
  const cells = rowData.map((cellData, i) => {
    const key = keys[cellData.key]
    return cell({ cellData, id, iterator: i, tableClass, key, styleObjRange })
  })
  const row = createElement({
    props: {
      className: `${tableClass}_row`,
      style,
      id: `${id}${iterator}`,
    },
    children: cells
  })


  return row
}


