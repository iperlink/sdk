const createElement = require('../../../renderer')

module.exports = props => {
  const { cellData, id, iterator, tableClass, styleObjRange, key } = props
  const { header, hideMobile } = key

  const headerClass = header ? "header" : "sub"
  const hideMobileClass = hideMobile ? "hideMobile" : ""
  const gridColumn = header ? "1/4" : `${iterator}/${iterator + 1}`

  const styles = styleObjRange.compact
    ? {
      gridTemplateColumns: styleObjRange[headerClass],
      gridColumn
    }
    : { gridTemplateColumns: styleObjRange[headerClass] }
  const cell = createElement({
    props: {
      className: `${tableClass}_cell ${hideMobileClass} ${headerClass} gC${gridColumn} word_break`,
      id: `${id}${iterator}`,
      style: styles,
    },
    children: cellData.label
  })
  return cell
}
