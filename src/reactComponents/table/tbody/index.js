const createElement = require('../../../renderer')
const row = require('./row')
const requiredKeys = require('../../../objMapper/requiredKeys')

module.exports = props => {
  requiredKeys({ obj: props, required: ['data', 'id', 'keys', 'className', 'styleObjRange'] })
  const {
    className,
    data,
    id,
    styleObjRange,
    keys,
  } = props
  if (!data) throw new Error('data is required')
  const rows = data.map((rowData, iterator) => {
    return row({
      id,
      iterator,
      rowData,
      keys,
      styleObjRange,
      tableClass: className,
    })
  })
  const table = createElement({
    type: 'div',
    props: {
      className,
      id: `${id}_tbody`,
    },
    children: rows
  })

  return table
}


