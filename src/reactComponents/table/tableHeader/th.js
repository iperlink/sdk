const createElement = require('../../../renderer')

module.exports = props => {
  const { id, iterator, tableClass, key } = props
  const cell = createElement({
    type: 'div',
    props: {
      className: `${tableClass}_header_cell `,
      id: `${id}${iterator}`,
    },
    children: key.title
  })
  return cell
}