const createElement = require('../../../renderer')
import th from './th'

module.exports = props => {
  const {
    headerData,
    id,
    keys,
    inRange,
    styleObjRange,
    tableClass,
  } = props
  if (inRange === 'celSmall') {
    return null
  }
  const style = { gridTemplateColumns: styleObjRange.row }
  const cells = headerData.map((cellData, iterator) => {
    const key = keys[cellData.key]
    return th({ id, iterator, tableClass, key, styleObjRange })
  })
  const row = createElement({
    type: 'div',
    props: {
      className: `${tableClass}_row`,
      id: `${id}_header`,
      style,
    },
    children: cells
  })
  return row
}