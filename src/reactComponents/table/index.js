const alertDataError = require('../errorAlert/alertDataError')
const { Component } = require('react')
const createElement = require('../../renderer')
const table = require('./table')
const addTableFunctions = require('./addTableFunctions')
const filterBar = require('../filterBar')
const getOptions = require('../../dataMappers/getOptions')

const TableContainer = class TableContainer extends Component {

  state = {
    filterBy: false,
  }

  submitFilter = (filterBy) => {
    this.setState({
      filterBy
    })
  }

  render() {
    const { id, data, changeComp } = this.props
    const {
      tables,
    } = CLIENT_CONFIG
    const currentTable = tables[id]
    if (!currentTable) {
      throw new Error(`table ${id} is not defined in tables, check route props`)
    }
    const tableData = data[currentTable.dataQuery]
    if (!tableData) {
      throw new Error(`data is not defined for table ${currentTable.dataQuery}`)
    }
    if (tableData.length !== 0 && tableData[0].error) {
      console.log(tableData[0])
      alertDataError(tableData)
      return {
        ...currentTable,
        data: []
      }
    }
    const { keys } = currentTable.template
    const remappedKeys = addTableFunctions(keys)
    const newTable = {
      ...currentTable,
      template: {
        ...currentTable.template,
        keys: remappedKeys
      }
    }
    const firstData = tableData[0] || {}
    const filter = filterBar({
      firstData: firstData,
      id,
      data,
      changeComp,
      options: getOptions(tableData),
      submit: (f) => { this.submitFilter(f) },
    });
    const result = {
      ...newTable,
      tableData: tableData,
      changeComp,
      filterBy: this.state.filterBy,
      data,
    }
    const rendered = table(result)
    const container = createElement({
      props: { id: `tableContainer${id}`, className: 'fullTableContainer' },
      children: [filter, rendered]
    })

    return container
  }
}

module.exports = (props) => {
  return createElement({
    type: TableContainer,
    props: props
  })
}