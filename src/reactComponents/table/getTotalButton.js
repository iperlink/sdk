const button = require('../btn')

module.exports = ({ total, fn }) => {
  const totalButton = total > 0
    ? button({
      id: 'sendSelected',
      message: `Enviar ${total} seleccionados`,
      onClick: fn
    })
    : null
  return totalButton
}