//const userCan = require('../../../localStorage/userCan')
const userCan = () => true

module.exports = (keys) => {
  const resultObj = {}
  Object.keys(keys).forEach(key => {
    const currentValue = keys[key]
    const { privilege } = currentValue
    if (!privilege) {
      resultObj[key] = currentValue
      return
    }
    if (userCan(privilege)) {
      resultObj[key] = currentValue
    }
    return
  })
  return resultObj
}