const dataMapper = require('../../dataMappers/table')
const tableFilterer = require('../../dataMappers/tableFilterer')

module.exports = (props) => {
  const {
    addSelected,
    chunk,
    data,
    filterBy,
    keys,
    page,
    selected,
    tableData,
    changeComp,
  } = props
  const rawData = tableFilterer({ tableData, page, filterBy, chunk })
  const mappedData = dataMapper({
    addSelected,
    data,
    changeComp,
    keys,
    rawData: rawData.paged,
    selected,
  })
  const result = {
    ...rawData,
    mappedData,
  }
  return result
}

