const { Component } = require('react')
const createElement = require('../../renderer')
const tbody = require('./tbody')
const theader = require('./tableHeader')
const tpaginator = require('./paginator')
const stepper = require('./paginator/stepper')
const emptyData = require('../emptyData')
const getScreenRange = require('../../../src/htmlHelpers/getScreenRange')
const getData = require('./getData')
const getTotalButton = require('./getTotalButton')
const togglePush = require('../../objMapper/togglePush')


const Table = class Table extends Component {
  state = {
    page: 1,
    selected: [],
  }

  handlePageChange = async (direction) => {
    const current = this.state.page
    const newCurrent = stepper({ direction, current })
    this.setState({
      page: newCurrent
    })
  }

  addSelected = (value) => {
    const newSelected = togglePush({
      array: this.state.selected,
      value: value.id
    })
    this.setState({
      selected: newSelected
    })
  }

  sendSelected = () => {
    console.log(this.state.selected)
  }

  render() {
    const {
      chunk = 10,
      className,
      tableData,
      data,
      filterBy,
      id,
      paginate,
      selected,
      template,
      changeComp,
    } = this.props
    const totalButton = getTotalButton({
      total: this.state.selected.length,
      fn: this.sendSelected,
    })
    const inRange = getScreenRange()
    const { keys, styleObj } = template
    const styleObjRange = styleObj[inRange]
    const { page } = this.state
    const {
      mappedData,
      filtered,
    } = getData({
      addSelected: this.addSelected,
      chunk,
      tableData,
      changeComp,
      filterBy,
      data,
      keys,
      page,
      selected: this.state.selected,
    })
    if (filtered === 0) {
      return emptyData()
    }
    const body = tbody({
      className,
      data: mappedData,
      id,
      keys,
      styleObjRange,
    })
    const header = theader({
      headerData: mappedData[0],
      styleObj,
      id: `${id}_header`,
      inRange,
      styleObjRange,
      tableClass: className,
      keys,
    })

    const paginator = paginate
      ? tpaginator({
        id: `${id}_paginator`,
        fn: this.handlePageChange,
        current: this.state.page,
        filtered,
        chunk,
      })
      : null
    const table = createElement({
      type: 'div',
      props: {
        className: `${className}_container`,
        id,
      },
      children: [header, body, paginator, totalButton]
    })
    return table
  }
}


module.exports = (props) => {
  return createElement({
    type: Table,
    props: props
  })
}