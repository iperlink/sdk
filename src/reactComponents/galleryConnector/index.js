const galleryTypes = require('./galleryTypes')

module.exports = (props) => {
  const { data, queryName, type, groupedBy, id } = props
  const QueryName = queryName || video
  if (data.loading || data.error) {
    return null
  }
  if (!type) {
    throw new Error('must include gallery type')
  }
  const currentData = data[QueryName]
  if (!currentData) {
    throw new Error(`gallery ${QueryName} has no current data`)
  }
  const gallery = galleryTypes[type]({
    data: currentData,
    groupedBy,
    id,
  })
  return gallery
}
