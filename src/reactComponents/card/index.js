const createElement = require('../../renderer')
const list = require('./list')

module.exports = props => {
  const {
    body,
    id,
    iterator,
    subTitles,
    title,
    className,
  } = props
  const titleDiv = createElement({
    type: 'div',
    props: {
      id: `${id}_${iterator}_title`,
      className: `${className}_card_title`,
    },
    children: title
  })
  const elements = JSON.parse(subTitles)
  const subTitleList = list({ elements, id, className })
  const divArr = [body, titleDiv].concat(subTitleList)
  const card = createElement({
    type: 'div',
    props: {
      id: `${id}_${iterator}_card`,
      className: `${className}_card`,
    },
    children: divArr
  })

  return card
}
