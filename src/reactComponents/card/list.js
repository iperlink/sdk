const createElement = require('../../renderer')

module.exports = ({ elements, id, className }) => {
  const subtitles = elements.map((element, i) => {
    const subtitle = createElement({
      type: 'div',
      props: {
        id: `subtitle${id}${i}`,
        className: `subtitle_${className}`,
      },
      children: element
    })
    return subtitle
  })

  return subtitles
}