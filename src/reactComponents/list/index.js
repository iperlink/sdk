const createElement = require('../../renderer')

module.exports = (props) => {
  const { data } = props
  const nodes = data.map(prop => {
    return createElement('li', null, prop.name)
  }
  )
  const ul = createElement('ul', null, nodes)
  return ul
}
