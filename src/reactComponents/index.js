const createElement = require('../renderer')
const wrapper = require('./wrapper')
const form = require('./formCreator')
const table = require('./table')
const gallery = require('./galleryConnector')
const dummyCLick = require('./dummys/divOnclick')
const loader = require('./loader')
const mapPoint = require('./mapPoint')
const mapPolygon = require('./mapPolygon')

const plain = (props) => {
  const { id, name, paramFromRoute, queryName, data } = props
  if (data && data.error) {
    console.Error(data.error)
  }
  const Mydata = data && data[queryName]
    ? data[queryName].map(d => d.name).join()
    : ''
  const div = createElement({
    props: {
      id,
      className: id,
    },
    children: [name, paramFromRoute, Mydata]
  })
  return div
}

const componentList = {
  dummyCLick,
  form,
  loader,
  plain,
  gallery,
  wrapper,
  table,
  mapPoint,
  mapPolygon,
  //controlTable,
}

const componentRenderer = (name) => {
  const current = componentList[name]
  if (!current) {
    throw new Error(`component ${name} is not defined`)
  }
  const toRender = (props) => {
    if (props.loaded) {
      return current(props)
    }
    const loading = loader(props.data)
    if (loading) return loading
    return current(props)
  }
  return toRender
}

module.exports = componentRenderer
