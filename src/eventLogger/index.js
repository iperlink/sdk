const insert = require('../mysqlUtils/insert')

exports.handler = async ({operation, user, values}) => {
  try {
    await insert({
      db: process.env.DB_NAME,
      table: 'error',
      values: {
        user,
        operation,
        values: JSON.stringify(values),
      }
    })
  } catch (e) {
    console.error(e)
  }
}
