module.exports = ({ params, args, Q }) => {
  if (params) {
    const paramsNames = params.map(p => p.name)
    if (!args) throw new Error(`
      query or mutation ${Q} has no args in backend,
      but has params in frontend
    `)
    paramsNames.forEach(P => {
      if (!args[P]) {
        throw new Error(`
          param ${P} is not defined in query ${Q} as args
        `)
      }
    })
  }
}