const compareParams = require('./compareParams')
const compareFields = require('./compareFields')
const capitalize = require('../../../strings/capitalize')

module.exports = ({ front, schema, types }) => {
  Object.keys(front).forEach(Q => {
    if (front[Q].raw.fields) {
      if (!schema[Q]) {
        throw new Error(` Apollo Query or Mutation ${Q} is not defined in backend`)
      }
      const { raw } = front[Q]
      const { params, fields } = raw
      const { args, type } = schema[Q]
      const Type = type || capitalize(Q)
      compareParams({ args, params, Q })
      compareFields({ fields, types, type: Type, Q })
    }
  })
}
