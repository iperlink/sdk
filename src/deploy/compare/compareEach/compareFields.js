

module.exports = ({ fields, types, type, Q }) => {
  const currentType = types[type]
  if (!currentType) {
    throw new Error(`query in front ${Q} has a type ${type} not defined in types of back`)
  }
  const typeKeys = Object.keys(currentType)
  if (fields) {
    fields.forEach(P => {
      if (!typeKeys.includes(P)) {
        throw new Error(` field ${P} is not defined in types of query in front ${Q}`)
      }
    })
  }
}