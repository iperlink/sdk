const compareEach = require('./compareEach')

module.exports = (containerFolder) => {
  const querysFrontFn = require(`${containerFolder}/front/querys`)
  const querysFront = querysFrontFn()
  const querysBack = require(`${containerFolder}/schema/querys`)
  const mutationsBack = require(`${containerFolder}/schema/mutations`)
  const types = require(`${containerFolder}/schema/types`)
  const schema = {
    ...querysBack,
    ...mutationsBack,
  }
  const front = querysFront.querys
  compareEach({ front, schema, types })
  return {
    back: schema,
    front,
  }
}
