
module.exports = ({ service, AWS_REGION, AWS_ACCOUNT }) => {
  const result = {
    Type: 'AWS::IAM::Role',
    Properties: {
      RoleName: `Lambda-${service}ServiceRole`,
      AssumeRolePolicyDocument: {
        Version: '2012-10-17',
        Statement: [{
          Effect: 'Allow',
          Principal: {
            Service: 'appsync.amazonaws.com',
          },
          Action: 'sts:AssumeRole'
        }]
      },
      Policies: [{
        PolicyName: `Lambda-${service}-Policy`,
        PolicyDocument: {
          Version: '2012-10-17',
          Statement: {
            Effect: 'Allow',
            Action: [
              'lambda:invokeFunction',
              'secretsmanager:*',
              "cognito-idp:*",
              "cognito-identity:*"
            ],
            Resource: [
              `arn:aws:lambda:${AWS_REGION}:${AWS_ACCOUNT}:*`
            ]
          }
        }
      }]
    }
  }
  return result
}