

module.exports = ({ materializeTables, schema }) => {
  const { rawTypes } = schema
  Object.keys(rawTypes).map(type => {
    const currentTable = materializeTables[type.toLowerCase()]
    if (currentTable) {
      const currentFields = Object.keys(currentTable.fields)
      const currentType = Object.keys(rawTypes[type])
      currentType.forEach(ct => {
        if (ct !== 'error' && !currentFields.includes(ct)) {
          throw new Error(`
            Field ${ct} is not included in materializedTable ${type}
          `)
        }
      })
    }
  })
}

