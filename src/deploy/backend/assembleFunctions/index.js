const mapValues = require('../../../objMapper/mapValues')
const mapper = require('./mapper')

module.exports = functions => {
  const assembled = mapValues({
    obj: functions,
    fn: (value, name) => {
      return mapper({ name, env: value.env })
    }
  })
  return assembled
}
