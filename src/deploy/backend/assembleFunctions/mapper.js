module.exports = ({ name, env }) => {
  console.log({ env })
  const result = {
    handler: `src/${name}.handler`,
    environment: env
  }
  return result
}