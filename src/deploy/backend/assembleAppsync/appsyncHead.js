
module.exports = ({ API_NAME, AWS_REGION, poolId }) => {
  const result = {
    name: API_NAME,
    authenticationType: 'AMAZON_COGNITO_USER_POOLS',
    userPoolConfig: {
      awsRegion: AWS_REGION,
      defaultAction: 'ALLOW',
      userPoolId: poolId
    },
    schema: 'schema.graphql',
    serviceRole: 'AppSyncTesterServiceRole'
  }
  return result
}
