const getAppsyncHead = require('./appsyncHead')
const schemaMapper = require('./schemaMapper')
const writeFile = require('../../../fs/writeFileSync')

module.exports = async ({
  env,
  schema,
  functionKeys
}) => {
  try {
    const appsyncHead = getAppsyncHead(env)
    const {
      dataSources,
      mappingTemplates,
    } = schemaMapper({ env, schema, functionKeys })
    const consolidated = {
      ...appsyncHead,
      mappingTemplates,
      dataSources
    }
    const resFileContent = '$util.toJson($context.result)'
    writeFile({ name: `mapping-templates/def-res.txt`, content: resFileContent })
    return consolidated
  } catch (e) {
    console.error(e);
    return { error: e.message }
  }
}