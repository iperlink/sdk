const writeFile = require('../../../../fs/writeFileSync')

module.exports = ({ field, key, functionKeys }) => {
  console.log({ field })
  const { dataSource, privilege, type, operation, omitNodes } = field;
  const Operation = operation || key
  if (type === 'Mutation' && !privilege) {
    throw new Error(`privilege, is required, not included in mutation ${key}`)
  }
  const defDataSource = type === 'Mutation'
    ? 'create'
    : 'find'
  const DataSource = dataSource || defDataSource
  if (!functionKeys.includes(DataSource)) {
    throw new Error(`Datasource ${DataSource} is not included in functions `)
  }
  const content = privilege
    ? `{
      "version": "2017-02-28",
      "operation": "Invoke",
      "payload": {
          "privilege": ${privilege},
          "operation": "${Operation}",
          "omitNodes": "${Operation}",
          "arguments":  $utils.toJson($context.arguments),
          "handle" : $utils.toJson($context.identity)
      }
    }`
    : `{
      "version": "2017-02-28",
      "operation": "Invoke",
      "payload": {
          "operation": "${Operation}",
          "arguments":  $utils.toJson($context.arguments),
          "handle" : $utils.toJson($context.identity)
      }
    }`

  const result = {
    field: key,
    type,
    dataSource: `${DataSource}Lambda`,
    request: `${key}-req.txt`,
    response: 'def-res.txt',
  }
  writeFile({ name: `mapping-templates/${key}-req.txt`, content })
  return result
}