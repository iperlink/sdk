const mapValues = require('../../../../objMapper/mapValues')

module.exports = (schema) => {
  const mappedMutations = mapValues({
    obj: schema.rawMutations,
    fn: (value) => {
      const result = {
        ...value,
        type: 'Mutation',
      }
      return result
    }
  })
  const mappedQuerys = mapValues({
    obj: schema.rawQuerys,
    fn: (value) => {
      const result = {
        ...value,
        type: 'Query',
      }
      return result
    }
  })

  const mappingTemplates = {
    ...mappedQuerys,
    ...mappedMutations,
  }
  return mappingTemplates
}