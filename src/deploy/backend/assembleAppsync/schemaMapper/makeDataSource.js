const makeArn = require('./makeArn')

module.exports = ({ field, service, AWS_REGION, AWS_ACCOUNT, stage }) => {
  const { dataSource, type } = field
  const defDataSource = type === 'Mutation'
    ? 'create'
    : 'find'
  const DataSource = dataSource || defDataSource
  const result = {
    type: 'AWS_LAMBDA',
    name: `${DataSource}Lambda`,
    description: `${DataSource} DataSource`,
    config: {
      lambdaFunctionArn: makeArn({
        name: DataSource,
        stage,
        type: 'lambda',
        service,
        AWS_REGION,
        AWS_ACCOUNT
      }),
      serviceRoleArn: makeArn({
        name: 'Lambda',
        stage,
        type: 'serviceRole',
        service,
        AWS_REGION,
        AWS_ACCOUNT
      })
    }
  }
  return result
}