const getArgs = require('../../../../objMapper/getArgs')

module.exports = ({ name, type, service, AWS_REGION, AWS_ACCOUNT }) => {
  const { stage } = getArgs()
  const stageName = stage || 'beta'
  const types = {
    lambda: `arn:aws:lambda:${AWS_REGION}:${AWS_ACCOUNT}:function:${service}-${stageName}-${name}`,
    serviceRole: `arn:aws:iam::${AWS_ACCOUNT}:role/Lambda-${service}ServiceRole`
  }
  const result = types[type]
  return result
}