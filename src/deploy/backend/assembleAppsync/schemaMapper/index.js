const mapValues = require('../../../../objMapper/mapValues')
const addType = require('./addType')
const makeMappingTemplate = require('./makeMapingTemplate')
const makeDataSource = require('./makeDataSource')

module.exports = ({ schema, env, functionKeys }) => {
  const withType = addType(schema)
  const mappingTemplates = []
  const dataSources = {}
  const mapp = mapValues({
    obj: withType,
    fn: (field, key) => {
      const makingTemplate = makeMappingTemplate({ field, key, functionKeys })
      mappingTemplates.push(makingTemplate)
      const dataSource = makeDataSource({ ...env, field })
      dataSources[dataSource.name] = dataSource
      return dataSource
    },
  })
  return {
    dataSources,
    mappingTemplates,
  }
}