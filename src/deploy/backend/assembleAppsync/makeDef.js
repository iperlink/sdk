const writeFile = require('../../fs/writeFile')
const fs = require('fs')

module.exports = () => {
  const content = `$util.toJson($context.result)`
  return fs.writeFileSync(`mapping-templates/def-res.txt`, content)
}