const checkErrors = require('../../mysqlUtils/checkError')

module.exports = async ({ views, env }) => {
  const { DB_NAME } = env
  const promiseArray = Object.values(views).map(view => {
    const query = view.query({ db: DB_NAME })
    return checkErrors(query)
  });
  await Promise.all(promiseArray)
}