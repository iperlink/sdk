const fs = require('fs')
const assembleFunctions = require('./assembleFunctions')
const assembleAppsync = require('./assembleAppsync')
const makeServiceRole = require('./makeServiceRole')
const checkViews = require('./checkViews')
const compareViewsToQuery = require('./compareViewsToQuery')
const start = require('../../serviceStarter/tester')

module.exports = async ({
  env,
  functions,
  schema,
  views,
  materializeTables,
}) => {
  await start({})
  await checkViews({ views, env })
  compareViewsToQuery({ materializeTables, schema })
  const assembledFunctions = assembleFunctions(functions)
  const functionKeys = Object.keys(assembledFunctions)
  const appSync = await assembleAppsync({ env, schema, functionKeys })
  if (appSync.error) {
    console.error(appSync.error)
    return false
  }
  const roles = makeServiceRole(env)
  console.log(roles)
  fs.writeFileSync('yml/templates/functions.json', JSON.stringify(assembledFunctions, null, 2))
  fs.writeFileSync('yml/templates/appSync.json', JSON.stringify(appSync, null, 2))
  fs.writeFileSync('yml/templates/roles.json', JSON.stringify(roles, null, 2))
  return true
}
