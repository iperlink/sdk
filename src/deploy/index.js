const backend = require('./backend')
const front = require('./front')
const compare = require('./compare')
const path = require('path')

module.exports = async ({
  functions,
  env,
  schema,
  views,
  materializeTables,
}) => {
  const containerFolder = path.join(process.cwd())
  const comparator = await compare(containerFolder)
  await front(containerFolder, comparator)
  await backend({
    containerFolder,
    env,
    functions,
    materializeTables,
    schema,
    views,
  })
  process.exit()
}
