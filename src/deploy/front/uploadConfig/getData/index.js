const path = require('path')
const appendDefault = require('./appendDefaults')

module.exports = async ({ functs, containerFolder }) => {
  const data = functs.reduce((acc, filename) => {
    const currName = path.join(containerFolder, 'front', filename)
    const curr = require(currName)
    const res = appendDefault[filename]
      ? appendDefault[filename](curr())
      : curr()
    const newRes = Object.assign(
      {},
      acc,
      res
    )
    return newRes
  }, {})
  return data
}
