
module.exports = ({ data, key }) => {
  const newId = data.id || key
  const newClass = data.className || 'simpleGrid'
  const newPaginate = data.paginate || true
  const result = {
    ...data,
    id: newId,
    className: newClass,
    paginate: newPaginate,
  }
  return result
}