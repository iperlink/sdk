const getIdClass = require('./getIdClass')

module.exports = ({ data, key }) => {
  const withIdClass = getIdClass({ data, key })
  return withIdClass
}