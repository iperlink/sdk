const mapValues = require('../../../../../objMapper/mapValues')
const tableDefaults = require('./tableDefaults')

module.exports = {
  tables: (data) => {
    const { tables } = data
    const maped = mapValues({
      obj: tables,
      fn: (value, key) => {
        const result = tableDefaults({ data: value, key })
        return result
      }
    })
    return { tables: maped }
  }
}
