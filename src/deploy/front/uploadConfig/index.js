const upload = require('../../../aws/s3/put')
const validateForms = require('./validateForms')
const validateRoutes = require('./validateRoutes')
const validateTables = require('./validateTables')
const getData = require('./getData')

module.exports = async ({ functs, bucket, name, containerFolder, comparator }) => {
  try {
    const data = await getData({ functs, containerFolder })
    const string = JSON.stringify(data)
    validateTables({ tables: data.tables, comparator })
    validateForms({ forms: data.forms, comparator })
    validateRoutes({ childs: data.childs, comparator, forms: data.forms, tables: data.tables })
    await upload({ data: string, bucket, name: `${name}.json` })
    return string
  } catch (e) {
    console.error(e)
  }
}
