const validateField = require('./validateField')

module.exports = ({ comparator, component, path, tables, forms }) => {
  const { mutationName, props } = component
  if (!props) throw new Error(`props is not defined in route ${path}`)
  const {
    queryNames
  } = props
  const {
    table,
    form
  } = props
  if (!form) throw new Error(`form prop is not defined in route ${path}`)
  if (!table) throw new Error(`table prop is not defined in route ${path}`)
  if (!queryName) throw new Error(`query name is not defined in route ${path}`)
  if (!mutationName) throw new Error(`mutationName is not defined in route ${path}`)
  const { front } = comparator
  const currentQueryFront = front[queryName]
  const currentMutationFront = front[mutationName]
  if (!currentQueryFront) {
    throw new Error(`${queryName} from path ${path} is not defined in querysFront`)
  }
  if (!currentMutationFront) {
    throw new Error(`${mutationName} from path ${path} is not defined in querysFront`)
  }
  const currentForm = forms[form]
  const currentTable = tables[table]
  if (!currentForm) throw new Error(`form ${form} in route ${path} is not defined in forms`)
  if (!currentTable) throw new Error(`table ${table} in route ${path} is not defined in tables`)
  validateField({
    fields: Object.values(currentForm),
    currentQueryFront,
    queryName,
    form,
    path,
  })
}