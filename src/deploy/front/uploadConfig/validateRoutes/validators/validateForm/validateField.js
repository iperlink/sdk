
module.exports = ({
  fields,
  currentQueryFront,
  queryName,
  form,
  path,
}) => {
  const { raw } = currentQueryFront
  fields.forEach(field => {
    const { options } = field
    if (options && options.from && !options.local) {
      if ((options.from !== queryName) && !raw[options.from]) {
        throw new Error(
          `
          ${options.from}
          in form ${form}
          is not defined in
          querys given to the route ${path}
          and queryName ${queryName}
          `
        )
      }
    }
  })
}