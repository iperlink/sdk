module.exports = ({ component, comparator, path }) => {
  const { props } = component
  const { id, queryName } = props
  const { front } = comparator
  const currentQuery = front[queryName]
  if (!currentQuery) {
    throw new Error(`
    table ${id} has query ${queryName}
    that is not defined in path ${path}
    `)
  }
}