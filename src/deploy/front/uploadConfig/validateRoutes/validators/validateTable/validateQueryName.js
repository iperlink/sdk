module.exports = ({ queryName, front, id }) => {
  const currentQuery = front[queryName]
  if (!currentQuery) {
    throw new Error(`table ${id} has query that is not defined in path ${path}`)
  }
}