const validateForm = require('./validateForm')
const validateTable = require('./validateTable')
const validateGallery = require('./validateGallery')

module.exports = {
  fullTable: ({ comparator, component, path, forms, tables }) => {
    // validateForm({ comparator, component, path, forms, tables })
    //validateTable({ comparator, component, path, forms, tables })
  },
  table: ({ comparator, component, path, forms, tables }) =>
    validateTable({ comparator, component, path, forms, tables }),
  controlTable: () => { },
  gallery: validateGallery
}