module.exports = ({ component, comparator, path }) => {
  const { queryName, props } = component
  const { id } = props
  const { front } = comparator
  const currentQuery = front[queryName]
  if (!currentQuery) {
    throw new Error(`table ${id} has query that is not defined in path ${path}`)
  }
}