const validators = require('./validators')

module.exports = ({ childs, comparator, tables, forms }) => {
  childs.forEach(child => {
    const { component, path } = child
    const { type } = component
    validators[type]({ comparator, component, path, tables, forms })
  })
}