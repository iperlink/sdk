
module.exports = ({ tables, comparator }) => {
  const { front } = comparator
  const tablesKeys = Object.keys(tables)
  const allowedSpecialrenderers = ['switch', 'date', 'formCreator', 'showMapPoint', 'showMapPolygon']
  tablesKeys.forEach(tkey => {
    const currentTable = tables[tkey]
    const { template, dataQuery } = currentTable
    const currentDataQuery = front[dataQuery]
    if (!currentDataQuery) {
      throw new Error(`table ${tkey} has dataquery ${dataQuery} that is undefined`)
    }
    const { fields } = currentDataQuery.raw
    if (!fields) {
      throw new Error(`table ${tkey} has dataquery ${dataQuery} that does not have fields`)
    }
    const { keys } = template
    const keyNames = Object.keys(keys)
    keyNames.forEach(kn => {
      const currentKeyName = keys[kn]
      const { specialRenderer } = currentKeyName
      const isSpecial = allowedSpecialrenderers.includes(specialRenderer)
      if (!fields.includes(kn) && !isSpecial) {
        throw new Error(`key ${kn} in table ${tkey} is not included in the associated query`)
      }
    })
  })
}