
module.exports = async ({ forms, comparator }) => {
  const {
    back,
    front,
  } = comparator
  Object.keys(forms).forEach(formKey => {
    const form = forms[formKey]
    const { fields } = form
    const fieldKeys = Object.keys(fields)
    fieldKeys.forEach(fieldKey => {
      const field = fields[fieldKey]
      const { options } = field
      if (options && !Array.isArray(options)) {
        const { from, local } = options
        const currentFormData = front[from]
        if (!currentFormData && !local) {
          throw new Error(
            `
            form ${formKey} have field ${fieldKey}
            with query ${from} that does not exist
            and is not local
            `
          )
        }
      }
    })
  })

}
