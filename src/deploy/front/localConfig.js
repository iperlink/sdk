const path = require('path')

module.exports = async (containerFolder) => {
  const functs = [
    'querys', 'menu', 'aws', 'childRoutes', 'masterRoute', 'forms', 'tables'
  ]
  const data = functs.reduce((acc, filename) => {
    const currName = path.join(containerFolder, filename)
    const curr = require(currName)
    const res = curr()
    const newRes = Object.assign(
      {},
      acc,
      res
    )
    return newRes
  }, {})
  return JSON.stringify(data)
}
