const uploadConfig = require('../../../src/deploy/front/uploadConfig')
const { start } = require('../../../src/serviceStarter')
const AWS = require('aws-sdk')

const fs = require('fs')
require('dotenv').config()

module.exports = async (containerFolder, comparator) => {
  try {
    await start({ dependencies: { AWS }, servicesList: ['s3', 'awsCreds'] })
    const functs = [
      'querys', 'menu', 'aws', 'childRoutes', 'masterRoute', 'forms', 'tables', 'errors'
    ]
    const string = await uploadConfig({
      functs,
      bucket: 'paneldecontrolcl',
      name: 'mainData',
      containerFolder,
      comparator
    })
    fs.writeFileSync(`${containerFolder}/front/config.json`, string)
  } catch (e) {
    console.error(e)
  }
}
