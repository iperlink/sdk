const create = require('react').createElement

module.exports = (params) => {
  const { type, props, children } = params
  const Type = type || 'div'
  const Key = props.id
  if (!Key) {
    console.log(props)
    console.log(params)
    throw new Error(`Component of withProps ${Object.keys(props)} does not have Key`)
  }
  try {
    if (!props) {
      throw new Error('createElement requires a props object')
    }
    const withKey = Object.assign({}, props, { key: `${Key}_key` })
    const rendered = create(Type, withKey, children)
    return rendered
  } catch (e) {
    console.info(e)
  }
}
