const {
  GraphQLString,
  GraphQLInt,
} = require('graphql')

module.exports = {
  string: { type: GraphQLString },
  'string!': { type: GraphQLString, required: true },
  int: { type: GraphQLInt },
  'int!': { type: GraphQLInt, required: true },
}