const {
  GraphQLList,
  GraphQLObjectType,
} = require('graphql')
const typeDict = require('../typeDict')
const mapValues = require('../../objMapper/mapValues')

module.exports = ({ rawMutation, key, types }) => {
  const { type, resultType } = rawMutation
  if (!type) throw new Error(`type is not defined in mutation ${key}`)
  const currentType = types[rawMutation.type];
  if (!currentType) throw new Error(`type ${rawMutation.type} is not defined in ${key}`)
  const res = resultType === 'list'
    ? GraphQLList(currentType)
    : currentType
  const result = {
    type: res,
    name: key,
    args: mapValues({
      obj: rawMutation.args,
      fn: (value) => {
        return typeDict[value]
      }
    })
  }
  return result
}
