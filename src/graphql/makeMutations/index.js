const {
  GraphQLObjectType,
} = require('graphql')
const mapValues = require('../../objMapper/mapValues')
const mapper = require('./mapper')

module.exports = ({ rawMutations, types }) => {
  const mutations = mapValues({
    obj: rawMutations,
    fn: (rawMutation, key) => {
      const result = mapper({ rawMutation, key, types })
      return result
    }
  })
  const compiled = new GraphQLObjectType({
    name: 'Mutation',
    fields: mutations
  })
  return compiled
}
