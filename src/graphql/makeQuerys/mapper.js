const {
  GraphQLList,
} = require('graphql')
const typeDict = require('../typeDict')
const mapValues = require('../../objMapper/mapValues')
const capitalize = require('../../strings/capitalize')

module.exports = ({ rawQuery, key, types }) => {
  const { type } = rawQuery
  const Type = type || capitalize(key)
  const currentType = types[Type];
  if (!currentType) throw new Error(`type ${key} is not defined`)
  const args = rawQuery.args
    ? mapValues({
      obj: rawQuery.args,
      fn: (value) => {
        return typeDict[value]
      }
    })
    : {}
  const result = {
    type: GraphQLList(currentType),
    name: key,
    args
  }
  return result
}
