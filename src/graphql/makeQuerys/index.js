const {
  GraphQLObjectType,
} = require('graphql')
const mapValues = require('../../objMapper/mapValues')
const mapper = require('./mapper')

module.exports = ({ rawQuerys, types }) => {
  const querys = mapValues({
    obj: rawQuerys,
    fn: (rawQuery, key) => {
      const result = mapper({ rawQuery, key, types })
      return result
    }
  })
  const compiled = new GraphQLObjectType({
    name: 'Query',
    fields: querys
  })
  return compiled
}
