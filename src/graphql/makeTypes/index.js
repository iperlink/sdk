const mapValues = require('../../objMapper/mapValues')
const mapper = require('./mapper')

module.exports = (rawTypes) => {
  const types = mapValues({
    obj: rawTypes,
    fn: (type, key) => {
      const result = mapper({ type, key })
      return result
    }
  })
  return types
};