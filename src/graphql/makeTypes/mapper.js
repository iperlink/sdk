const { GraphQLObjectType } = require('graphql')
const typeDict = require('../typeDict')
const mapValues = require('../../objMapper/mapValues')

module.exports = ({ type, key }) => {
  const result = {
    name: key,
    fields: mapValues({
      obj: type,
      fn: (value) => {
        return typeDict[value]
      }
    })
  }
  return new GraphQLObjectType(result)
}