const {
  GraphQLSchema,
  printSchema,
} = require('graphql')

const makeMutations = require('./makeMutations')
const makeQuerys = require('./makeQuerys')
const makeTypes = require('./makeTypes')

module.exports = ({
  rawMutations,
  rawQuerys,
  rawTypes,
}) => {
  const types = makeTypes(rawTypes)
  const query = makeQuerys({ rawQuerys, types })
  const mutation = makeMutations({ rawMutations, types })

  const schema = new GraphQLSchema({
    query,
    mutation,
  });
  const printed = printSchema(schema)
  return {
    printed,
    rawMutations,
    rawQuerys,
    rawTypes,
    schema,
  }
}

