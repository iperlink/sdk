module.exports = ({ name, fields, many, type }) => {
  const result = many
    ? `${name}: ${name}{${fields}}`
    : `${type} { ${name}{${fields}}}`
  return result
}
