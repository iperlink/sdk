const createSingle = require('./createSingleQuery')
const createMulti = require('./createMultiQuery')

module.exports = ({ raw, key, allQuerys }) => {
  const { fields, params, type } = raw
  if (fields) {
    return createSingle({ fields, params, key, type })
  }
  return createMulti({ raw, key, allQuerys, type })
}
