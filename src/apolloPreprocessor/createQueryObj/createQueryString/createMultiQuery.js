const mapValues = require('../../../objMapper/mapValues')
const createSingle = require('./createSingleQuery')

module.exports = ({ raw, key, allQuerys, type }) => {
  const mapped = mapValues({
    obj: raw,
    fn: (val, key) => {
      const current = allQuerys[val]
      if (!current) throw new Error(`query ${val} is not defined in allQuerys at front`)
      const { fields, params } = current
      return createSingle({ fields, params, key, many: true })
    }
  })
  const Type = type || 'query'
  const compiled = ` ${Type} { ${Object.values(mapped)} }`
  return compiled
}
