module.exports = ({ fields, name, params, many, type }) => {
  const paramsMap = params.map(f => {
    const result = f.type ? `$${f.name}: ${f.type}` : `$${f}: String`
    return result
  })
  const returned = params.map(f => {
    const result = f.type ? `${f.name}: $${f.name}` : `${f}: $${f}`
    return result
  })
  const queryString = many
    ? ` ${name}: ${name}(${paramsMap}):{${name}(${returned}){${fields}}}`
    : ` ${type} (${paramsMap}){${name}(${returned}){${fields}}}`
  return queryString
}
