const getParamString = require('./getParamString')
const getDefaultString = require('./getDefaultString')

module.exports = ({ fields, params, key, many, type }) => {
  if (params) {
    const result = getParamString({
      fields,
      name: key,
      params,
      many,
      type
    })
    return result
  }
  const defaultStr = getDefaultString({ fields, name: key, many, type })
  return defaultStr
}
