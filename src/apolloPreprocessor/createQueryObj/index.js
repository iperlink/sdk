const createQueryString = require('./createQueryString')
const mapValues = require('../../objMapper/mapValues')

module.exports = allQuerys => {
  const keyMapped = mapValues({
    obj: allQuerys,
    fn: (raw, key) => {
      const compiled = createQueryString({ raw, key, allQuerys })
      return { compiled, raw }
    }
  })
  return keyMapped
}
