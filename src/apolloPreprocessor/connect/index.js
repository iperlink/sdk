const { compose } = require('react-apollo')
//const compose = require('./compose')
const createElement = require('../../renderer')
const getRendered = require('./rendered')
const mutationMapper = require('./mutationMapper')
const queryConnector = require('./queryConnector')
const component = require('../../reactComponents/connectors')

module.exports = (props) => {
  const {
    queryName,
    mutationNames,
    id,
  } = props
  const compiledQuerys = queryConnector(queryName)
  const compiledMutations = mutationNames
    ? mutationNames.map((mutationName) => mutationMapper({
      mutationName,
      refetch: compiledQuerys.tag
    }))
    : []
  const rendered = getRendered({ props, component })
  const toConnect = compose(
    [compiledQuerys.connected].concat(compiledMutations)
  )
  const connected = toConnect(rendered)
  const result = createElement({
    type: connected,
    props: {
      id
    },
  })
  return result
}