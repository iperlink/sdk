const gql = require('graphql-tag')

module.exports = ({ querys, queryName }) => {
  const currentQuery = querys[queryName]
  if (!currentQuery) return null
  const Q = Object.values(currentQuery)[0]
  const tag = gql`${Q}`
  const preConnected = { query: tag }
  return preConnected
}