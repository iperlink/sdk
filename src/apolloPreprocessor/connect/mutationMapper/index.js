const { graphql } = require('react-apollo')
const gql = require('graphql-tag')

module.exports = ({ mutationName, refetch }) => {
  const {
    querys
  } = CLIENT_CONFIG
  const currentQuery = querys[mutationName]
  if (!currentQuery) throw new Error(` Mutation ${mutationName} is not defined`)
  const Q = Object.values(currentQuery)[0]
  const tag = gql`${Q}`
  const GqlProps = {
    name: mutationName,
    errorPolicy: 'all',
    optimisticResponse: false,
    options: {
      refetchQueries: () => {
        return [
          { query: refetch }
        ]
      },
      awaitRefetchQueries: true,

    }
  }
  const compiledMutation = graphql(
    tag,
    GqlProps,
  )
  return compiledMutation
}