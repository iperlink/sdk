const createElement = require('../../renderer')
const titleBar = require('../../reactComponents/titleBar')

module.exports = ({ props, state, component }) => {
  const result = (fromGraph) => {
    const concatedWithGraph = {
      ...props,
      ...fromGraph,
      ...state
    }
    const content = createElement({
      type: component,
      props: concatedWithGraph
    })
    const title = titleBar({ title: props.title })
    const master = createElement({
      props: { className: 'masterCont', id: 'masterCont' },
      children: [title, content]
    })
    return master
  }

  return result
}
