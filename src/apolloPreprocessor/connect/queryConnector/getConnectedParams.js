
module.exports = (name) => {
  const connectedParams = {
    name,
    fetchPolicy: 'cache-and-network',
    variables: { id: 1 },
    options: ownProps => {
      const fromRoute = ownProps.match
        ? ownProps.match.params
        : {}
      const variables = { ...fromRoute }
      const result = {
        fetchPolicy: 'cache-and-network',
        variables,
        errorPolicy: 'all',
      }
      return result
    }
  }
  return connectedParams
}