const { graphql } = require('react-apollo')
const gql = require('graphql-tag')
const getConnectedParams = require('./getConnectedParams')

module.exports = (queryName) => {
  const {
    querys
  } = CLIENT_CONFIG

  const currentQuery = querys[queryName]
  if (!currentQuery) throw new Error(`query ${queryName} is not defined`)
  const Q = Object.values(currentQuery)[0]
  const tag = gql`${Q}`
  const connectedParams = getConnectedParams()
  const connected = graphql(tag, connectedParams)
  const result = {
    tag,
    connected
  }
  return result
}