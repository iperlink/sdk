module.exports = (args) => {
  var funcs = [];
  for (var _i = 0; _i < args.length; _i++) {
    funcs[_i] = args[_i];
  }
  var functions = funcs.reverse();
  return function () {
    var args = [];
    for (var _i = 0; _i < args.length; _i++) {
      args[_i] = args[_i];
    }
    var firstFunction = functions[0], restFunctions = functions.slice(1);
    console.log({ firstFunction })
    var result = firstFunction.apply(null, args);
    restFunctions.forEach(function (fnc) {
      result = fnc.call(null, result);
    });
    return result;
  };
}