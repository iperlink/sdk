module.exports = (email) => {
  var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  const validEmail = re.test(String(email).toLowerCase());
  if (!validEmail) {
    throw new Error(JSON.stringify({
      code: 'emailInvalid',
      message: 'email is invalid'
    }))
  }
}
