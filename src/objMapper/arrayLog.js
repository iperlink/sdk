module.exports = ({ array, name }) => {
  const l = array.length
  if (l > 8) {
    return {
      name,
      first: array[0],
      last: array[l - 1],
      length: l
    }
  }
  else {
    console.error(JSON.stringify(array, null, 2))
  }
}