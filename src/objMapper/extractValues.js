module.exports = ({obj, value}) => {
  const result = {}
  const keys = Object.keys(obj)
  const L = keys.length
  for(let i=0; i<L;i++){
    result[keys[i]] = obj[keys[i]][value]
  }
  return result
}