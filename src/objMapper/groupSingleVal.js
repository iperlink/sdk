module.exports = ({ array, key, val }) => {
  const result = array.reduce((acc, current) => {
    const currentKey = acc[current[key]]
    if (currentKey) {
      acc[current[key]].push(current[val])
      return acc
    }
    acc[current[key]] = [current[val]]
    return acc
  }, {})
  return result
}