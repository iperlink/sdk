module.exports = ({ obj, keyfn, valfn }) => {
  const keys = Object.keys(obj)
  const res = {}
  const l = keys.length
  for (let i = 0; i < l; i++) {
    const key = keyfn(keys[i], obj[keys[i]])
    const value = valfn(obj[keys[i]], keys[i])
    res[key] = value
  }
  return res
}