
module.exports = ({ array, by }) => {
  const acc = {}
  array.forEach(x => {
    if (!acc[x[by]]) {
      acc[x[by]] = x
    }
  })
  const values = Object.values(acc)
  return values
}