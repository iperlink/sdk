module.exports = ({ array, toAppend }) => {
  const result = array.map(obj => {
    return Object.assign({}, obj, toAppend)
  })
  return result
}