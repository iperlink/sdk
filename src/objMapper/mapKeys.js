module.exports = ({obj, fn}) => {
  const keys = Object.keys(obj)
  const res = {}
  const l = keys.length
  for (let i = 0; i < l; i++) {
    res[fn(keys[i], obj[keys[i]])] = obj[keys[i]]
  }
  return res
}