
module.exports = ({ array, fn }) => {
  const result = []
  array.forEach(element => {
    const ret = fn(element)
    if (ret) {
      result.push(ret)
    }
  })
  return result
}