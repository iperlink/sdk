
module.exports = ({ array, amount, offset }) => {
  const start = offset || 0
  const result = array.slice(start, amount);
  return result
}