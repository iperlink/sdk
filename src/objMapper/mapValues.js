module.exports = ({ obj, fn }) => {
  const keys = Object.keys(obj)
  const res = {}
  const l = keys.length
  for (let i = 0; i < l; i++) {
    res[keys[i]] = fn(obj[keys[i]], keys[i], i)
  }
  return res
}
