
module.exports = () => {
  const result = {}
  const args = process.argv.slice(2)
  args.forEach((val) => {
    const splitted = val.split(':')
    result[splitted[0]] = splitted[1]
  })
  return result
}