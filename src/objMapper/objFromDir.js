const path = require('path')
const fs = require('fs')

module.exports = pathName => {
  const currentPath = pathName || process.cwd()
  const files = fs.readdirSync(currentPath)
  const reduced = files.reduce((acc, curr) => {
    if (curr == 'index.js') {
      return acc
    }
    const name = curr.split('.')[0]
    const filename = path.join(pathName, curr)
    const obj = require(filename)
    acc[name] = obj
    return acc
  }, {})
  return reduced
}