module.exports(obj) => {
  const result = {}
  const keys = Object.keys(obj)
  keys.forEach(k => {
    if (obj[k] !== null) {
      result[k] = obj[k]
    }
  })
  return result
}