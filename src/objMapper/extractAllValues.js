module.exports = (array) => {
  const result = array.map(obj => {
    return Object.values(obj)
  })
  return result
}