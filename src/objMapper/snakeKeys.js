const snake = require('just-snake-case')

module.exports = (obj) => {
  if (!obj) return
  const keys = Object.keys(obj)
  const res = {}
  const l = keys.length
  for (let i = 0; i < l; i++) {
    res[snake(keys[i])] = obj[keys[i]]
  }
  return res
}