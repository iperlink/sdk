
module.exports = (array) => {
  const acc = {}
  array.forEach(x => {
    if (!acc[x]) {
      acc[x] = 1
    }
  })
  const keys = Object.keys(acc)
  return keys
}