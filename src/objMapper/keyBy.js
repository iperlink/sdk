module.exports = ({ array, key }) => {
  if (!array) throw new Error(JSON.stringify({
    code: 'arrayUndefinedKeyBy',
    message: 'array undefined at keyBy',
  }))
  const result = array.reduce((acc, current) => {
    acc[current[key]] = current
    return acc
  }, {})
  return result
}