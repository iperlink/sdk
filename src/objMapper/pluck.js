

module.exports = ({ array, key }) => {
  const result = array.map(element => {
    return element[key]
  })
  return result
}