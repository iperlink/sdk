module.exports = ({array, value}) => {
  const index = array.indexOf(value)
  if(index != -1) {
    return [].concat(
      array.slice(0, index),
      array.slice(index + 1))
  }
  return array.concat([value])
}