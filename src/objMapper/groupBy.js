module.exports = ({ array, key }) => {
  if (!array || !key) {
    throw new Error(JSON.stringify({
      code: 'arreyAndKEysRequiredGroupBy',
      message: 'array and keys are required'
    }))
  }
  const result = array.reduce((acc, current) => {
    const currentKey = acc[current[key]]
    if (currentKey) {
      acc[current[key]].push(current)
      return acc
    }
    acc[current[key]] = [current]
    return acc
  }, {})
  return result
}