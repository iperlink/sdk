module.exports = ({ obj, required, message }) => {
  let valid = true
  const mess = message || ''
  required.forEach(r => {
    if (!obj[r]) throw new Error(JSON.stringify({
      code: 'requiredKeys',
      message: `${r} is required ${mess}`
    }))
  })
  return valid
}