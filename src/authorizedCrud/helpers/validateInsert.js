const userCan = require('../../identicorp/user/userCan')
const validatePrivilegeTree = require('../../identicorp/privilege_tree/validatePrivilegeTree')


module.exports = async ({
  privilege,
  nodeId,
  username
}) => {
  const can = await userCan({ username, privilege })
  if (!can || can.error) {
    return can
  }
  if (!nodeId) return true
  const validatedPrivTree = await validatePrivilegeTree({
    privilegeId: privilege,
    nodes: nodeId
  })
  if (!validatedPrivTree) {
    return {
      error: 'noPrivTree'
    }
  }
  return true
}