const update = require('../../materialize/update')
const replaceContent = require('../../identicorp/content/replaceContent')
const validateInsert = require('../helpers/validateInsert')

module.exports = async ({
  forbiden,
  id,
  privilege,
  nodeId,
  table,
  username,
  values,
}) => {
  const can = await validateInsert({
    privilege,
    nodeId,
    username
  })
  if (!can) return false
  if (nodeId) {
    const content_node = {
      table,
      node_id: nodeId,
      content_id: id
    }
    await replaceContent(content_node)
  }
  const updated = await update({ id, table, values, views, forbiden })
  return updated
}