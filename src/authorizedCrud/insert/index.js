const insert = require('../../materialize/insert')
const replaceContent = require('../../identicorp/content/replaceContent')
const validateInsert = require('../helpers/validateInsert')
const { getServices } = require('../../serviceStarter')

module.exports = async ({
  table,
  values,
  privilege,
  nodeId,
  username
}) => {
  const Db = process.env.DB_NAME
  const { views } = getServices()
  const can = await validateInsert({
    privilege,
    nodeId,
    username
  })
  if (!can || can.error) {
    return can
  }
  const id = await insert({ table, values })
  const content_node = {
    node_id: nodeId,
    views,
    table,
    content_id: id,
  }
  await replaceContent(content_node)
  return id
}

