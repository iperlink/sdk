const find = require('../../mysqlUtils/find')
const userCan = require('../../identicorp/user/userCan')
const getUserNodes = require('../../identicorp/user/getUserNodes')
const getContent = require('../../identicorp/content/getContent')
const { getServices } = require('../../serviceStarter')

module.exports = async ({
  db,
  templateName,
  fields,
  queryObj,
  username,
  privilegeId,
  view,
  omitNodes,
  table
}) => {
  const can = await userCan({ username, privilege: privilegeId })
  if (!can || can.error) {
    console.error(`user ${username} not authorized with privilege ${privilegeId}`)
    return can
  }
  const userNodes = await getUserNodes({ user: can, privilege_id: privilegeId })
  console.log({ userNodes, omitNodes })
  const filtered = omitNodes
    ? false
    : await getContent({ userNodes, db, view })
  if (filtered && filtered.length === 0) {
    return []
  }
  const { findTemplates } = getServices()
  const template = findTemplates
    ? findTemplates[templateName]
    : null
  const found = await find({
    db: `${db}_materialized`,
    template,
    fields,
    queryObj,
    table,
    filtered
  })
  console.log(found)
  return found
}