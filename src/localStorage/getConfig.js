const getLocal = require('../../src/deploy/front/localConfig')

module.exports = async ({ key, endpoint, local }) => {
  if (local) {
    return getLocal({
      functs: [
        'querys', 'menu', 'aws', 'childRoutes', 'masterRoute', 'forms'
      ]
    })
  }
  const from = endpoint || STATIC_URL
  const newItem = await fetch(
    `${from}${key}`,
    { mode: "cors", method: "GET" }
  )
  if (newItem.json) {
    const json = await newItem.json()
    const string = JSON.stringify(json)
    return string
  }

}