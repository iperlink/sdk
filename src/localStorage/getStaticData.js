module.exports = async ({ key, endpoint, type }) => {
  const storage = (type === 'local')
    ? localStorage
    : sessionStorage
  const from = endpoint || STATIC_URL
  const item = await storage.getItem(key)
  if (!item) {
    const newItem = await fetch(
      `${from}${key}`,
      { mode: "cors", method: "GET" }
    )
    if (newItem.json) {
      const json = await newItem.json()
      storage.setItem(key, JSON.stringify(json))
      return json
    }
  }
  return item
}