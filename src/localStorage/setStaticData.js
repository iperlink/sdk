module.exports = async ({ key, endpoint, type }) => {
  const storage = (type === 'local')
    ? localStorage
    : sessionStorage
  const from = endpoint || STATIC_URL
  const newItem = await fetch(
    `${from}${key}`,
    { mode: "cors", method: "GET" }
  )
  const json = await newItem.json()
  storage.setItem(key, JSON.stringify(json))
  return json
}