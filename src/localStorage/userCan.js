
module.exports = async (priv) => {
  const userJson = localStorage.getItem('userData')
  const { privilegeArray } = JSON.parse(userJson)
  return privilegeArray.includes(priv)
}