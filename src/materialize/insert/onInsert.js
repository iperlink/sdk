const insertAggregate = require('../helpers/insertAggregate')
const hget = require('../../redisUtils/hget')
const setTableref = require('../helpers/setTableRef')
const { getServices } = require('../../serviceStarter')

module.exports = async ({ id, table }) => {
  const { views } = getServices()
  const db = process.env.DB_NAME
  if (!views || Object.keys(views).length === 0) {
    throw new Error(` views are not defined or empty`)
  }
  const currentTables = await hget({ name: `${db}_table_ref`, keys: table, fn: setTableref })
  if (!currentTables) {
    console.error(`${table} does not have any ref`)
  }
  const promiseArray = currentTables.map(async ctable => {
    const currentView = views[ctable]
    if (!currentView) {
      throw new Error(`${ctable} does not exist in views ${Object.keys(views)}`)
    }
    const queryFn = currentView.query
    const query = queryFn({ db, tables: currentView.tables, where: ` WHERE ${table}.id = ? ` })
    console.log(query)
    return insertAggregate({ db, table: ctable, id, query, currentView })
  })
  const found = await Promise.all(promiseArray)
  console.log('aggregated')
  return found
}
