const createAggregate = require('./createAggregate')
const replace = require('../../mysqlUtils/replace')

module.exports = async ({ db, table, id, query, currentView }) => {
  const values = await createAggregate({ id, query })
  if (values.length === 0) return table
  await replace({ db, table, values, currentView })
  return table
}