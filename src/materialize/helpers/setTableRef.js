const objectToHash = require('../../redisUtils/objectTohash')
const { getServices } = require('../../serviceStarter')

module.exports = async () => {
  const db = process.env.DB_NAME
  const { views } = getServices()
  if (!views || Object.keys(views).length === 0) {
    throw new Error(` views are not defined or empty`)
  }
  const querysKeys = Object.keys(views)
  const tables = querysKeys.reduce((acc, current) => {
    const cValue = views[current]
    const cTables = cValue.tables
    cTables.forEach(t => {
      if (acc[t]) {
        acc[t].push(current)
        return
      }
      acc[t] = [current]
    })
    return acc
  }, {})
  await objectToHash({ hash: `hash_${db}_table_ref`, obj: tables })
  return tables
}