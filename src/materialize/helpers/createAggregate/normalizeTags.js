const normalize = require('../../../strings/normalizeWithSpace')
const uniq = require('../../../objMapper/uniq')

module.exports = (objs) => {
  if (!objs[0].tags) return objs
  const mapped = objs.map(obj => {
    const { tags } = obj
    if (!tags) {
      return obj
    }
    const splitted = tags.split(',')
    const uniques = uniq(splitted)
    const normalized = uniques.map(x => normalize(x))
    const joined = normalized.join(',')
    const withTags = Object.assign(
      {},
      obj,
      { tags: joined }
    )
    return withTags
  })
  return mapped
}