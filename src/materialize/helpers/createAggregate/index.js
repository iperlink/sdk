const executeQuery = require('../../../mysqlUtils/query')
const normalizeTags = require('./normalizeTags')

module.exports = async ({ query, id }) => {
  try {
    const [found] = await executeQuery({ query, params: [id] })
    if (found.length === 0) return found
    const withTags = normalizeTags(found)
    return withTags
  } catch (e) {
    console.error(e)
  }
}
