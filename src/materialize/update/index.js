const update = require('../../mysqlUtils/update')
const onUpdate = require('./onUpdate')

module.exports = async ({ id, table, values, forbiden }) => {
  await update({ id, table, values, forbiden })
  await onUpdate({ id, table })
}