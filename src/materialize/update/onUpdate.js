const insertAggregate = require('../helpers/insertAggregate')
const assembleWhereUpdate = require('./assembleWhereUpdate')
const hget = require('../../redisUtils/hget')
const { getServices } = require('../../serviceStarter')


module.exports = async ({ id, table }) => {
  const db = process.env.DB_NAME
  const { views } = getServices()
  const currentViews = await hget({ name: `${db}_table_ref`, keys: table })
  const promiseArray = currentViews.map(async view => {
    const currentView = views[view]
    const queryFn = currentView.query
    const where = assembleWhereUpdate({ db, table })
    const query = queryFn({
      db,
      where
    })
    return insertAggregate({ db, table: view, id, query })
  })
  const found = await Promise.all(promiseArray)
  return found
}
