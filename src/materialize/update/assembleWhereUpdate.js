module.exports = ({ db, table }) => {
  const ref = `${table}.id`
  const where = ` WHERE ${db}.${ref} = ?`
  return where
}