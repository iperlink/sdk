const archive = require('../../mysqlUtils/update/archive')
const update = require('../../mysqlUtils/update')

module.exports = async ({ db, id, table, values }) => {
  const promiseArray = [
    update({ db, id, table, values }),
    archive({ db, id, table })
  ]
  await Promise.all(promiseArray)
  return true
}