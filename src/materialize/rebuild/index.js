const trim = require('../../strings/trim')
const { getServices } = require('../../serviceStarter')

module.exports = async (viewName, views, db) => {
  try {
    const currentView = views[viewName]
    if (!currentView) {
      throw new Error(`view ${viewName} does not exist`)
    }
    const { mysql } = getServices()
    await mysql.execute('SET FOREIGN_KEY_CHECKS = 0');
    const currentquery = currentView.query({ db })
    const columnBody = currentquery.split('SELECT')[1].split('FROM')[0].split('CONCAT_WS')
    const rawsCols = columnBody[0].split(',')
    rawsCols.pop()
    if (columnBody[1]) {
      rawsCols.push(' as tags')
    }
    const cols = []
    rawsCols.forEach(rc => {
      const splitter = rc.indexOf(' as ') !== -1
        ? 'as'
        : '.'
      const splited = rc.split(splitter)
      if (splited.length > 1 && !cols.includes(trim(splited[splited.length - 1]))) {
        cols.push(trim(splited[splited.length - 1]))
      }
    })
    const colsString = ` ${cols.join(',')} `
    const insertQuery = `
    INSERT into ${db}_materialized.${viewName} (
    ${colsString}
    )
    ${currentquery}
    `
    console.time('mysql rebuild query')
    await mysql.execute(` TRUNCATE ${db}_materialized.${viewName}`);

    await mysql.execute(insertQuery);
    await mysql.execute('SET FOREIGN_KEY_CHECKS = 1');
    console.timeEnd('mysql rebuild query')
  } catch (e) {
    console.log(e)
  }
}