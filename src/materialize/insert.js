const insert = require('../mysqlUtils/insert')
const onInsert = require('./insert/onInsert')

module.exports = async ({ table, values }) => {
  const { id } = await insert({ table, values })
  await onInsert({ table, id })
  return id
}