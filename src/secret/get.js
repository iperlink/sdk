const AWS = require('aws-sdk')

module.exports = (key) => {
  if (process.env[key]) {
    return process.env[key]
  }
  const endpoint = 'https://secretsmanager.us-east-1.amazonaws.com'
  const region = 'us-east-1'
  const client = new AWS.SecretsManager({
    endpoint: endpoint,
    region: region
  })
  return new Promise((resolve, reject) => {
    client.getSecretValue({ SecretId: key }, (err, data) => {
      if (err) {
        console.error(err)
        console.info(key)
      } else {
        const secret = (data.SecretString !== '')
          ? data.SecretString
          : data.SecretBinary
        console.info(secret)
        resolve(secret)
      }
    })
  })
}