const update = require('../../mysqlUtils/update')

module.exports = async ({ app, id }) => {
  const updated = await update({
    db: 'shared',
    table: 'app',
    values: app,
    id
  })
  if (updated.code) {
    throw new Error(JSON.stringify(updated))
  }
  return updated
}
