const insert = require('../../mysqlUtils/insert')

module.exports = async (app) => {
  const created = await insert({
    db: process.env.IDENTICORP_DB,
    table: 'app',
    values: { name: app },
  })
  if (created.code) {
    throw new Error(JSON.stringify(created))
  }
  return created
}
