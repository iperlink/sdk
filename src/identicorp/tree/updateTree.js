const update = require('../../mysqlUtils/update')

module.exports = async ({ tree, id }) => {
  const updated = await update({
    db: process.env.IDENTICORP_DB,
    table: 'tree',
    values: tree,
    id
  })
  if (updated.code) {
    throw new Error(JSON.stringify(updated))
  }
  return updated
}
