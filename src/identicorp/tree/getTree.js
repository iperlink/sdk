const find = require('../../mysqlUtils/find')

module.exports = async (company_id) => {
  const found = await find({
    db: 'shared',
    table: 'tree',
    queryObj: { company_id }
  })
  const result = found.map(x => ({ id: x.id, name: x.name }))
  return result
}
