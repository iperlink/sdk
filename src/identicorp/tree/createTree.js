const insert = require('../../mysqlUtils/insert')

module.exports = async (tree) => {
  const created = await insert({
    db: 'shared',
    table: 'tree',
    values: tree,
    refs: ['company'],
    requiredKeys: ['company_id', 'name']
  })
  if (created.code) {
    throw new Error(JSON.stringify(created))
  }
  return created
}
