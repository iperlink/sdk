const insert = require('../../mysqlUtils/insert')
const appendToHash = require('../../redisUtils/appendToHash')

module.exports = async (privilegeTree) => {
  const created = await insert({
    db: 'shared',
    table: 'privilege_tree',
    values: privilegeTree,
    refs: ['privilege', 'tree'],
    defaultValues: { status: 1 },
    requiredKeys: ['privilege_id', 'tree_id']
  })
  if (created.code) {
    throw new Error(JSON.stringify(created))
  }
  await appendToHash({
    hash: 'hash_privilegeTree',
    key: privilegeTree.privilege_id,
    val: privilegeTree.tree_id
  })
  return created
}
