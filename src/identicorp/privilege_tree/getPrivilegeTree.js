const hget = require('../../redisUtils/hget')
const rebuildPrivilegeTree = require('./rebuildPrivilegeTree')

module.exports = async (privilegeId) => {
  const db = process.env.IDENTICORP_DB
  const res = await hget({ name: 'privilegeTree', keys: privilegeId, db, fn: rebuildPrivilegeTree })
  return res
}
