const buildArrayHash = require('../../redisUtils/buildArrayHash')

module.exports = async () => {
  const db = process.env.IDENTICORP_DB
  await buildArrayHash({
    db,
    name: 'privilegeTree',
    table: 'privilege_tree',
    key: 'privilege_id',
    extract: 'tree_id'
  })
}
