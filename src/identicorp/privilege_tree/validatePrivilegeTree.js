const getPrivilegeTree = require('./getPrivilegeTree')

module.exports = async ({ privilegeId, nodes }) => {
  const privilegeTres = await getPrivilegeTree(privilegeId)
  const nodesArray = nodes.split(',')
  if (!privilegeTres) {
    throw new Error(`privilege ${privilegeId} has no tree`)
  }
  if (privilegeTres.length !== nodesArray.length) {
    console.error(`request must have ${privilegeTres} nodes, it was give, ${nodesArray}`)
    return false
  }
  return true
}
