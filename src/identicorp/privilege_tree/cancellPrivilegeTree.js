const changeStatusRef = require('../../mysqlUtils/changeStatus/changeStatusRef')
const removeFromHash = require('../../redisUtils/removeFromHash')

module.exports = async ({ privilege_id, tree_id }) => {
  const updated = await changeStatusRef({
    db: 'shared',
    table: 'privilege_tree',
    status: 2,
    ref: { privilege_id, tree_id }
  })
  if (updated.code) {
    throw new Error(JSON.stringify(updated))
  }
  await removeFromHash({
    hash: 'hash_privilegeTree',
    key: privilege_id,
    val: tree_id
  })
  return updated
}
