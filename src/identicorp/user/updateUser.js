const executeQuery = require('../../mysqlUtils/query')
const find = require('../../mysqlUtils/find')
const update = require('../../mysqlUtils/update')
const createUserPriv = require('./createUser/createUserPriv')
const createUserMasterNode = require('./createUser/createUserMasterNode')

module.exports = async ({ user, id, privileges, app_id, nodes }) => {
  const db = process.env.IDENTICORP_DB
  const app_privileges = await find({
    queryObj: { app_id },
    table: 'privilege'
  })
  const app_privileges_id = app_privileges.map(x => x.id)
  const query = `DELETE from ${db}.node_user_privilege WHERE user_id = ? and privilege_id in (?)`
  await executeQuery({ query, params: [id, app_privileges_id] })
  await update({
    table: 'user',
    db,
    values: user,
    id
  })
  const updated = await find({
    table: 'user',
    db,
    queryObj: { id }
  })
  await createUserPriv({ privileges, id, user: updated, nodes })
  await createUserMasterNode({ nodes, id, user: updated, privileges })
  return updated
}
