const insert = require('../../../mysqlUtils/insert')
const createUserPriv = require('./createUserPriv')
const insertCognito = require('./insertCognito')
const validateEnv = require('../../../../src/environment/validateVars')
const validateEmail = require('../../../objMapper/validateEmail')
const createUserMasterNode = require('./createUserMasterNode')
const validateRequired = require('../../../objMapper/requiredKeys')

module.exports = async ({ user, privileges, nodes }) => {
  validateEnv(['IDENTICORP_DB', 'USER_POOL_ID', 'AWS_REGION'])
  validateRequired({
    obj: { user, privileges, nodes },
    required: ['user', 'privileges'],
    message: 'at createUser'
  })
  validateEmail(user.email);
  await insertCognito(user)
  const res = await insert({
    table: 'user',
    values: user,
    db: process.env.IDENTICORP_DB
  })
  const id = res.id
  await createUserPriv({ privileges, id, user: res, nodes })
  await createUserMasterNode({ nodes, id, user: res, privileges })
  return res
}
