const multiInsert = require('../../../mysqlUtils/insert/multiInsert')
const remove = require('../../../mysqlUtils/delete')
const updateUserHash = require('./updateUserHash')
const validateRequired = require('../../../objMapper/requiredKeys')

module.exports = async ({ nodes, id, user, privileges }) => {
  validateRequired({
    obj: { nodes, id, user, privileges },
    required: ['nodes', 'id', 'user', 'privileges'],
    message: 'at createUserMasterNode called from createUser'
  })
  const db = process.env.IDENTICORP_DB
  const insertNode = []
  nodes.forEach(node => {
    insertNode.push({
      user_id: id,
      node_id: node
    })
  })
  const userNodes = await multiInsert({
    db,
    table: 'user_tree_node',
    values: insertNode,
    defaultValues: { status: 1 }
  })
  if (userNodes.code) {
    await remove({ db, table: 'user', id })
    throw new Error(JSON.stringify(userNodes))
  }
  await updateUserHash({ privileges, user, nodes })
}
