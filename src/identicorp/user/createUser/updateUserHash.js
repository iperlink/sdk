const hset = require('../../../redisUtils/hset')
const getTrees = require('../../tree/getTree')

module.exports = async ({ privileges, user, nodes }) => {
  const trees = await getTrees(user.company_id)
  const privilegeArray = privileges.map(x => {
    const privilege_id = x.privilege_id || x
    return privilege_id
  })
  const privilegeObj = privileges.reduce(((acc, curr) => {
    const currentPrivilege = curr.privilege_id || curr
    const nodeIds = curr.node_ids || nodes
    acc[currentPrivilege] = nodeIds
    return acc
  }), {})
  const userHash = Object.assign(
    {},
    user,
    { privilegeArray, privilegeObj, trees },
  )
  await hset({ name: 'user', key: user.email, doc: userHash })
}
