const { getServices } = require('../../../serviceStarter')

module.exports = async (user) => {
  const { cognito } = getServices()
  const params = {
    UserPoolId: process.env.USER_POOL_ID,
    Username: user.email,
    DesiredDeliveryMediums: [
      'EMAIL'
    ],
    ForceAliasCreation: false,
    MessageAction: 'SUPPRESS'
  }
  return new Promise((resolve, reject) => {
    cognito.adminCreateUser(params, function (err, data) {
      if (err) {
        console.error(err)
        reject(err)
      } else {
        resolve(data.User.Attributes[0].Value)
      }
    })
  })
}
