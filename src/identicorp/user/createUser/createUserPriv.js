const multiInsert = require('../../../mysqlUtils/insert/multiInsert')
const remove = require('../../../mysqlUtils/delete')
const updateUserHash = require('./updateUserHash')

module.exports = async ({ privileges, id, user, nodes }) => {
  const db = process.env.IDENTICORP_DB
  const privilege = []
  privileges.forEach(priv => {
    const nodeIds = priv.node_ids || nodes
    const privilege_id = priv.privilege_id || priv
    nodeIds.forEach(node_id => {
      privilege.push({
        user_id: id,
        privilege_id,
        node_id
      })
    })
  })
  const userPriv = await multiInsert({
    db,
    table: 'node_user_privilege',
    values: privilege,
    defaultValues: { status: 1 }
  })
  if (userPriv.code) {
    await remove({ db, table: 'user', id })
    throw new Error(JSON.stringify(userPriv))
  }
  await updateUserHash({ privileges, user })
}
