const getUser = require('./getUser')
const getNodesIds = require('../node/getNodesIds')
const getPrivilegeTree = require('../privilege_tree/getPrivilegeTree')

module.exports = async ({ user, privilege_id }) => {
  const foundUser = await getUser(user)
  console.log({ foundUser })
  if (!foundUser.id) {
    throw new Error(JSON.stringify({
      code: 'userNotExist',
      message: 'user does not exist'
    }))
  }
  if (privilege_id && !foundUser.privilegeArray.includes(privilege_id)) {
    return false
  }
  const currentPriv = privilege_id
    ? foundUser.privilegeObj[privilege_id]
    : Object.values(foundUser.privilegeObj)[0]
  const privilegeTrees = await getPrivilegeTree(currentPriv)
  if (!privilegeTrees) {
    throw new Error(JSON.stringify({
      code: 'privNoTree',
      message: `privilege ${currentPriv} has no tree`
    }))
  }
  const nodes = await getNodesIds(currentPriv)
  const result = {
    nodes,
    trees: privilegeTrees.length
  }
  return result
}

