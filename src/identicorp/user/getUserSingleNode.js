const getUser = require('./getUser')

module.exports = async ({ user, index }) => {
  const foundUser = await getUser(user)
  const result = Object.values(foundUser.privilegeObj)[0][index]
  return result
}