const find = require('../../mysqlUtils/find')
const execute = require('../../mysqlUtils/query')

module.exports = async ({ privilege_id, node_id }) => {
  const db = process.env.IDENTICORP_DB
  const template = {
    queryParams: {
      privilege_id: 'exact',
      node_id: 'exact',
    },
    table: 'node_user_privilege'
  }
  const nodeUserPrivilege = await find({
    table: 'node_user_privilege',
    db,
    queryObj: { privilege_id, node_id },
    template,
    fields: ['user_id']
  })
  const userIds = nodeUserPrivilege.map(x => x.user_id)
  const [users] = userIds.length > 0
    ? await execute({
      query: 'SELECT * FROM shared.user WHERE id in (?)',
      params: userIds
    })
    : [[]]
  return users
}
