const { getServices } = require('../../serviceStarter')

module.exports = async (user) => {
  const { cognito } = getServices()
  if (!process.env.USER_POOL_ID) {
    throw new Error(`USER_POOL_ID not setted in env`)
  }
  if (!user.email) {
    throw new Error(`Object user must contain email`)
  }
  const params = {
    UserPoolId: process.env.USER_POOL_ID,
    Username: user.email
  }
  return new Promise((resolve, reject) => {
    cognito.adminDeleteUser(params, function (err, data) {
      if (err) {
        resolve()
      } else {
        resolve(data)
      }
    })
  })
}
