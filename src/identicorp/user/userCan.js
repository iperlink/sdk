const getUser = require('./getUser')

module.exports = async ({ username, privilege }) => {
  if (!username) {
    return {
      error: 'noUserFound'
    }
  }
  const found = await getUser(username)
  if (!found) {
    console.error(`user ${username} not found`)
    return {
      error: 'noUserFound'
    }
  }
  if (!privilege) {
    console.error(`privilege is not defined`)
    return {
      error: 'noUserPriv'
    }
  }
  if (!found.privilegeArray) {
    console.error(`user ${username} does Not have privileges`)
    return {
      error: 'noUserPriv'
    }
  }
  if (found.privilegeArray.includes(privilege)) {
    return username
  }
  console.error(`user ${username} does not have privilege ${privilege}`)
  return {
    error: 'noUserPriv'
  }
}

