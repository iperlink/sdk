const find = require('../../mysqlUtils/find')
const groupBy = require('../../objMapper/groupBy')
const mapValues = require('../../objMapper/mapValues')
const hmset = require('../../redisUtils/hmset')
const uniq = require('lodash/uniq')
const delay = require('delay')

module.exports = async () => {
  const db = process.env.IDENTICORP_DB
  await delay(4000)
  const users = await find({
    table: 'user',
    db,
    fields: ['id, email', 'name', 'last_name', 'cognito_id']
  })
  const nodeUserPrivilege = await find({
    table: 'node_user_privilege',
    db,
    fields: ['user_id', 'node_id', 'privilege_id']
  })
  if (nodeUserPrivilege.error) {
    throw new Error(nodeUserPrivilege.error)
  }
  const grouped = groupBy({ array: nodeUserPrivilege, key: 'user_id' })
  const userArray = users.map(user => {
    const nodePriv = grouped[user.id]
    const groupedByPriv = groupBy({ array: nodePriv, key: 'privilege_id' })
    const privilegeArray = Object.keys(groupedByPriv).map(x => parseInt(x))
    const privilegeObj = mapValues({
      obj: groupedByPriv, fn: (groupPriv) => {
        const nodes = groupPriv.map(elem => {
          return elem.node_id
        })
        const uniques = uniq(nodes)
        return uniques
      }
    })
    console.log(privilegeObj)
    const res = Object.assign(
      {},
      user,
      { privilegeArray, privilegeObj }
    )
    return res
  })
  const params = []
  userArray.forEach(res => {
    params.push(res.cognito_id)
    params.push(JSON.stringify(res))
  })
  await hmset({ name: 'user', params })
}

