const hget = require('../../redisUtils/hget')
const rebuildUsersHash = require('./rebuildUsersHash')

module.exports = async (username) => {
  const user = await hget({ name: 'user', keys: username, fn: rebuildUsersHash })
  return user
}

