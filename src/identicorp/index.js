module.exports = {
  //Create
  createApp: require('./app/createApp'),
  createAppCompany: require('./app_company/createAppCompany'),
  createCompany: require('./company/createCompany'),
  createNode: require('./node/createNode'),
  createPrivilege: require('./privilege/createPrivilege'),
  createPrivilegeTree: require('./privilege_tree/createPrivilegeTree'),
  createTree: require('./tree/createTree'),
  createUser: require('./user/createUser'),

  //Update
  updateCompany: require('./company/updateCompany'),
  changeNodeName: require('./node/changeNodeName'),
  changeNodeParent: require('./node/changeNodeParent'),
}