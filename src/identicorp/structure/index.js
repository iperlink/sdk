require('dotenv').config()
const base = require('./base')
const createTables = require('../../mysqlUtils/createTables')
const start = require('../../serviceStarter/tester')

const tables = {
  app: {
    fields: {
      id: 'idS',
      name: 'char150'
    },
    uniques: ['name']
  },
  company: {
    fields: {
      id: 'idS',
      name: 'char150',
      rut: 'char50',
    },
    uniques: ['name', 'rut']
  },

  app_company: { isRef: true },
  node: {
    fields: {
      id: 'idInt',
      parent_node: 'Int',
      name: 'char150',
      description: 'char150',
    },
    refs: ['tree'],
    uniques: ['name&tree_id'],
  },
  node_user_privilege: { isRef: true },
  privilege: {
    fields: {
      id: 'idS',
      name: 'char50',
      description: 'char150'
    },
    uniques: ['name&app_id'],
    refs: ['app']
  },
  profile: {
    fields: {
      id: 'idS',
      name: 'char150',
    },
    uniques: ['name']
  },
  profile_privilege: { isRef: true },
  profile_user: { isRef: true },
  status: {
    omitBase: true,
    fields: {
      id: 'idT',
      name: 'char150',
      created_at: 'Int',
      version: 'T'
    }
  },
  updates: {
    fields: {
      id: 'idInt',
      comment: 'char150',
      tableName: 'char150',
      content: 'char500'
    }
  },
  profile: {
    fields: {
      id: 'idInt',
      name: 'char150',
    }
  },
  privilege_tree: { isRef: true },
  privilege_profile: { isRef: true },
  tree: {
    fields: {
      id: 'idS',
      name: 'char150',
      description: 'char150'
    },
    uniques: ['name&company_id'],
    refs: ['company'],
  },
  user_tree_node: { isRef: true },
  user_node: { isRef: true },
  user: {
    fields: {
      id: 'idM',
      name: 'char150',
      last_name: 'char150',
      cognito_id: 'char150',
      email: 'char150'
    },
    refs: ['company']
  }
}

start({}).then(() => {
  createTables({
    db: 'shared', tables, base
  }).then(() => {
    console.info('ready')
    process.exit()
  })
})