module.exports = {
  created_at: 'Int',
  updated_at: 'Int',
  version: 'T',
  status: 'T',
  last_op: 'char150'
}