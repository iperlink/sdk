const find = require('../../mysqlUtils/query')

module.exports = async (profileId) => {
  const [profiles] = await find({
    query: `
      SELECT shared.profile.name as profile,
      shared.privilege.id as privilege_id,
      shared.privilege.name as privilege
      from shared.profile
      inner join shared.profile_privilege on shared.profile_privilege.profile_id = shared.profile.id
      inner join shared.privilege on shared.privilege.id = shared.profile_privilege.privilege_id
      where shared.profile.id = ?
    `,
    params: [profileId]
  })
  return profiles
}