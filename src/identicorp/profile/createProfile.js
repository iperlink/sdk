const insert = require('../../mysqlUtils/insert')
const multiInsert = require('../../mysqlUtils/insert/multiInsert')
const userCan = require('../user/userCan')

module.exports = async ({ name, privileges, username }) => {
  const can = await userCan({ username, privilege: 3 })
  if (can.error) {
    return can
  }
  const created = await insert({
    db: 'shared',
    table: 'profile',
    values: { name },
    requiredKeys: ['name']
  })
  if (created.code) {
    throw new Error(JSON.stringify(created))
  }
  const mappedPrivileges = privileges.map(p => {
    const result = Object.assign(
      {},
      {
        profile_id: created.id,
        privilege_id: p,
      }
    )
    return result
  })
  await multiInsert({
    db: 'shared',
    table: 'profile_privilege',
    values: mappedPrivileges
  })
  return created
}
