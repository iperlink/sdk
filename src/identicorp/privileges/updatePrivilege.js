const update = require('../../mysqlUtils/update')

module.exports = async ({ privilege, id }) => {
  const updated = await update({
    db: 'shared',
    table: 'privilege',
    values: privilege,
    id
  })
  if (updated.code) {
    throw new Error(JSON.stringify(updated))
  }
  return updated
}
