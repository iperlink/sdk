const insert = require('../../mysqlUtils/insert')

module.exports = async (privilege) => {
  const created = await insert({
    db: 'shared',
    table: 'privilege',
    values: privilege,
    refs: ['app'],
    requiredKeys: ['app_id', 'name']
  })
  if (created.code) {
    throw new Error(JSON.stringify(created))
  }
  return created
}
