const hget = require('../../redisUtils/hget')
const setNodes = require('./setNodes')

module.exports = async () => {
  const res = await hget({ name: 'node_id', notNull: true, fn: setNodes })
  return res
}
