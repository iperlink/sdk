const insert = require('../../mysqlUtils/insert')

module.exports = async (node) => {
  const created = await insert({
    db: process.env.IDENTICORP_DB,
    table: 'node',
    values: node,
    requiredKeys: ['name']
  })
  if (created.code) {
    throw new Error(JSON.stringify(created))
  }
  return created
}
