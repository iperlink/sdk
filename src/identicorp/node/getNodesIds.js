const hget = require('../../redisUtils/hget')
const setNodes = require('./setNodes')
const uniq = require('lodash/uniq')

module.exports = async (keys) => {
  const array = await hget({ name: 'descIds', keys, notNull: true, fn: setNodes })
  const l = array.length
  let ids = array[0]
  for (let i = 1; i < l; i++) {
    ids = ids.concat(array[i])
  }
  return uniq(ids)

}
