const update = require('../../mysqlUtils/update')
const find = require('../../mysqlUtils/find')
const setNodes = require('./setNodes')

module.exports = async ({ parent, id }) => {
  const db = process.env.IDENTICORP_DB
  const ref = await find({ db, table: 'node', queryObj: { id: parent } })
  if (!ref[0]) {
    throw new Error(JSON.stringify({
      code: 'cantFindNodeParent',
      message: `cant find parent node, parent: ${parent}, node: ${id}`,
    }))
  }
  const updated = update({
    db: process.env.IDENTICORP_DB,
    table: 'node',
    values: { parent_node: parent },
    id
  })
  if (updated.code) {
    throw new Error(JSON.stringify(updated))
  }
  await setNodes()
  return updated
}
