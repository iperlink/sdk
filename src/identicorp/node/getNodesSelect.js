const get = require('../../redisUtils/getInt')
const setNodes = require('./setNodes')

module.exports = async () => {
  const nodes = await get({ name: 'node_structure', fn: setNodes })
  return nodes
}
