const queryExecutor = require('../../../mysqlUtils/query')
const setNodeData = require('./setNodeData')
const consolidate = require('./consolidate')
const groupBy = require('../../../objMapper/groupBy')
const set = require('../../../redisUtils/set')
const hmset = require('../../../redisUtils/hmset')

module.exports = async () => {
  const query = `SELECT
    shared.node.id,
    shared.node.name,
    shared.node.tree_id,
    shared.node.parent_node as parent
    #shared.tree.name as tree,
    #shared.company.id as company_id,
    #shared.company.name as company
    from shared.node
    #inner JOIN shared.tree on shared.node.tree_id = shared.tree.id
    #inner JOIN shared.company on shared.company.id = shared.tree.company_id
    `
  const [nodes] = await queryExecutor({ query })

  const groupByParent = groupBy({
    array: nodes,
    key: 'parent'
  })
  const nodeData = nodes.map(node => {
    return setNodeData({ node, groupByParent })
  })
  const {
    descIds,
    node_structure
  } = consolidate(nodeData)
  await set({ key: 'node_structure', doc: node_structure })
  await hmset({ name: 'descIds', params: descIds })

  return true
}
