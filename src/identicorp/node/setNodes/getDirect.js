const queryExecutor = require('../../../mysqlUtils/query')

module.exports = ({ node, groupByParent }) => {
  const childs = groupByParent[node.id] || []
  const mappedChilds = childs.map(x => ({ id: x.id, name: x.name }))
  return mappedChilds
}
