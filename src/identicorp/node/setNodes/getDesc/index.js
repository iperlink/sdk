const getDescExecutor = require('./executor')

module.exports = ({ nodeId, groupByParent }) => {
  const results = []
  const executed = getDescExecutor({
    results,
    nodeIds: [nodeId],
    groupByParent,
  })
  return executed
}
