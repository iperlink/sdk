
const getDescExecutor = ({ nodeIds, groupByParent, results }) => {
  let newResultAcc = []
  nodeIds.forEach(nodeId => {
    const children = groupByParent[nodeId]
    if (children) {
      newResultAcc = newResultAcc.concat(children)
    }
  })
  const newNodeIds = newResultAcc.map(x => x.id)
  const newResults = results.concat(newResultAcc)
  if (newNodeIds.length > 0) {
    return getDescExecutor({
      nodeIds: newNodeIds,
      groupByParent,
      results: newResults,
    })
  } else {
    return results
  }
}

module.exports = getDescExecutor

