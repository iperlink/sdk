const getDesc = require('./getDesc')
const hset = require('../../../redisUtils/hset')
const getDirect = require('./getDirect')

module.exports = ({ node, groupByParent }) => {
  const { id, name } = node
  const desc = getDesc({ nodeId: id, groupByParent });
  const descIds = desc.map(x => x.id)
  const direct = getDirect({ node, groupByParent })
  return {
    desc,
    descIds: [id].concat(descIds),
    direct,
    id,
    name,
  }
}
