const keyBy = require('../../../objMapper/keyBy')
const mapValues = require('../../../objMapper/mapValues')

module.exports = (array) => {
  const node_structure = keyBy({ array, key: 'id' })
  const descIds = []
  array.forEach(value => {
    descIds.push(value.id)
    descIds.push(JSON.stringify(value.descIds))
  })
  return {
    descIds,
    node_structure
  }
}
