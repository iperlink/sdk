const update = require('../../mysqlUtils/update')
const setNodes = require('./setNodes')

module.exports = async ({ name, id }) => {
  const updated = await update({
    db: process.env.IDENTICORP_DB,
    table: 'node',
    values: { name },
    id
  })
  if (updated.code) {
    throw new Error(JSON.stringify(updated))
  }
  await setNodes()
  return updated
}
