const rebuildPrivilegeTree = require('../privilege_tree/rebuildPrivilegeTree')
const rebuildUsersHash = require('../user/rebuildUsersHash')

module.exports = async () => {
  const promiseArray = [rebuildPrivilegeTree, rebuildUsersHash]
  await rebuildPrivilegeTree()
  await rebuildUsersHash()
}