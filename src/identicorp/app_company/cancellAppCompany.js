const changeStatusRef = require('../../mysqlUtils/changeStatus/changeStatusRef')

module.exports = async ({ app_id, company_id, status }) => {
  const updated = await changeStatusRef({
    db: process.env.IDENTICORP_DB,
    table: 'app_company',
    status,
    ref: { app_id, company_id }
  })
  if (updated.code) {
    throw new Error(`error updating app_company ${updated.code}`)
  }
  return updated
}
