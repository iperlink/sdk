const createCompany = require('../company/createCompany')
const insert = require('../../mysqlUtils/insert')
const find = require('../../mysqlUtils/find')

module.exports = async ({ company, app }) => {
  const db = process.env.IDENTICORP_DB
  const companyQueryObj = { rut: company.rut }
  const companies = await find({
    db,
    table: 'company',
    queryObj: companyQueryObj
  })
  const companyId = companies[0]
    ? companies[0]
    : await createCompany(company)
  const obj = { app_id: app, company_id: companyId.id, status: 1 }
  const created = await insert({ db, table: 'app_company', values: obj })
  if (created.code) {
    throw new Error(JSON.stringify(created))
  }
  return created
}
