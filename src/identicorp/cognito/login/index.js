const login = require('./login')
const checkLogin = require('./checkLogin')

module.exports = async ({ email, password }) => {
  const isLoguedIn = await checkLogin()
  if (isLoguedIn) {
    return isLoguedIn
  }
  const token = await login({ email, password })
  return token
}