const { Auth } = require('aws-amplify')
const checkLogin = require('./checkLogin')
const devError = require('../../../reactComponents')
const changePassword = require('../changePassword')

module.exports = async ({ username, password }, onSuccess, onFail) => {
  try {
    await changePassword(username)
    // await Auth.signIn('developer@gmail.com', password)
    // //await Auth.signIn('andresriosaragon@gmail.com', 'splinermann')
    // const res = await checkLogin()
    // onSuccess(res)
    return 'res'
  } catch (e) {
    console.error(e.message)
    onFail(e)
  }
}