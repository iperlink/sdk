const { Auth } = require('aws-amplify');
const initUserNodes = require('../../../htmlHelpers/serviceStarter/initUserNodes')

module.exports = async () => {
  try {
    const session = await Auth.currentSession()
    const token = session.accessToken.jwtToken
    const email = session.idToken.payload['cognito:username']
    const user = await fetch('https://7yktz5u9o8.execute-api.us-east-1.amazonaws.com/beta/getUser', {
      method: 'POST',
      body: JSON.stringify({ email }),
    })
    const userValue = await user.json()
    console.log(userValue)
    localStorage.setItem('userData', JSON.stringify(userValue))
    initUserNodes()
    return token
  } catch (e) {
    console.info(e)
    return false
  }
}