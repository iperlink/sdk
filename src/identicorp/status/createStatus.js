const insert = require('../../mysqlUtils/insert/multiInsert')

module.exports = async (mapped) => {
  const values = mapped.map(name => ({ name }))
  const res = await insert({
    db: 'shared',
    table: 'status',
    values,
  })
  return res

}
