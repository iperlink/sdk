const insert = require('../../mysqlUtils/insert')

module.exports = async (company) => {
  const values = Object.assign({},
    company,
    { status: 1 }
  )
  const created = await insert({
    db: 'shared',
    table: 'company',
    values,
    requiredKeys: ['rut', 'name']
  })
  if (created.code) {
    throw new Error(JSON.stringify(created))
  }
  return created
}
