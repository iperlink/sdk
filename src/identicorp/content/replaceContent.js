const multiInsert = require('../../mysqlUtils/insert/multiInsert')
const multiDelete = require('../../mysqlUtils/delete/multiDelete')
const hget = require('../../redisUtils/hget')

module.exports = async (content_node) => {
  const { DB_NAME } = process.env
  const {
    node_id,
    content_id,
    table,
  } = content_node
  const nodes = node_id.split(',')
  const values = []
  const currentTables = await hget({ name: `${DB_NAME}_table_ref`, keys: table })
  nodes.forEach(node_id => {
    currentTables.forEach(ct => {
      values.push({
        view: ct,
        node_id,
        content_id
      })
    })
  })
  if (!DB_NAME) {
    throw new Error(JSON.stringify({
      code: 'envDbNotDefined',
      message: 'env.DB_NAME is not defined',
    }))
  }
  const withTick = currentTables.map(ct => `'${ct}'`)
  await multiDelete({
    db: DB_NAME,
    table: 'content_node',
    paramValue: [content_id],
    param: 'content_id',
    where: ` view in (${withTick.join(',')}) and `
  })
  const res = await multiInsert({
    db: process.env.DB_NAME,
    table: 'content_node',
    values,
  })
  return res
}
