const queryExecutor = require('../../mysqlUtils/query')

module.exports = async ({ userNodes, db, view }) => {
  const { nodes, trees } = userNodes
  const query = `select content_id from ${db}.content_node where node_id in (?) and view = ?
    group by content_id
    HAVING count(content_id)  = ?`
  console.log({ query, nodes, view })
  const [found] = await queryExecutor({ query, params: [nodes, view, trees] })
  const result = found.map(x => x.content_id)
  return result
}
