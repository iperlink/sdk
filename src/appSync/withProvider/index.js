const createRehydrated = require('./rehydrated')
const getApolloProvider = require('./apolloProvider');

module.exports = (props) => {
  const rehydrated = createRehydrated(props.children)
  const apolloProvider = getApolloProvider({ props, rehydrated })
  return apolloProvider
}
