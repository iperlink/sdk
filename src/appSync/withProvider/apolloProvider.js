const Client = require('aws-appsync').default
const { ApolloProvider } = require('react-apollo')
const createElement = require('../../renderer')

module.exports = ({ props, rehydrated }) => {
  const { token } = props
  const { AWS } = CLIENT_CONFIG
  const APP_SYNC = {
    url: AWS.GRAPHQL_URL,
    region: AWS.REGION,
    defaultOptions: {
      query: {
        fetchPolicy: 'cache-and-network',
        errorPolicy: 'all',
      },
    },
    disableOffline: true,
    auth: {
      type: 'AMAZON_COGNITO_USER_POOLS',
      userPoolId: AWS.userPoolId,
      userPoolWebClientId: AWS.userPoolWebClientId,
      jwtToken: token
    }
  }
  const client = new Client(APP_SYNC)
  APOLLO = client
  const apolloProvider = createElement({
    type: ApolloProvider,
    props: { client, id: 'apollo' },
    children: rehydrated
  })
  return apolloProvider
}
