const { Rehydrated } = require('aws-appsync-react')
const createElement = require('../../renderer')

module.exports = (children) => {
  const rehydrated = createElement({
    type: Rehydrated,
    props: { id: 'rehidrated' },
    children,
  })
  return rehydrated
}
