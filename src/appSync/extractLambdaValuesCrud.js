const omit = require('lodash/omit')

module.exports = (event) => {
  const db = process.env.DB_NAME
  const {
    username
  } = event.handle
  const {
    privilege,
    operation,
    templateName,
    omitNodes,
  } = event
  console.log(event)
  const args = event.arguments
  const values = omit(args, ['nodeId'])
  const result = {
    db,
    privilege,
    queryObj: values,
    operation,
    templateName,
    username,
    values,
    omitNodes,
  }
  return result
};
