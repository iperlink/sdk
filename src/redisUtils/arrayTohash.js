const { getServices } = require('../serviceStarter')

module.exports = async ({ key = 'id', array, hashName }) => {
  const { redis } = getServices()
  const params = []
  array.forEach(res => {
    params.push(res[key])
    params.push(JSON.stringify(res))
  })
  await redis.hmset(hashName, params)
  return array
}