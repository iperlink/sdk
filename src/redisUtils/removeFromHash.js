const { getServices } = require('../serviceStarter')
const without = require('lodash/without')

module.exports = async ({ hash, key, val }) => {
  const { redis } = getServices()
  const prev = await redis.hget(hash, key)
  if (prev) {
    const parsed = JSON.parse(prev)
    if (!Array.isArray(parsed)) {
      throw new Error(JSON.stringify({
        code: 'redisKeyHasNotArrayRemove',
        message: `key ${key} in hash ${hash} is not an Array`,
      }))
    }
    const newArr = without(parsed, val)
    await redis.hset(hash, key, JSON.stringify(newArr))
    return true
  }
  return
}
