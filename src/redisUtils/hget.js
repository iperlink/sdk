const { getServices } = require('../serviceStarter')
const hasNull = require('../objMapper/hasNull')
const mapValues = require('../objMapper/mapValues')
const hexist = require('./hexist')
const delay = require('delay')
const rebuild = require('./rebuild')

const get = async ({ name, keys, notNull, fn }) => {
  const { redis } = getServices()
  const h = await hexist(name)
  if (h != 1) {
    await rebuild({ name, fn })
    await delay(3000)
    await get({ name, keys, notNull, fn })
  }
  if (keys) {
    if (Array.isArray(keys)) {
      const foundRedis = await redis.hmget(`hash_${name}`, keys)
      if (notNull) {
        const allDefined = hasNull(foundRedis)
        if (!allDefined) {
          throw new Error(JSON.stringify({
            code: 'redisHgetNodeNotExist',
            message: 'one of the nodes does not exist'
          }))
        }
      }
      const parsed = foundRedis.map(x => JSON.parse(x))
      return parsed
    }
    else {
      const foundRedis = await redis.hget(`hash_${name}`, keys)
      const parsed = JSON.parse(foundRedis)
      return parsed
    }
  }
  else {
    const obj = await redis.hgetall(`hash_${name}`)
    const parsed = mapValues({ obj, fn: (x) => JSON.parse(x) })
    return parsed
  }
}

module.exports = async ({ name, keys, notNull, fn }) => {
  const { redis } = getServices()
  const result = await get({ name, keys, notNull, fn, redis })
  return result
}

