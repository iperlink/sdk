const { getServices } = require('../serviceStarter')

module.exports = async ({ name, fn }) => {
  console.info('on rebuild', name)
  const { redis } = getServices()
  if (!fn) {
    throw new Error(`fn is not defined on rebuild ${name}`)
  }
  const working = await redis.get(`${name}_workingAt`)
  if (working) {
    return
  }
  await redis.set(`${name}_workingAt`, '1')
  try {
    await fn()
  } catch (e) {
    console.error(e)
    process.exit()
  }
  await redis.del(`${name}_workingAt`)
}