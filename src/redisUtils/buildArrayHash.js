const { getServices } = require('../serviceStarter')
const groupBy = require('../objMapper/groupBy')

module.exports = async ({ db, key = 'id', table, name, extract }) => {
  const { mysql, redis } = getServices()
  const cacheName = name || table
  await redis.set(`building_${cacheName}`, 1)
  const baseQuery = `SELECT * from ${db}.${table}`
  const [found] = await mysql.execute(baseQuery)
  if (found.length === 0) {
    await redis.hset(`hash_${cacheName}`, 'default', 'default')
    return found
  }
  const grouped = groupBy({ array: found, key })
  const params = []
  Object.keys(grouped).forEach(res => {
    params.push(res)
    const obj = grouped[res]
    const extracted = obj.map(x => x[extract])
    params.push(JSON.stringify(extracted))
  })
  await redis.hmset(`hash_${cacheName}`, params)
  return found
}