const { getServices } = require('../serviceStarter')

module.exports = async ({ hash, obj }) => {
  const { redis } = getServices()
  const keys = Object.keys(obj)
  const params = []
  keys.forEach(k => {
    params.push(k)
    params.push(JSON.stringify(obj[k]))
  })
  await redis.hset(hash, params)
  return params
}
