const { getServices } = require('../serviceStarter')

module.exports = async ({ db, key = 'id', table, name }) => {
  const { mysql, redis } = getServices()
  const cacheName = name || table
  await redis.set(`building_${cacheName}`, 1)
  const baseQuery = `SELECT * from ${db}.${table}`
  const [found] = await mysql.execute(baseQuery)
  const params = []
  found.forEach(res => {
    params.push(res[key])
    params.push(JSON.stringify(res))
  })
  await redis.hmset(`hash_${cacheName}`, params)
  return found
}