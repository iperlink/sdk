const { getServices } = require('../serviceStarter')

module.exports = async ({ key, doc }) => {
  const { redis } = getServices()
  await redis.set(key, doc)
}
