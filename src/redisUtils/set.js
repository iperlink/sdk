const { getServices } = require('../serviceStarter')

module.exports = async ({ key, doc }) => {
  const { redis } = getServices()
  if (!key || !doc) {
    throw new Error(JSON.stringify({
      code: 'redisKeyDocRequired',
      message: 'key and doc are required'
    }))
  }
  await redis.set(key, JSON.stringify(doc))
}
