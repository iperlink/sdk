const { getServices } = require('../serviceStarter')

module.exports = async (force) => {
  const { redis } = getServices()
  if (redis.options.host === 'localhost' || force) {
    redis.flushall()
  } else {
    throw new Error('can not flush on remote server')
  }
}
