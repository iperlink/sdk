const { getServices } = require('../serviceStarter')

module.exports = async ({ name, params }) => {
  const { redis } = getServices()
  await redis.hmset(`hash_${name}`, params)
}
