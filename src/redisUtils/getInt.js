const { getServices } = require('../serviceStarter')
const hexist = require('./hexist')
const delay = require('delay')
const rebuild = require('./rebuild')

const get = async ({ name, fn }) => {
  const { redis } = getServices()
  const h = await redis.exists(name)
  if (h != 1) {
    await rebuild({ name, fn })
    await delay(3000)
    await get({ name, fn })
  }
  const raw = await redis.get(name)
  return raw
}

module.exports = get