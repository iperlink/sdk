const { getServices } = require('../serviceStarter')

module.exports = async (name) => {
  const { redis } = getServices()
  const h = await redis.exists(`hash_${name}`)
  return h
}
