const { getServices } = require('../serviceStarter')

module.exports = async ({ name, key, doc }) => {
  const { redis } = getServices()
  await redis.hset(`hash_${name}`, key, JSON.stringify(doc))
}
