const { getServices } = require('../serviceStarter')
const uniq = require('lodash/uniq')

module.exports = async ({ hash, key, val }) => {
  const { redis } = getServices()
  const prev = await redis.hget(hash, key)
  if (prev) {
    const parsed = JSON.parse(prev)
    if (!Array.isArray(parsed)) {
      throw new Error(JSON.stringify({
        code: 'redisKeysNotArray',
        message: `key ${key} in hash ${hash} is not an Array`,
      }))
    }
    const newArr = uniq(parsed.concat([val]))
    await redis.hset(hash, key, JSON.stringify(newArr))
    return true
  }
  await redis.hset(hash, key, JSON.stringify([val]))
}
