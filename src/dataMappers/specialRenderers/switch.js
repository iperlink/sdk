const Switch = require('../../reactComponents/switch')


module.exports = ({ rowData, addSelected, selected, iterator }) => {
  const checked = selected
    ? selected.includes(rowData.id)
    : false
  const btn = Switch({
    switchData: rowData,
    checked,
    addSelected,
    iterator
  })
  return btn
}