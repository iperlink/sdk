const button = require('../../reactComponents/btn')
const formConnector = require('../../reactComponents/formConnector')

module.exports = ({ iterator, rowData, key, data, changeComp }) => {
  const { form } = key
  const { idAs, name, title } = form
  const initialValues = idAs
    ? {
      ...rowData,
      [idAs]: rowData.id,
    }
    : rowData
  const formProps = {
    id: name,
    data,
    initialValues,
    title
  }
  const btn = button({
    className: 'editBtn',
    id: `editbtn${iterator}`,
    message: 'editar',
    icon: 'FaEdit',
    onClick: () => {
      const newProps = formConnector(formProps)
      changeComp({ newProps, type: 'form' })

    }
  })
  return btn
}