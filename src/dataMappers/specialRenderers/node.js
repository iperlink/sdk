
module.exports = ({ data, keyName }) => {
  const nodeId = (data[keyName])
  return USER_NODES[nodeId].name
}