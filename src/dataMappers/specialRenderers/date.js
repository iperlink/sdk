const moment = require('moment')

module.exports = ({ rowData, keyName }) => {
  var utcSeconds = (rowData[keyName])
  var d = new Date(utcSeconds * 1000)
  const formated = new moment(d).format('DD-MM-YYYY')
  return formated
}