const button = require('../../reactComponents/btn')


module.exports = ({ iterator, data, key }) => {
  const btn = button({
    className: 'editBtn',
    id: `editbtn${iterator}`,
    message: 'editar',
    icon: 'FaEdit',
    onClick: () => key.fn(data)
  })
  return btn
}