const button = require('../../reactComponents/btn')

module.exports = ({ rowData, keyName, iterator, changeComp }) => {
  const mapData = (rowData[keyName]) || JSON.stringify({
    zoom: 6,
    cords: { lat: -33, lng: -70 },
  })
  const mission = rowData.objective
  const area = rowData.geodiv1
  const btn = button({
    className: 'editBtn',
    id: `editbtn${iterator}`,
    message: 'Mapa',
    onClick: () => {
      changeComp({ newProps: { mapData, loaded: true, mission, area }, type: 'mapPoint' })
    }
  })
  return btn
}