
module.exports = {
  date: require('./date'),
  formCreator: require('./formCreator'),
  edit: require('./edit'),
  switch: require('./switch'),
  // showMapPoint: require('./showMapPoint'),
  showMapPoint: require('./showMapPolygon'),
  // switch: require('./switch'),
}