const filterData = require('./filterData')
const getDataPage = require('./getPageData')

module.exports = ({ tableData, page, filterBy, chunk }) => {
  const filtered = filterBy
    ? filterData({ data: tableData, filterBy })
    : tableData
  const paged = page
    ? getDataPage({ data: filtered, page, chunk })
    : filtered
  const result = {
    paged,
    filtered: filtered.length,
    total: tableData.length,
  }
  return result
}