const normalize = require('../../strings/normalizeWithSpace')
const day = require('../../localEpoch/day')

module.exports = ({ filterBy, data, dateParam }) => {
  const DateParam = dateParam || 'filter_date'
  if (data[0][DateParam]) {
    const dataAfter = filterBy.startDate
      ? data.filter(d => d[DateParam] >= day(filterBy.startDate))
      : data
    const dataBefore = filterBy.endDate
      ? dataAfter.filter(d => d[DateParam] <= day(filterBy.endDate))
      : dataAfter
    const tagged = (filterBy.tags && filterBy.tags !== '')
      ? dataBefore.filter(d => {
        const tagArray = normalize(d.tags).split(',')
        return tagArray.includes(filterBy.tags)
      })
      : dataAfter
    return tagged
  }
  const tagged = filterBy.tags !== ''
    ? data.filter(d => {
      const tagArray = normalize(d.tags).split(',')
      return tagArray.includes(filterBy.tags)
    })
    : data
  return tagged
}