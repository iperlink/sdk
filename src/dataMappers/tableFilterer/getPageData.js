
module.exports = ({ data, page, chunk }) => {
  const from = (page - 1) * chunk
  const to = page * chunk
  const sliced = data.slice(from, to);
  return sliced
}