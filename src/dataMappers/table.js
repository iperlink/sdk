const specialRenderer = require('./specialRenderers')

module.exports = ({ rawData, keys, addSelected, selected, data, changeComp }) => {
  const mapped = rawData.map((d, i) => {
    const result = Object.keys(keys).map(keyName => {
      const key = keys[keyName]
      if (key.specialRenderer) {
        if (!specialRenderer[key.specialRenderer]) {
          throw new Error(`
            Special renderer ${key.specialRenderer}
            is not defined as a special renderer
          `)
        }
        return {
          label: specialRenderer[key.specialRenderer]({
            addSelected,
            changeComp,
            data,
            iterator: i,
            key,
            keyName,
            rowData: d,
            selected,
          }),
          key: keyName
        }
      }
      const label = d[keyName]
      return {
        label,
        key: keyName,
      }
    })
    return result
  })
  return mapped
}