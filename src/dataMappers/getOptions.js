const uniq = require('../objMapper/uniq')
const normalize = require('../strings/normalizeWithSpace')

module.exports = (data) => {
  const firstData = data[0]
  if (!firstData) {
    return false
  }
  const hasTags = Boolean(data[0].tags)
  if (!hasTags) {
    return false
  }
  const tags = data.reduce((acc, curr) => {
    const result = `${acc},${normalize(curr.tags)}`
    return result
  }, '')
  const splitted = tags.split(',')
  const uniques = uniq(splitted)
  return uniques
}