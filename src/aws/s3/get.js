const { getServices } = require('../servicesStarter')

module.exports = ({ bucket, file }) => {
  const { s3 } = getServices()
  const params = { Bucket: bucket, Key: file }
  return new Promise((resolve, reject) => {
    s3.getObject(params, (err, data) => {
      console.error(err)
      if (err) return reject(err)
      resolve(data)
    })
  })
}
