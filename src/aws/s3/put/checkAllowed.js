const sweetalert = require('sweetalert')

module.exports = ({ allowed, name, front }) => {
  const splitted = name.split('.')
  const ext = splitted[splitted.length - 1]
  if (!allowed.includes(ext)) {
    if (front) {
      sweetalert(`Los archivos tipo ${ext} no estan permitidos`)
    }
    throw new Error(JSON.stringify({
      code: 'fileExtNotAllowed',
      message: `extension ${ext} is not allowed`,
    }))
    return false
  }
  return true
}