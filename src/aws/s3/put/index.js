const { getServices } = require('../../../serviceStarter')
const checkAllowed = require('./checkAllowed')

module.exports = ({ bucket, name, data, allowed }) => {
  const { s3 } = getServices()
  if (allowed) {
    checkAllowed({ allowed, name })
  }
  const params = { Bucket: bucket, Key: name, Body: data }
  return new Promise((resolve, reject) => {
    s3.putObject(params, (err, data) => {
      if (err) {
        console.log(err)
        return reject('error fetching' + err)
      }
      console.info('uploaded to ', bucket)
      resolve(data)
    })
  })
}
