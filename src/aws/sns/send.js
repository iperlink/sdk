const AWS = require('aws-sdk');

const send = (i) => {
  AWS.config.update({ region: 'us-east-1' });

  // Create publish parameters
  var params = {
    Message: `MESSAGE_TEXT${i}`, /* required */
    TopicArn: 'arn:aws:sns:us-east-1:045321087816:lambdaCaller'
  };
  return new AWS.SNS({ apiVersion: '2010-03-31' }).publish(params).promise();
}

const many = async () => {
  const arr = []
  for (let i = 0; i < 10; i++) {
    arr.push(send(i))
  }
  await Promise.all(arr)
}

send().then();