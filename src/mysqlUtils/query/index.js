const { getServices } = require('../../serviceStarter')
const errorCodes = require('../errors/codes')

module.exports = async ({ query, params, log }) => {
  const { mysql } = getServices()
  if (!query) throw new Error(JSON.stringify({
    code: 'queryIsRequired',
    message: 'query is required'
  }))
  try {
    const result = await mysql.query(query, params)
    if (log) {
      console.error(mysql.format(query, params))
    }
    return result
  }
  catch (e) {
    console.info('error in', mysql.format(query, params))
    if (errorCodes[e.code] && typeof errorCodes[e.code] === "function") {
      const result = errorCodes[e.code](e)
      return result
    }
    throw e
  }
}


