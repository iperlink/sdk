const now = require('../../localEpoch/now')
const snake = require('../../objMapper/snakeKeys')

module.exports = ({ defaultValues, values }) => {
  const snakedValues = snake(values)
  const snakedDefaults = snake(defaultValues)
  const defaults = {
    created_at: now(),
    version: 1
  }
  const toInsert = Object.assign({}, snakedDefaults, snakedValues, defaults)
  return toInsert
}