const required = require('../../objMapper/requiredKeys')
const queryExecutor = require('../query')
const prepareMultiValues = require('./prepareMultiValues')

/**
 * Insert multiple values in a mysql table.
 * @async
 * @function multiInsert
 * @example
 * // return promise
 * multiInsert({ db: 'a', table: 'b', values:[{'v:1'}] })
 * @param {Object} multiInsertParams - the params.
 * @param {string} multiInsertParams.db - The name of the db.
 * @param {string} multiInsertParams.table - The name of the table.
 * @param {array} multiInsertParams.values - Array of Object Values to be inserted.
 * @param {array} [multiInsertParams.requiredKeys] - every inserted object must have them.
 * @param {boolean} [multiInsertParams.returnIds] - if inserted row ids must be returned.
 * @param {Object} [multiInsertParams.defaultValues] - if an object does not have these values will be inserted with de defaults.
 */



const multiInsert = async ({ db, table, values, requiredKeys, defaultValues, returnIds }) => {

  const {
    keys,
    toInsert,
    last_op
  } = prepareMultiValues({ values, defaultValues })
  const query = `INSERT INTO ${db}.${table} (${keys}) values ?`
  if (requiredKeys) {
    required({ obj: toInsert, message: `in table ${table}`, required: requiredKeys })
  }
  const res = await queryExecutor({ query, params: [toInsert] })
  const resultLength = res.code
    ? res
    : res[0].affectedRows
  if (!returnIds) {
    return resultLength
  }
  const [inserted] = await queryExecutor({
    query: `SELECT * from ${db}.${table} where last_op = ?`,
    params: [last_op]
  })
  return inserted
}

module.exports = multiInsert