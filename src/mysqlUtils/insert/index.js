const required = require('../../objMapper/requiredKeys')
const queryExecutor = require('../query')
const prepareValues = require('./prepareValues/index.js')

module.exports = async ({ defaultValues, requiredKeys, table, values, db = process.env.DB_NAME }) => {
  const query = `INSERT INTO ${db}.${table} set ?`
  const toInsert = prepareValues({ defaultValues, values })
  if (requiredKeys) {
    required({ obj: toInsert, message: `in table ${table}`, required: requiredKeys })
  }
  const res = await queryExecutor({ query, params: [toInsert] })
  if (res.code) return res
  const result = Object.assign({}, toInsert, { id: res[0].insertId })
  return result
}
