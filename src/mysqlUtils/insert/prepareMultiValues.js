const appendEach = require('../../objMapper/appendEach')
const collectionKeys = require('../../objMapper/collectionKeys')
const extractAllValues = require('../../objMapper/extractAllValues')
const now = require('../../localEpoch/now')
const uuidv1 = require('uuid/v1');

module.exports = ({ values, defaultValues }) => {
  const last_op = uuidv1()
  const defaults = {
    created_at: now(),
    version: 1,
    last_op
  }
  const withdefaults = appendEach({ array: values, toAppend: defaults })
  const withdefaultValues = appendEach({ array: withdefaults, toAppend: defaultValues })
  const toInsert = extractAllValues(withdefaultValues)
  const keys = collectionKeys(withdefaultValues)
  const result = {
    keys,
    toInsert,
    last_op
  }
  return result
}