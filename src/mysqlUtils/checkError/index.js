const { getServices } = require('../../serviceStarter')

module.exports = async (query) => {
  const { mysql } = getServices()
  if (!query) throw new Error(JSON.stringify({
    code: 'queryIsRequired',
    message: 'query is required'
  }))
  await mysql.query(`EXPLAIN ${query}`)
}


