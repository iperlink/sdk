const errorCodes = require('../errors/codes')
const { getServices } = require('../../serviceStarter')

module.exports = async ({ db, id, status, statusList, table }) => {
  if (!statusList.includes(status)) {
    throw new Error(JSON.stringify({
      code: 'statusNotDefined',
      message: 'status is not defined'
    }))
  }
  const { mysql } = getServices()
  const query = `UPDATE ${db}.${table} set status = ? WHERE id = ?`
  try {
    const [res] = await mysql.query(query, [status, id])
    if (res.changedRows === 1) {
      return res.changedRows
    }
    return errorCodes.NO_UPDATE_STATUS_ID(null, null, table, id)
  } catch (e) {
    if (errorCodes[e.code]) {
      return errorCodes[e.code](e, null, table, id)
    }
    return e
  }
}
