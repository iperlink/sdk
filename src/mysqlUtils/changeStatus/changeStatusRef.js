const execute = require('../query')

module.exports = async ({ db, ref, status, table }) => {
  const keys = Object.keys(ref)
  const query = `UPDATE ${db}.${table} set status = ? WHERE ${keys[0]} = ? and ${keys[1]} = ?`
  try {
    const [res] = await execute({ query, params: [status, ref[keys[0]], ref[keys[1]]] })
    if (res.changedRows === 1) {
      return res.changedRows
    }
    console.error(`${db}.${table} ${JSON.stringify(ref)} points to no ref`)
    return { code: 'Ref does not exist' }
  } catch (e) {
    return e
  }
}
