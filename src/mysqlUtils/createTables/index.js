const { getServices } = require('../../serviceStarter')
const mapper = require('./mapTables')
const createRefs = require('./createRefs')
const mapValues = require('../../../src/objMapper/mapValues')
const mapFk = require('./mapFk')

module.exports = async ({ tables, db, base }) => {
  const { mysql } = getServices()
  await mysql.execute('SET FOREIGN_KEY_CHECKS = 0');
  const withRefs = createRefs({ tables, db })
  const promiseArray = mapValues({
    obj: withRefs,
    fn: (table, key) => {
      return mapper({ table, db, mysql, base, tables, key })
    }
  })
  await Promise.all(Object.values(promiseArray))
  const promiseArray2 = mapValues({
    obj: withRefs,
    fn: (table, key) => {
      return mapFk({ table, db, mysql, base, tables, key })
    }
  })
  await Promise.all(Object.values(promiseArray2))
}
