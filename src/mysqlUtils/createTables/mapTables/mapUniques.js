
module.exports = ({ table, key }) => {
  if (table.isRef) {
    const splitted = key.split('_')
    const withId = splitted.map(x => `${x}_id`)
    const string = withId.join(',')
    const result = `,  UNIQUE INDEX ${key}_UNIQUE (${string} ASC)`
    return result
  }
  if (!table.uniques) return ''
  const mapped = table.uniques.map(un => {
    if (Array.isArray(un)) {
      const mapped = un.map(u => `${u} ASC`)
      const res = ` UNIQUE INDEX ${un.join('_')}_UNIQUE (${mapped.join(',')})`
      return res
    }
    if (un.includes('&')) {
      const unsplitted = un.split('&')
      const joined = unsplitted.join(',')
      const result = ` UNIQUE INDEX ${un.replace('&', '_')}_UNIQUE (${joined} ASC)`
      return result
    }
    const result = ` UNIQUE INDEX ${un}_UNIQUE (${un} ASC)`
    return result
  })
  return `, ${mapped.join(',')}`
}
