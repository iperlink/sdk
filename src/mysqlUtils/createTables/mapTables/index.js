const mapUniques = require('./mapUniques')

module.exports = async ({ table, db, mysql, base, key }) => {
  const removeQuery = `DROP TABLE IF EXISTS ${db}.${key} `
  await mysql.execute(removeQuery)

  const fields = table.fields
    ? table.fields
    : [{ id: 'id' }]
  const concatedFields = table.omitBase
    ? fields
    : Object.assign({}, fields, base)
  const typeHash = {
    idT: ' TINYINT UNSIGNED UNIQUE AUTO_INCREMENT',
    idS: ' SMALLINT UNSIGNED UNIQUE AUTO_INCREMENT',
    idM: ' MEDIUMINT UNSIGNED UNIQUE AUTO_INCREMENT',
    idInt: ' INT UNSIGNED UNIQUE AUTO_INCREMENT',
    T: ' TINYINT UNSIGNED ',
    Int: ' INT UNSIGNED ',
    S: ' SMALLINT UNSIGNED ',
    M: ' MEDIUMINT UNSIGNED ',
    char150: ' VARCHAR(150) ',
    char500: ' VARCHAR(500) ',
    char50: ' VARCHAR(150) ',
    json: ' JSON(500)'
  }
  const mappedFields = Object.keys(concatedFields).map(K => {
    const field = concatedFields[K]
    if (!field) {
      throw new Error(` ${key}.${K} is not in fields `)
    }
    const type = typeHash[field]
    if (!type) {
      throw new Error(` ${key}.${K} ${field} is not a valid type `)
    }
    return ` ${K} ${type}`
  })
  const baseQuery = `CREATE TABLE IF NOT EXISTS ${db}.${key} `
  const uniques = mapUniques({ table, key })
  const fullQuery = ` ${baseQuery} (${mappedFields.join(',')} , PRIMARY KEY (id) ${uniques} )`
  try {
    await mysql.execute(fullQuery)
  }
  catch (e) {
    console.error('error in', fullQuery)
    throw new Error(e)
  }
  return
}
