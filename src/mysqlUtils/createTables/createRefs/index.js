const makeTableRef = require('./makeTableRef')
const createdRefFields = require('./createRefFields')
const mapValues = require('../../../objMapper/mapValues')

module.exports = ({ tables, db }) => {
  const mapped = mapValues({
    obj: tables,
    fn: (current, key) => {
      if (current.refs || current.isRef) {
        const refs = current.refs || key.split('_')
        const createdRefs = refs.map(name => {
          const result = makeTableRef({ name, tables, key, db })
          return result
        })
        const baseObj = createdRefFields({tables, current, refs })
        return Object.assign({}, baseObj, { fks: createdRefs, refs })
      }
      return current
    }
  })
  return mapped
}