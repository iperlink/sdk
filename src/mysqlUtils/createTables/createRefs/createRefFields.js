const firstValue = require('../../../objMapper/firstValue')

module.exports = ({ tables, current, refs }) => {
  const reduced = refs.reduce((acc, curr) => {
    const type = firstValue(tables[curr].fields).split('id')[1]
    acc[`${curr}_id`] = type
    return acc
  }, {})
  const arr = (current.fields)
    ? reduced
    : Object.assign({}, { id: 'idInt' }, reduced)
  return Object.assign(
    {},
    current,
    {
      fields: Object.assign({}, current.fields, arr)
    })
}