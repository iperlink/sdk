module.exports = ({ name, tables, key, db }) => {
  const refedTable = tables[name]
  if (!refedTable) {
    throw new Error(` ${key}.${name} references a missing table `)
  }
  const result = `ADD FOREIGN KEY (${name}_id) REFERENCES ${db}.${name}(id)`
  return result
}