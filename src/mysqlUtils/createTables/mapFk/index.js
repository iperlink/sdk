
module.exports = async ({ table, db, mysql, base, key }) => {
  if (!table.fks) return
  const fullQuery = ` ALTER TABLE ${db}.${key}
    ${table.fks.join(',')}
  `
  try {
    await mysql.execute(fullQuery)
  }
  catch (e) {
    console.error('error in', fullQuery)
    throw new Error(e)
  }
}
