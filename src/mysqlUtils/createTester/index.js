require('dotenv').config()
const base = require('./base')
const createTables = require('../createTables')
const start = require('../../serviceStarter/tester')

const tables = {
  content_node: {
    fields: {
      id: 'idInt',
      node_id: 'Int',
      privilege_id: 'Int',
      content_id: 'Int'
    }
  }
}

start().then(() => {
  createTables({
    db: 'shared', tables, base
  }).then(() => {
    console.error('ready')
    process.exit()
  })
})