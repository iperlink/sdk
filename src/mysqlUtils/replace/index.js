const errorCodes = require('../errors/codes')
const { getServices } = require('../../serviceStarter')
const queryExecutor = require('../query')

module.exports = async ({ db, table, values }) => {
  try {
    if (!values) {
      return true
    }
    const keys = Object.keys(values[0])
    const vals = values.map(val => Object.values(val))
    const query = `REPLACE INTO ${db}_materialized.${table} (${keys.join(',')}) values (?);`
    const promiseArray = vals.map(val => queryExecutor({ query, params: [val] }))
    await Promise.all(promiseArray)
    return true
  } catch (e) {
    console.error(e)
    if (errorCodes[e.code]) {
      return errorCodes[e.code](e)
    }
    throw e
  }
}