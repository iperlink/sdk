ALTER TABLE app MODIFY created_at INTEGER unsigned;
ALTER TABLE app MODIFY updated_at INTEGER unsigned;
ALTER TABLE app ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE app_company MODIFY created_at INTEGER unsigned;
ALTER TABLE app_company MODIFY updated_at INTEGER unsigned;
ALTER TABLE app_company ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE app_company_user MODIFY created_at INTEGER unsigned;
ALTER TABLE app_company_user MODIFY updated_at INTEGER unsigned;
ALTER TABLE app_company_user ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE company MODIFY created_at INTEGER unsigned;
ALTER TABLE company MODIFY updated_at INTEGER unsigned;
ALTER TABLE company ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE content_node MODIFY created_at INTEGER unsigned;
ALTER TABLE content_node MODIFY updated_at INTEGER unsigned;
ALTER TABLE content_node ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE group MODIFY created_at INTEGER unsigned;
ALTER TABLE group MODIFY updated_at INTEGER unsigned;
ALTER TABLE group ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE group_privilege MODIFY created_at INTEGER unsigned;
ALTER TABLE group_privilege MODIFY updated_at INTEGER unsigned;
ALTER TABLE group_privilege ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE node MODIFY created_at INTEGER unsigned;
ALTER TABLE node MODIFY updated_at INTEGER unsigned;
ALTER TABLE node ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE node_user MODIFY created_at INTEGER unsigned;
ALTER TABLE node_user MODIFY updated_at INTEGER unsigned;
ALTER TABLE node_user ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE privilege MODIFY created_at INTEGER unsigned;
ALTER TABLE privilege MODIFY updated_at INTEGER unsigned;
ALTER TABLE privilege ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE privilege_tree MODIFY created_at INTEGER unsigned;
ALTER TABLE privilege_tree MODIFY updated_at INTEGER unsigned;
ALTER TABLE privilege_tree ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE status MODIFY created_at INTEGER unsigned;
ALTER TABLE status MODIFY updated_at INTEGER unsigned;
ALTER TABLE status ADD COLUMN version tinyint AFTER updated_at;

ALTER TABLE tree MODIFY created_at INTEGER unsigned;
ALTER TABLE tree MODIFY updated_at INTEGER unsigned;
ALTER TABLE tree ADD COLUMN version tinyint AFTER updated_at;