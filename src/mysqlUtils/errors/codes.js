module.exports = {
  ER_DUP_ENTRY: (e) => {
    const splited = e.sqlMessage.split('for key')
    const duplicateKey = splited[1]
    const value = splited[0].split('Duplicate entry ')[1]
    return {
      code: 'duplicateEntry',
      message: `DB error ${duplicateKey} ${value} already exist`
    }
  },
  NO_UPDATE_ID: (e, values, table, id) => {
    return {
      code: 'noUpdateID',
      message: `update query did not match any id`,
      values: { table, id }
    }
  },
  ER_NO_SUCH_TABLE: (table) => {
    return {
      code: 'tableNotExist',
      message: `table does not exist`,
      values: { table }
    }
  },
  NO_UPDATE_STATUS_ID: (e, values, table, id) => {
    return {
      code: 'noUpdateID',
      message: `cant update status with this id`,
      values: { table, id }
    }
  },
  NO_UPDATE_STATUS_REF: (e, values, table, ref) => {
    return {
      code: 'noUpdateRef',
      message: `cant update status with this id`,
      values: { table, ref }
    }
  },
  1452: (e, values, table, ref) => {
    const splited = e.sqlMessage.split('FOREIGN KEY ')[1].split('REFERENCES')[0]
    console.error(`DB error ${splited} ref fails`)
    return {
      code: 'noRef',
    }
  }
}