const { getServices } = require('../../serviceStarter')
const errorCodes = require('../errors/codes')

module.exports = async ({ table, db }) => {
  const { mysql } = getServices()
  const removeQuery = `DELETE FROM ${db}.${table}`
  try {
    await mysql.execute('SET FOREIGN_KEY_CHECKS = 0');
    const [res] = await mysql.execute(removeQuery)
    await mysql.execute(`ALTER TABLE ${db}.${table} AUTO_INCREMENT = 1;`)
    await mysql.execute('SET FOREIGN_KEY_CHECKS = 1');
    return res.warningStatus
  } catch (e) {
    if (errorCodes[e.code]) {
      return errorCodes[e.code]({ table })
    }
    throw e
  }
}
