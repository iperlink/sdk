const mysql = require('mysql2/promise')
const get = require('../../secret/get')

module.exports = async () => {
  const account = process.env.ACCOUNT
  if (!process.env.ACCOUNT) {
    throw new Error(JSON.stringify({
      code: 'noMysqlAccount',
      message: 'must configure account name to connect to mysql'
    }))
  }
  const params = JSON.parse(await get(`${account}_mysql`))
  const {
    host,
    password,
    user
  } = params
  const connection = await mysql.createConnection({
    host,
    password,
    user
  })
  return connection
}
