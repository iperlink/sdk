const withoutTemplate = require('./withoutTemplate')
const withTemplate = require('./withTemplate')

module.exports = async ({ template, table, queryObj, filtered, fields, db }) => {
  const fieldsString = fields
    ? fields.join(',')
    : '*'
  const filteredString = (filtered && filtered.length !== 0)
    ? ` id in (${filtered.join(',')}) and `
    : ''
  console.log(filteredString)
  const found = template
    ? withTemplate({ template, queryObj, db, filteredString, fieldsString })
    : withoutTemplate({ template, table, queryObj, db, filteredString, fieldsString, filtered })
  return found
}