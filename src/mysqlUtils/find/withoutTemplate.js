const execute = require('../query')

module.exports = async ({ filteredString, table, queryObj, filtered, db, fieldsString }) => {
  const baseQuery = ` SELECT ${fieldsString} FROM ${db}.${table} WHERE ${filteredString} ?`
  const baseQueryFull = filtered
    ? ` SELECT ${fieldsString} FROM ${db}.${table} where id in (${filtered.join(',')})`
    : ` SELECT ${fieldsString} FROM ${db}.${table} `
  const [res] = (queryObj && Object.keys(queryObj).length)
    ? await execute({ query: baseQuery, params: [queryObj] })
    : await execute({ query: baseQueryFull })
  return res
}