const day = require('../../../../localEpoch/day')
const firstKey = require('../../../../objMapper/firstKey')
const firstValue = require('../../../../objMapper/firstValue')

module.exports = (object) => {
  const key = firstKey(object)
  const { begin, end } = firstValue(object)
  const beginEpoch = day(begin)
  const endEpoch = day(end)
  if (begin && end) {
    return { string: ` ${key} between ${beginEpoch} and ${endEpoch} ` }
  }
  if (begin) {
    return { string: ` ${key} >= ${beginEpoch} ` }
  }
  if (end) {
    return { string: ` ${key} <= ${endEpoch} ` }
  }
}