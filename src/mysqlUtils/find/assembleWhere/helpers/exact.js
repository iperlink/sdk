module.exports = (param) => {
  const keys = Object.keys(param)
  const val = param[keys[0]]
  if (Array.isArray(val)) {
    return {
      string: ` ${keys[0]} in (?)`, val
    }
  }
  return {
    string: ` ${keys[0]} = ?`, val
  }
}
