const dateBetween = require('./helpers/dateBetween')
const In = require('./helpers/in')
const exact = require('./helpers/exact')
const snake = require('../../../objMapper/snakeKeys')

module.exports = ({ queryObj, queryParams }) => {
  const builders = {
    dateBetween,
    exact,
    in: In
  }
  const strings = []
  const values = []
  const snaked = snake(queryObj)
  const snakedQueryParams = snake(queryParams)
  const keys = Object.keys(snaked)
  keys.forEach(K => {
    const current = snaked[K]
    if (!current) return null
    const type = snakedQueryParams[K]
    const { string, val } = builders[type]({ [K]: current })
    strings.push(string)
    val && values.push(val)
  })
  const assembledString = strings.length !== 0
    ? ` ${strings.join(' and ')} `
    : ''
  const result = {
    assembledString,
    values
  }
  return result
}