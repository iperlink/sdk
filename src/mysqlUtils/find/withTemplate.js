const assembleWhere = require('./assembleWhere')
const execute = require('../query')

module.exports = async ({ template, queryObj, db, filteredString, fieldsString }) => {
  const { queryParams, table } = template
  if (!queryObj || Object.keys(queryObj).length === 0) {
    throw new Error(JSON.stringify({
      code: 'noQueryObj',
      message: 'query object does not exist or is empty'
    }))
  }
  const FieldsString = fieldsString || template.fieldsString
  const { assembledString, values } = assembleWhere({ queryObj, queryParams })
  const query = ` SELECT ${FieldsString} FROM ${db}.${table} WHERE ${filteredString} ${assembledString}`
  const [res] = await execute({ query, params: values })
  return res

}