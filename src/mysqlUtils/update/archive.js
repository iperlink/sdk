const { getServices } = require('../../serviceStarter')
const errorCodes = require('../errors/codes')

module.exports = async ({ db, table, id }) => {
  const { mysql } = getServices()
  const findQuery = `SELECT * FROM ${db}.${table} WHERE id= ?`
  try {
    const [found] = await mysql.query(findQuery, [id])
    const content = JSON.stringify(found[0])
    const toInsert = {
      content,
      table,
      version: found[0].version,
    }
    return toInsert
  } catch (e) {
    if (errorCodes[e.code]) {
      return errorCodes[e.code](e, values, table, id)
    }
    return e
  }
}