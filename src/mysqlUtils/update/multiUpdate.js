
const { getServices } = require('../../serviceStarter')

module.exports = async ({ db, table, values, id, param }) => {
  const { mysql } = getServices()
  const query = `UPDATE ${db}.${table} set ? WHERE ${param} in (?)`
  const promiseArray = values.map(val => {
    return mysql.query(query, [val, id])
  })
  await Promise.all(promiseArray)
}

