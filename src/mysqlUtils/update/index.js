const snake = require('../../objMapper/snakeKeys')
const errorCodes = require('../errors/codes')
const execute = require('../query')
const checkForbiden = require('./checkForbiden')

module.exports = async ({ table, values, id, forbiden }) => {
  const db = process.env.DB_NAME
  checkForbiden({ forbiden, values })
  const query = `UPDATE ${db}.${table} set ? WHERE id = ?`
  const snakedValues = snake(values)
  try {
    const res = await execute({ query, params: [snakedValues, id] })
    if (res.code) return res.code
    if (res[0].affectedRows === 1) {
      return res[0].affectedRows
    }
    console.error(`${db}.${table} id =${id} does not exist`)
    return { code: 'RefDoesNotExist' }
  } catch (e) {
    return e
  }
}
