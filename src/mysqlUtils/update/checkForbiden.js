

module.exports = async ({ values, forbiden, table }) => {
  let cantUpdateParam = false
  if (forbiden) {
    forbiden.forEach(param => {
      if (values[param]) {
        cantUpdateParam = true
      }
    })
  }
  if (cantUpdateParam) {
    throw new Error(JSON.stringify({
      code: 'updateForbidenParams',
      message: `request contains forbiden params ${table} ${values}`
    }))
  }
}
