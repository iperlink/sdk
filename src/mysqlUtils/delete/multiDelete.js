const { getServices } = require('../../serviceStarter')

module.exports = async ({ db, paramValue, table, param, where }) => {
  const Where = where || ''
  const {
    mysql
  } = getServices()
  const query = `DELETE FROM ${db}.${table} WHERE ${Where} ${param} in (?)`
  await mysql.query(query, [paramValue])
}
