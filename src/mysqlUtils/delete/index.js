const { getServices } = require('../../serviceStarter')

module.exports = async ({ db, id, table }) => {
  const {
    mysql
  } = getServices()

  await mysql.query(`DELETE FROM ${db}.${table} WHERE id = ?`, [id])
}
