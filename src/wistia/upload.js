const Wistia = require('wistia-js')(process.env.WISTIA_KEY);
const WistiaUpload = Wistia.WistiaUpload();

module.exports = (url) => {
  return new Promise((resolve, reject) => {
    WistiaUpload.upload({
      project_id: 'vl73i89qfx',
      url,
    }, (error, data) => {
      if (error) {
        console.error('accountError', error)
        reject()
      }
      const parsed = JSON.parse(data)
      resolve({ id: parsed.id, name: parsed.name })
    });
  })
}
