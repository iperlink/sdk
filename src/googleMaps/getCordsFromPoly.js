
module.exports = (poly) => {
  const { features } = poly
  const { geometry } = features[0]
  const { coordinates } = geometry
  if (Array.isArray(coordinates[0])) {
    const latLng = coordinates[0].map(F => {
      return { lng: F[0], lat: F[1] }
    })
    return latLng
  }
  const result = {
    lng: coordinates[0],
    lat: coordinates[1],
  }
  return result
}