const savePolygon = require('./savePoligon')

module.exports = (dataLayer) => {
  dataLayer.addListener('addfeature', () => savePolygon(dataLayer));
  dataLayer.addListener('removefeature', () => savePolygon(dataLayer));
  dataLayer.addListener('setgeometry', () => savePolygon(dataLayer));
}
