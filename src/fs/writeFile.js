const fs = require('fs')

module.exports = ({ name, content }) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(name, content, (err, data) => {
      if (err) return reject(err)
      resolve(data)
    })
  })
}