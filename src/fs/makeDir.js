const fs = require('fs')
const removeDir = require('./removeDir')

module.exports = async (name) => {
  const exist = fs.existsSync(name)
  if (exist) return
  const dir = fs.mkdirSync(name, 0744)
  return dir
}