const rimraf = require('rimraf')
const fs = require('fs')


module.exports = (name) => {
  return new Promise((resolve, reject) => {
    const exist = fs.existsSync(name)
    if (!exist) resolve()
    rimraf(name, (err, data) => {
      if (err) return reject(err)
      resolve()
    })
  })
}