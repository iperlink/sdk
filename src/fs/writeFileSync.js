const fs = require('fs')
const requiredKeys = require('../objMapper/requiredKeys')

module.exports = ({ content, name }) => {
  requiredKeys({ obj: { content, name }, required: ['content', 'name'] })
  fs.writeFileSync(name, content)
}