const assemble = require('@opensafety/sdk/src/deploy')
const functions = require('./src/functions')
const env = require('./env')
const makeSchema = require('./schema')
const views = require('./views')
const materializeTables = require('./materializeStructure/tables')

const schema = makeSchema()

deploy = async () => {
  try {
    await assemble({
      functions,
      env,
      schema,
      views,
      materializeTables,
    })
  } catch (e) {
    console.error(e)
    process.exit()
  }
}



deploy().then()

