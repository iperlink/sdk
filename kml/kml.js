var initMap = () => {
  const text = window.location.href;
  // let id = text.split('?')[1].split('=')[1]
  let id = '20181225-17-31.kml';
  const map = new google.maps.Map(document.getElementById('map'), {
    center: new google.maps.LatLng(-19.257753, 146.823688),
    zoom: 2,
    mapTypeId: 'satellite'
  })
  const kmlLayer = new google.maps.KmlLayer('https://s3.amazonaws.com/secdroneimages/' + id, {
    suppressInfoWindows: true,
    preserveViewport: false,
    map,
  })
  var drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.MARKER,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: ['polygon',]
    },
    circleOptions: {
      fillColor: '#ffff00',
      fillOpacity: 1,
      strokeWeight: 5,
      clickable: false,
      editable: true,
      zIndex: 1
    }
  });
  const polygons = []
  google.maps.event.addDomListener(drawingManager, 'polygoncomplete', function (polygon) {
    polygons.push(polygon);
  });
  drawingManager.setMap(map);
  // kmlLayer.addListener('click', event => {
  //   const content = event.featureData.infoWindowHtml
  //   const testimonial = document.getElementById('capture')
  //   testimonial.innerHTML = content
  // });
}


// var map;
// function initMap() {
//   map = new google.maps.Map(document.getElementById('map'), {
//     center: { lat: -34.397, lng: 150.644 },
//     zoom: 4,
//     // only show roadmap type of map, and disable ability to switch to other type
//     mapTypeId: google.maps.MapTypeId.ROADMAP,
//     mapTypeControl: false
//   });

//   map.data.setControls(['Polygon']);
//   map.data.setStyle({
//     editable: true,
//     draggable: true
//   });
//   bindDataLayerListeners(map.data);

//   //load saved data
//   loadPolygons(map);
// }


// // Apply listeners to refresh the GeoJson display on a given data layer.
// function bindDataLayerListeners(dataLayer) {
//   dataLayer.addListener('addfeature', savePolygon);
//   dataLayer.addListener('removefeature', savePolygon);
//   dataLayer.addListener('setgeometry', savePolygon);
// }

// function loadPolygons(map) {
//   var data = JSON.parse(localStorage.getItem('geoData'));

//   map.data.forEach(function (f) {
//     map.data.remove(f);
//   });
//   map.data.addGeoJson(data)
// }



// function savePolygon() {
//   map.data.toGeoJson(function (json) {
//     console.log(json)
//     localStorage.setItem('geoData', JSON.stringify(json));
//   });
// }
// initMap();