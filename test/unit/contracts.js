const fs = require('fs');
const groupBy = require('lodash/groupBy')
const keyBy = require('lodash/keyBy')
const mapValues = require('lodash/mapValues')


const contracts = './test/unit/contracts.json';
const parsed = JSON.parse(fs.readFileSync(contracts, 'utf8'));
const mission = './test/unit/mision.json';
const missionParsed = JSON.parse(fs.readFileSync(mission, 'utf8'));
const misionMapped = missionParsed.map(x => ({ id: x._id, area: x.area, coordinador: x.ground_coordinator, contract: x.contract }))
const keyedcontracts = keyBy(parsed, '_id')
console.log(keyedcontracts)
const remaped = misionMapped.map(x => {
  const contrato = keyedcontracts[x.contract]
    ? keyedcontracts[x.contract].name
    : ''
  return { ...x, contrato }
})
const file = 'contratos.json'
fs.writeFileSync(file, JSON.stringify(remaped, null, 2))
console.log(remaped)