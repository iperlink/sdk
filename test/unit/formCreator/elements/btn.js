const proxyquire = require('proxyquire')
const test = require('ava')
const mockReact = require('../../../../src/formCreator/react')

test('#btnall elements are rendered ', t => {
  const dependencies = {
    'react': mockReact
  }
  const btn = proxyquire('../../../../src/formCreator/elements/btn', dependencies)
  const rendered = btn({
    className: 'cl',
    id: 'id',
    message: 'mem',
    onClick: 'clk',
  })
  t.is(rendered, 'mem')
})