const proxyquire = require('proxyquire')
const test = require('ava')

const mockReact = require('../../../../src/formCreator/react')

test('#invalid elements are rendered on invalid', t => {
  const dependencies = {
    'react': mockReact
  }
  const invalid = proxyquire('../../../../src/formCreator/elements/invalid', dependencies)
  const rendered = invalid({
    invalid: true,
    id: 'id',
    invalidMsg: 'mem',
  })
  t.is(rendered, 'mem')
})

test('#invalid returns null if invalid is null', t => {
  const dependencies = {
    'react': mockReact
  }
  const invalid = proxyquire('../../../../src/formCreator/elements/invalid', dependencies)
  const rendered = invalid({
    invalid: null,
    id: 'id',
    invalidMsg: 'mem',
  })
  t.is(rendered, null)
})