const proxyquire = require('proxyquire')
const test = require('ava')
const mockReact = require('../../../../../../src/formCreator/react')

test('#select valid input all elements are rendered ', t => {
  const makeList = () => 'theList'
  const dependencies = {
    'react': mockReact,
    './list': makeList
  }

  const field = {
    classes: { containerCL: 'cCl', inputCl: 'IcL', labelCl: 'LcL' },
    id: 'firstId',
    name: 'this is the name',
    onChange: 'changeFunc',
    value: 2,
    options: [
      { label: 'one', value: 1 },
      { label: 'two', value: 2 },
      { label: 'three', value: 3 }
    ]
  }
  const select = proxyquire(`../../../../../../src/formCreator/fieldCreator/types/select`, dependencies)
  const values = {
    first: 'firstValue'
  }

  const rendered = select(field)
  t.deepEqual(rendered, ['this is the name', 'theList'])
})
