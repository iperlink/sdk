const proxyquire = require('proxyquire')
const test = require('ava')
const mockReact = require('../../../../../../src/formCreator/react')

test('#select list all elements are rendered ', t => {
  const dependencies = {
    'react': mockReact,
  }

  const elements = [
    { label: 'one', value: 1 },
    { label: 'two', value: 2 },
    { label: 'three', value: 3 }
  ]
  const value = 2
  const id = 'theId'
  const list = proxyquire('../../../../../../src/formCreator/fieldCreator/types/select/list', dependencies)


  const rendered = list({ elements, id, value })
  t.deepEqual(rendered, ['one', 'two', 'three'])
})
