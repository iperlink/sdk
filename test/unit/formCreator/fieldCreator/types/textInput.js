import proxyquire from 'proxyquire'
import sinon from 'sinon'
import test from 'ava'

import libDir from './libDir'
import mockReact from '../../../lib/react'


test('#text valid input all elements are rendered ', t => {
  const dependencies = {
    'react': mockReact
  }
  const field = {
    classes: { containerCL: 'cCl', inputCl: 'IcL', labelCl: 'LcL' },
    id: 'firstId',
    name: 'this is the name',
    onChange: 'changeFunc',
    value: 'val',
    }
  const textInput = proxyquire(`${libDir}/textInput`, dependencies).default
  const values = {
    first: 'firstValue'
  }

  const rendered = textInput(field)
  t.deepEqual(rendered, [ 'this is the name', 'input' , null])
})

test('#text invalid input all elements are rendered ', t => {
  const makeInvalid = sinon.stub()
  const dependencies = {
    'react': mockReact,
    '../../elements/invalid': { default: makeInvalid }
  }
  makeInvalid.withArgs({
    id: 'firstId',
    invalid: true,
    invalidMsg: 'tis invalid'
  }).returns('tis invalid')

  const field = {
    classes: { containerCL: 'cCl', inputCl: 'IcL', labelCl: 'LcL' },
    id: 'firstId',
    name: 'this is the name',
    onChange: 'changeFunc',
    value: 'val',
    invalid: true,
    invalidMsg: 'tis invalid'
    }
  const textInput = proxyquire(`${libDir}/textInput`, dependencies).default
  const values = {
    first: 'firstValue'
  }

  const rendered = textInput(field)
  t.deepEqual(rendered, [ 'this is the name', 'input' , 'tis invalid'])
})
test('#text input is bound with onChange', t => {
  const createElement= sinon.spy()
  const dependencies = {
    'react': {createElement}
  }
  const field = {
    classes: { containerCL: 'cCl', inputCl: 'IcL', labelCl: 'LcL' },
    id: 'firstId',
    name: 'this is the name',
    onChange: 'changeFunc',
    value: 'val',
    invalid: true,
    invalidMsg: 'tis invalid'
    }
  const textInput = proxyquire(`${libDir}/textInput`, dependencies).default
  const inputArgs = [
    'input',
    {
      className: 'IcL',
      id: 'firstId',
      key: 'firstIdinput',
      onChange: 'changeFunc',
      value: 'val'
    }
  ]
  const labelArgs = [
    'label',
    {
      className: 'LcL',
      key: 'firstIdlabel',
      htmlFor: 'firstId'
    },
    'this is the name'
  ]

  const rendered = textInput(field)
  t.deepEqual(createElement.getCall(0).args, inputArgs)
  t.deepEqual(createElement.getCall(1).args, labelArgs)
})
