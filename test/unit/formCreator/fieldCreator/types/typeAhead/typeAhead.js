import proxyquire from "proxyquire"
import sinon from 'sinon'
import test from 'ava'

import mockReact from "../../../../lib/react"
import libDir from '../libDir'

test('#typeAhead valid input all elements are rendered ', t => {
  const makeList = () => 'theList'
  const dependencies = {
    'react': mockReact,
    './list': { default: makeList }
  }

  const field = {
    classes: { containerCL: 'cCl', inputCl: 'IcL', labelCl: 'LcL' },
    id: 'firstId',
    name: 'this is the name',
    onChange: 'changeFunc',
    value: 'val',
    options: ['one', 'two', 'three']
    }
  const typeAhead = proxyquire(`${libDir}/typeAhead`, dependencies).default
  const values = {
    first: 'firstValue'
  }

  const rendered = typeAhead(field)
  t.deepEqual(rendered, [ 'this is the name', 'input', 'theList', null ])
})

test('#typeAhead invalid input all elements are rendered ', t => {
  const makeList = () => 'theList'
  const makeInvalid = sinon.stub()
  const dependencies = {
    './list': { default: makeList },
    'react': mockReact,
    '../../../elements/invalid': { default: makeInvalid }
  }
  makeInvalid.withArgs({
    id: 'firstId',
    invalid: true,
    invalidMsg: 'tis invalid'
  }).returns('tis invalid')
  const field = {
    classes: { containerCL: 'cCl', inputCl: 'IcL', labelCl: 'LcL' },
    id: 'firstId',
    name: 'this is the name',
    onChange: 'changeFunc',
    value: 'val',
    invalid: true,
    options: ['one', 'two', 'three'],
    invalidMsg: 'tis invalid'
    }
  const typeAhead = proxyquire(`${libDir}/typeAhead`, dependencies).default
  const values = {
    first: 'firstValue'
  }

  const rendered = typeAhead(field)
  t.deepEqual(rendered, [
    'this is the name',
    'input',
    'theList',
    'tis invalid'
  ])
})

test('#typeAhead is bound with onChange', t => {
  const createElement= sinon.spy()
  const dependencies = {
    'react': {createElement}
  }
  const field = {
    classes: { containerCL: 'cCl', inputCl: 'IcL', labelCl: 'LcL' },
    id: 'firstId',
    name: 'this is the name',
    onChange: 'changeFunc',
    value: 'val',
    invalid: true,
    options: ['one', 'two', 'three'],
    invalidMsg: 'tis invalid'
    }
  const textInput = proxyquire(`${libDir}/typeAhead`, dependencies).default
  const inputArgs = [
    'input',
    {
      className: 'IcL',
      id: 'firstId',
      key: 'firstIdinput',
      onChange: 'changeFunc',
      value: 'val'
    }
  ]
  const labelArgs = [
    'label',
    {
      className: 'LcL',
      key: 'firstIdlabel',
      htmlFor: 'firstId'
    },
    'this is the name'
  ]

  const rendered = textInput(field)
  t.deepEqual(createElement.getCall(0).args, inputArgs)
  t.deepEqual(createElement.getCall(1).args, labelArgs)
})
