import proxyquire from 'proxyquire'
import sinon from 'sinon'
import test from 'ava'

import libDir from '../libDir'
import mockReact from '../../../../lib/react'

test(
  '#typeAhead list all elements are rendered less than 8 '
  , t => {
  const dependencies = {
    'react': mockReact,
  }
  const list = proxyquire(`${libDir}/typeAhead/list`, dependencies).default
  const elements = ['one', 'two', 'three']
  const className = 'class'
  const id = 'theId'
  const onClick = 'click'
  const rendered = list({elements, className, id, onClick})
  t.deepEqual(rendered, [ 'one', 'two', 'three' ])
})

test(
  '#typeAhead list all elements are rendered more than 8 '
  , t => {
  const dependencies = {
    'react': mockReact,
  }
  const list = proxyquire(`${libDir}/typeAhead/list`, dependencies).default
  const elements = [
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eigth',
    'nine'
  ]
  const className = 'class'
  const id = 'theId'
  const onClick = 'click'
  const rendered = list({elements, className, id, onClick})
  t.deepEqual(rendered, [
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eigth',
    '... + 1'
  ])
})