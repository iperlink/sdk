import proxyquire from "proxyquire"
import sinon from 'sinon'
import test from 'ava'

import mockReact from "../../lib/react"
import libDir from './libDir'

const fields = {
  first:{
    name: 'first',
    type: 'text',
    config: {
      placeholder: 'Ingrese nombre...',
      label: 'Nombre'
    }
  }
}

const handleChange = () => 'hola'

test('#fieldCreator/all elements are rendered', t => {
  const textSpy = sinon.spy()
  const onChange = {text:sinon.spy()}
  const dependencies = {
    './types': {
      default: {
        text: () => 'text'
      }
    },
    './onChange': onChange,
    'react': mockReact 
  }
  const fieldCreator = proxyquire(libDir, dependencies).default
  const values = {
    first: 'firstValue'
  }
  const textArgs = [{
    name: 'first',
    type: 'text',
    config: { placeholder: 'Ingrese nombre...', label: 'Nombre' },
    value: 'firstValue',
  }]

  const rendered = fieldCreator({ fields, handleChange, values, key:1 })
  const compareTo = ['text']
  t.deepEqual(rendered, compareTo)
})


test('#fieldCreator/builds fields array', t => {
  const textSpy = sinon.spy()
  const onChange = {text:sinon.spy()}
  const dependencies = {
    './types': {
      default: {
        text: textSpy
      }
    },
    './onChange': onChange,
    'react': mockReact 
  }
  const fieldCreator = proxyquire(libDir, dependencies).default
  const values = {
    first: 'firstValue'
  }
  const textArgs = [{
    name: 'first',
    type: 'text',
    config: { placeholder: 'Ingrese nombre...', label: 'Nombre' },
    value: 'firstValue',
  }]

  fieldCreator({ fields, handleChange, values })
  t.deepEqual(textSpy.firstCall.args.name, textArgs.name)
  t.deepEqual(textSpy.firstCall.args.type, textArgs.type)

  t.true(textSpy.calledOnce)
})
