const sinon = require('sinon')
const test = require('ava')
const onChangeSelect = require('../../../../../src/formCreator/fieldCreator/onChange/select')

test('#select onChange calls all functions', async t => {
  const onChangeField = sinon.stub()
  onChangeField.withArgs('val1').returns('changed_val')
  const e = {
    target:
    {
      options: [
        { label: 'label', value: 'val' },
        { label: 'label1', value: 'val1' }
      ],
      selectedIndex: 1,
    }
  }
  const field = { onChange: onChangeField, name: 'fieldName' }
  const formOnChange = sinon.spy()
  onChangeSelect({ e, field, formOnChange })
  const compareTo = [{
    value: 'val1',
    name: 'fieldName',
    fromFieldOnChange: 'changed_val'
  }]
  t.deepEqual(formOnChange.getCall(0).args, compareTo)
})

