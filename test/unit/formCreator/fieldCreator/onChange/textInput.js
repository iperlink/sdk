const sinon = require('sinon')
const test = require('ava')
const onChangeTextInput = require('../../../../../src/formCreator/fieldCreator/onChange/textInput')

test('#textInput onChange calls all functions', async t => {
  const onChangeField = sinon.stub()
  onChangeField.withArgs('val').returns('changed_val')
  const e = { target: { value: 'val' } }
  const field = { onChange: onChangeField, name: 'fieldName' }
  const formOnChange = sinon.spy()
  onChangeTextInput({ e, field, formOnChange })
  const compareTo = [{
    value: 'val',
    name: 'fieldName',
    fromFieldOnChange: 'changed_val'
  }]
  t.deepEqual(formOnChange.getCall(0).args, compareTo)
})

