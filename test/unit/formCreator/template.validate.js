import test from 'ava'
import validator from '../../../src/formCreator/template.validate'

const template = {
  text1: {
    classes: 'tempClasses',
    id: `theID`,
    name: `textTest`,
    onChange: () => 'test',
    asyncFn: () => 'async',
    type: 'typeAhead',
    value: 'this is a test',
    invalidMsg: 'tis invalid'
  }
}
test('validates template', t => {
  validator(template)
  t.is(1, 1)
})