import test from 'ava'
import sinon from 'sinon'
import proxyquire from 'proxyquire'
import libDir from '../libDir'

test('#onChange/calls optionsFilters', t => {
  const optionsFilters = sinon.stub()
  optionsFilters.withArgs({
    options: ['a', 'b', 'c'],
    value: 'b'
  }).returns('b')

  const dependencies = {
    './optionFilters': {
      default: { typeAhead: optionsFilters }
    }
  }
  const onChange = proxyquire(`${libDir}/handlers/onChange`, dependencies).default

  const fields = { first: { type: 'typeAhead', options:['a', 'b'], value: 'x' } }
  const original = { first: { type: 'typeAhead', options:['a', 'b', 'c'], value: 'x' } }
  const newFields = onChange({
    fields,
    invalid: false,
    name: 'first',
    original,
    value: 'b',
  })
  const compareTo = {
    first: {
      type: 'typeAhead',
      options: 'b',
      value: 'b',
      invalid: undefined
    }
  }

  t.deepEqual(newFields,compareTo )
})
