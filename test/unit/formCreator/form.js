import test from "ava"
import sinon from "sinon"
import proxyquire from "proxyquire"
import mockReact from "../lib/react"
import libDir from './libDir'

test("#form/renders all elements", t => {
  const dependencies = {
    './fieldCreator': {
      default: ({ fields }) => {
        return ['a', 'b', 'c']
      }
    },
    './elements/btn': {
        default: () => "this is the injected btn"
      },
    'react': mockReact 
  }
  const form = proxyquire(libDir, dependencies).default

  const fields = {a:"a", b:"b", c:"c"}
  const Form = new form({ text: "hola", fields, id:'newForm' })
  const rendered = Form.render()
  const compareTo = [["a", "b", "c"], "this is the injected btn"]
  t.deepEqual(rendered, compareTo)
})

test('#form/calls submit on button clik', async t => {
  const dependencies = {
    './fieldCreator': {
      default: ({ fields }) => {
        return fields
      }
    },
    './elements/btn': {
      default:({ onClick }) => {
        return {
          key: 'btn',
          fn: () => {
            onClick()
          }
        }
      }
    },
    'react': mockReact 
  }
  const form = proxyquire(libDir, dependencies).default
  const submitFn = sinon.spy()
  const fields = ['a', 'b', 'c']

  
  const Form = new form({
    text: 'hola',
    submitFn,
    fields,
    id:'newForm'

  })
  const rendered = Form.render()
  t.is(Form.state.status, 'waiting')
  const pressButton = rendered.find(x => x.key === 'btn').fn
  await pressButton()
  t.is(Form.state.status, 'submiting')
  t.true(submitFn.calledOnce)
})

test('#form/adds on change function to fields', async t => {
  const fieldCreator = sinon.spy()
  const dependencies = {
    './fieldCreator': {default: fieldCreator},
    './elements/btn': { default: () =>'btn' },
    'react': mockReact 
  }
  const form = proxyquire(libDir, dependencies).default

  const fields = ['a']
  const Form = new form({
    text: 'hola',
    submitFn:'',
    fields,
    id:'newForm'
  })
  Form.render()
  t.truthy(fieldCreator.getCall(0).args[0].onChange)
  t.is(Form.state.status, 'waiting')
})

test('#form/handles change', async t => {
  const fields = {
      first:{
      type: 'text',
      value: 'start',
      onChange: () => 'aa'
    }
  }
  const onChangeHandler = ({value, name}) => {
    const field = fields[name]
    const newField = {
      ...field,
      value: value,
    }
    const newFields = {
      ...fields,
      [name]: newField
    }
    return newFields
  }

  const dependencies = {
    './fieldCreator': {
      default: ({ fields, onChange }) => {
        const maped = Object.values(fields).map(field => {
          return {
            ...field,
            onChange: () => onChange({value: 'val', name: 'first'})
          }
        })
        return maped
      }
    },
    './elements/btn': ({ onClick }) => {
      return {
        key: 'btn',
        fn: () => {
          onClick()
        }
      }
    } ,
    './handlers/onChange': {default: onChangeHandler},
    'react': mockReact 
  }
  const form = proxyquire(libDir, dependencies).default
  const submitFn = sinon.spy()
  const Form = new form({
    text: 'hola',
    submitFn,
    fields,
    id:'newForm'
  })
  const rendered = Form.render()
  const {onChange} = rendered[0][0]
  t.is(Form.state.fields.first.value, 'start')
  onChange()
  t.is(Form.state.fields.first.value, 'val')
})
