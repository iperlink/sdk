const test = require('ava')
const collectionKeys = require('../../../src/objMapper/collectionKeys')

test('#collectionKeys/', (t) => {
  const array = [
    { testKey: 'testVal', other: 1 },
    { testKey: 'testVal2', other: 2 },
    { testKey: 'testVal', other: 1 },
  ]
  const extracted = collectionKeys(array)
  const compareTo = ['testKey', 'other']
  t.deepEqual(extracted, compareTo)
})
