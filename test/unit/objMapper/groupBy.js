const test = require('ava')
const groupBy = require('../../../src/objMapper/groupBy')

test('#groupBy/', (t) => {
  const array = [
    { testKey: 'testVal', other: 1 },
    { testKey: 'testVal2', other: 2 },
    { testKey: 'testVal', other: 1 },
    { testKey: 'testVal2', other: 2 },
    { testKey: 'testVal', other: 1 },
  ]
  const grouped = groupBy({ array, key: 'testKey' })
  const compareTo = {
    testVal:
      [{ testKey: 'testVal', other: 1 },
      { testKey: 'testVal', other: 1 },
      { testKey: 'testVal', other: 1 }],
    testVal2:
      [{ testKey: 'testVal2', other: 2 },
      { testKey: 'testVal2', other: 2 }]
  }
  t.deepEqual(grouped, compareTo)
})
