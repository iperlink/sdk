const test = require('ava')
const appendEach = require('../../../src/objMapper/appendEach')

test('#appendEach/', (t) => {
  const array = [
    { testKey: 'testVal', other: 1 },
    { testKey: 'testVal2', other: 2 },
    { testKey: 'testVal', other: 1 },
  ]
  const extracted = appendEach({ array, toAppend: { def: 'default' } })
  const compareTo = [{ testKey: 'testVal', other: 1, def: 'default' },
  { testKey: 'testVal2', other: 2, def: 'default' },
  { testKey: 'testVal', other: 1, def: 'default' }]
  t.deepEqual(extracted, compareTo)
})
