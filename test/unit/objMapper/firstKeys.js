const test = require('ava')
const firstKey = require('../../../src/objMapper/firstKey')

test('#firstKey/', (t) => {
  const obj = {
    first: 'val',
    xtracted: 'ex1',
  }


  const extractedValues = firstKey(obj)
  const compareTo = 'first'
  t.deepEqual(extractedValues, compareTo)
})
