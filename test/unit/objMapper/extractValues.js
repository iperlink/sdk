const test = require('ava')
const extractValues = require('../../../src/objMapper/extractValues')

test('#extractValues/', (t) => {
  const obj = {
    k1: {
      first: 'val',
      xtracted: 'ex1',
    },
    k2: {
      first: 'val2',
      xtracted: 'ex2',
    }
  }
  const extractedValues = extractValues({ obj, value: 'xtracted' })
  const compareTo = { k1: 'ex1', k2: 'ex2' }
  t.deepEqual(extractedValues, compareTo)
})
