const test = require('ava')
const requiredKeys = require('../../../src/objMapper/requiredKeys')

test('#requiredKeys/', (t) => {
  const obj = {
    first: 'val',
    xtracted: 'ex1',
  }
  const required = ['extra']
  t.throws(() => requiredKeys({ obj, required }))
})
