const test = require('ava')
const firstValue = require('../../../src/objMapper/firstValue')

test('#firstValue/', (t) => {
  const obj = {
    first: 'val',
    xtracted: 'ex1',
  }


  const extractedValues = firstValue(obj)
  const compareTo = 'val'
  t.deepEqual(extractedValues, compareTo)
})
