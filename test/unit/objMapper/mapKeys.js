const test = require('ava')
const mapKeys = require('../../../src/objMapper/mapKeys')

test('#mapKeys/', (t) => {
  const obj = { testKey: 'testVal' }
  const fn = k => k.toUpperCase()
  const mapped = mapKeys({ obj, fn })
  const compareTo = { TESTKEY: 'testVal' }
  t.deepEqual(mapped, compareTo)
})
