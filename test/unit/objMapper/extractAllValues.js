const test = require('ava')
const extractAllValues = require('../../../src/objMapper/extractAllValues')

test('#extractAllValues/', (t) => {
  const array = [
    { testKey: 'testVal', other: 1 },
    { testKey: 'testVal2', other: 2 },
    { testKey: 'testVal', other: 1 },
  ]
  const extracted = extractAllValues(array)
  const compareTo = [
    ['testVal', 1],
    ['testVal2', 2],
    ['testVal', 1],
  ]
  t.deepEqual(extracted, compareTo)
})
