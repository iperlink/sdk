const test = require('ava')
const mapObj = require('../../../src/objMapper/mapObj')

test('#mapObj/', (t) => {
  const obj = { testKey: 'testVal' }
  const keyfn = k => k.toUpperCase()
  const valfn = k => k.toUpperCase()
  const mapped = mapObj({ obj, keyfn, valfn })
  const compareTo = { TESTKEY: 'TESTVAL' }
  t.deepEqual(mapped, compareTo)
})
