const test = require('ava')
const snakeKeys = require('../../../src/objMapper/snakeKeys')

test('#snakeKeys/', (t) => {
  const obj = { testKey: 'testVal', secondKey: 'val' }
  const mapped = snakeKeys(obj)
  const compareTo = { test_key: 'testVal', second_key: 'val' }
  t.deepEqual(mapped, compareTo)
})
