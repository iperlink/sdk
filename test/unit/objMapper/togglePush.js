const test = require('ava')
const tooglePush = require('../../../src/objMapper/togglePush')

test('#togglePush/', (t) => {
  const added = tooglePush({ array: ['a', 'b', 'c'], value: 'd' })
  const subtracted = tooglePush({ array: ['a', 'b', 'c'], value: 'b' })


  const compareTo = ['a', 'b', 'c', 'd']
  const compareTo2 = ['a', 'c']
  t.deepEqual(added, compareTo)
  t.deepEqual(subtracted, compareTo2)
})
