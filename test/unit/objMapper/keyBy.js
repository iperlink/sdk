const test = require('ava')
const keyBy = require('../../../src/objMapper/keyBy')

test('#keyBy/', (t) => {
  const array = [
    { testKey: 'testVal', other: 1 },
    { testKey: 'testVal2', other: 2 },
  ]
  const grouped = keyBy({ array, key: 'testKey' })
  const compareTo = {
    testVal: { testKey: 'testVal', other: 1 },
    testVal2: { testKey: 'testVal2', other: 2 }
  }
  t.deepEqual(grouped, compareTo)
})
