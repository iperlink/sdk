const test = require('ava')
const groupSingleVal = require('../../../src/objMapper/groupSingleVal')

test('#groupSingleVal/', (t) => {
  const array = [
    { testKey: 'testVal', other: 1 },
    { testKey: 'testVal2', other: 2 },
    { testKey: 'testVal', other: 1 },
    { testKey: 'testVal2', other: 2 },
    { testKey: 'testVal', other: 1 },
  ]
  const grouped = groupSingleVal({ array, key: 'testKey', val: 'other' })
  const compareTo = { testVal: [1, 1, 1], testVal2: [2, 2] }
  t.deepEqual(grouped, compareTo)
})
