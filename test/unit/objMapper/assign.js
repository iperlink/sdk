const test = require('ava')
const assign = require('../../../src/objMapper/assign')

test('#assign/', (t) => {
  const obj = {
    key: 'val'
  }
  const newObj = {
    key2: 'val2'
  }
  const assigned = assign({ obj, newObj })
  t.deepEqual(assigned, { key: 'val', key2: 'val2' })
})

test('#assign overwrite/', (t) => {
  const obj = {
    key: 'val'
  }
  const newObj = {
    key: 'val2'
  }
  const assigned = assign({ obj, newObj })
  t.deepEqual(assigned, { key: 'val2' })
})