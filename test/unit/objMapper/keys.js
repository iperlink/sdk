const test = require('ava')
const keys = require('../../../src/objMapper/keys')

test('#keys/', (t) => {
  const obj = {
    first: 'val',
    xtracted: 'ex1',
  }


  const keyed = keys(obj)
  const compareTo = ['first', 'xtracted']
  t.deepEqual(keyed, compareTo)
})
