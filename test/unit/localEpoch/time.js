const test = require('ava')
const time = require('../../../src/localEpoch/time')

test('#localEpoch/time', (t) => {
  const epoch = time('00:00')
  t.is(epoch, 10800)
  t.pass()
})
