const test = require('ava')
const day = require('../../../src/localEpoch/day')

test('#localEpoch/day', (t) => {
  const epoch = day('1970-01-01')
  t.is(epoch, 10800)
  t.pass()
})
