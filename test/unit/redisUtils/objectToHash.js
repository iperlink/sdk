const test = require('ava')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

test('#redisUtils/objectToHash', async (t) => {
  const getServices = sinon.stub()
  const hset = sinon.spy()
  const redis = {
    hset
  }
  getServices.returns({ redis })
  const dependencies = {
    '../serviceStarter': { getServices }
  }
  const objectTohash = proxyquire(
    '../../../src/redisUtils/objectTohash',
    dependencies)
  const hash = 'thehash'
  const obj = { key: 'theVal' }
  await objectTohash({ hash, obj })
  const args = hset.getCall(0).args
  const compareTo = ['thehash', ['key', '"theVal"']]
  t.deepEqual(args, compareTo)
})
