const test = require('ava')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

test('#redisUtils/buildCache', async (t) => {
  const getServices = sinon.stub()
  const execute = sinon.stub()
  const hmset = sinon.spy()
  const redis = {
    hmset
  }
  const mysql = {
    execute
  }
  execute.withArgs('SELECT * from theDb.theTable').returns([[
    { key: 1, val: 'val1' },
    { key: 2, val: 'val2' },
    { key: 3, val: 'val3' }
  ]])
  getServices.returns({ mysql, redis })
  const dependencies = {
    '../serviceStarter': { getServices }
  }
  const buildCache = proxyquire(
    '../../../src/redisUtils/buildCache',
    dependencies)
  const db = 'theDb'
  const key = 'key'
  const table = 'theTable'
  await buildCache({ db, key, table })
  const params = [1,
    '{"key":1,"val":"val1"}',
    2,
    '{"key":2,"val":"val2"}',
    3,
    '{"key":3,"val":"val3"}']
  const args = ['theTable', params]
  t.deepEqual(args, hmset.getCall(0).args)
})
