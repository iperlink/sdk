const test = require('ava')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

test('#redisUtils/hashGet', async (t) => {
  const getServices = sinon.stub()
  const hget = sinon.stub()
  const redis = {
    hget
  }
  getServices.returns({ redis })
  hget.withArgs('theHash', 'theKey').returns(JSON.stringify({ key: 'val' }))
  const dependencies = {
    '../serviceStarter': { getServices }
  }
  const hashGet = proxyquire(
    '../../../src/redisUtils/hashGet',
    dependencies)
  const hash = 'theHash'
  const key = 'theKey'
  const res = await hashGet({ hash, key })
  t.is(res.key, 'val')
})
