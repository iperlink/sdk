const test = require('ava')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

test('#redisUtils/appendToHash hash exist', async (t) => {
  const hset = sinon.spy()
  const hget = sinon.stub()
  const getServices = sinon.stub()
  const redis = {
    hget,
    hset
  }
  getServices.returns({ redis })
  const dependencies = {
    '../serviceStarter': { getServices }
  }
  hget.withArgs('theHash', 'theKey')
    .returns(JSON.stringify(['a']))
  const appendToHash = proxyquire(
    '../../../src/redisUtils/appendToHash',
    dependencies)
  const hash = 'theHash'
  const key = 'theKey'
  const val = 'theVal'
  await appendToHash({ hash, key, val })
  const args = hset.getCall(0).args
  const compareTo = ['theHash', 'theKey', '["a","theVal"]']
  t.deepEqual(args, compareTo)
})


test('#redisUtils/appendToHash hash does not exist', async (t) => {
  const hset = sinon.spy()
  const hget = sinon.stub()
  const getServices = sinon.stub()
  const redis = {
    hget,
    hset
  }
  getServices.returns({ redis })
  const dependencies = {
    '../serviceStarter': { getServices }
  }
  hget.withArgs('theHash', 'theKey')
    .returns(null)
  const appendToHash = proxyquire(
    '../../../src/redisUtils/appendToHash',
    dependencies)
  const hash = 'theHash'
  const key = 'theKey'
  const val = 'theVal'
  await appendToHash({ hash, key, val })
  const args = hset.getCall(0).args
  const compareTo = ['theHash', 'theKey', '["theVal"]']
  t.deepEqual(args, compareTo)
})

test('#redisUtils/appendToHash hash error', async (t) => {
  const hset = sinon.spy()
  const hget = sinon.stub()
  const getServices = sinon.stub()
  const redis = {
    hget,
    hset
  }
  getServices.returns({ redis })
  const dependencies = {
    '../serviceStarter': { getServices }
  }
  hget.withArgs('theHash', 'theKey')
    .returns(JSON.stringify({ key: 'a' }))
  const appendToHash = proxyquire(
    '../../../src/redisUtils/appendToHash',
    dependencies)
  const hash = 'theHash'
  const key = 'theKey'
  const val = 'theVal'
  const error = await t.throws(appendToHash({ hash, key, val }))
  t.is(error.message, 'key theKey in hash theHash is not an Array')
})