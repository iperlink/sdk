const test = require('ava')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

test('#materialize/updateArchive', async (t) => {
  const archive = sinon.spy()
  const update = sinon.spy()
  const dependencies = {
    '../../mysqlUtils/update/archive': archive,
    '../../mysqlUtils/update': update,
  }
  const updateArchive = proxyquire(
    '../../../../src/materialize/update/updateArchive',
    dependencies
  )
  const db = 'theDb'
  const id = 'theId'
  const table = 'theTable'
  const values = 'theValues'
  await updateArchive({ db, id, table, values })
  const archiveArgs = [{ db, id, table }]
  const updateArgs = [{ db, id, table, values }]
  t.deepEqual(archiveArgs, archive.getCall(0).args)
  t.deepEqual(updateArgs, update.getCall(0).args)
})