const test = require('ava')
const assembleWhereUpdate = require('../../../../src/materialize/update/assembleWhereUpdate')

test('#materialize/assembleWhereUpdate', async (t) => {
  const db = 'theDb'
  const query = {
    tables: {
      table1: { ref: 'theRef', name: 'theTable' },
    }
  }
  const table = 'table1'
  const where = assembleWhereUpdate({ db, query, table })
  const compareTo = ` WHERE theDb.theRef = ?`
  t.is(compareTo, where)
})