const test = require('ava')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

test('#materialize/onUpdate', async (t) => {
  const insertAggregate = sinon.spy()
  const getServices = sinon.stub()
  const hgetall = sinon.stub()
  const assembleWhereUpdate = sinon.stub()
  const redis = {
    hgetall
  }
  getServices.returns({ redis })
  const dependencies = {
    '../helpers/insertAggregate': insertAggregate,
    '../../serviceStarter': { getServices },
    './assembleWhereUpdate': assembleWhereUpdate
  }
  const onUpdate = proxyquire(
    '../../../../src/materialize/update/onUpdate',
    dependencies
  )
  const view = ['view1', 'view2']

  const views = {
    view1: {
      query: ({ where }) => `theQuery ${where}`
    },
    view2: {
      query: ({ where }) => `theQuery2 ${where}`
    }

  }
  assembleWhereUpdate.withArgs({
    db: 'theDb',
    query: views.view1,
    table: 'theTable'
  }).returns('where update1')

  assembleWhereUpdate.withArgs({
    db: 'theDb',
    query: views.view2,
    table: 'theTable'
  }).returns('where update2')

  hgetall.withArgs('hash_theDb_table_ref')
    .returns({ theTable: JSON.stringify(view, null, 2) })

  await onUpdate({
    db: 'theDb',
    table: 'theTable',
    id: 'theId',
    views
  })
  const args = [{
    db: 'theDb',
    table: 'view1',
    query: 'theQuery where update1',
    id: 'theId'
  }]
  const args2 = [{
    db: 'theDb',
    table: 'view2',
    query: 'theQuery2 where update2',
    id: 'theId'
  }]
  t.deepEqual(args, insertAggregate.getCall(0).args)
  t.deepEqual(args2, insertAggregate.getCall(1).args)
})