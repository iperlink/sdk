const test = require('ava')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

test('#materialize/onInsert', async (t) => {
  const insertAggregate = sinon.spy()
  const getServices = sinon.stub()
  const hgetall = sinon.stub()
  const redis = {
    hgetall
  }
  getServices.returns({ redis })
  const dependencies = {
    '../helpers/insertAggregate': insertAggregate,
    '../../serviceStarter': { getServices }
  }
  const onInsert = proxyquire(
    '../../../../src/materialize/insert/onInsert',
    dependencies
  )
  const view = ['view1', 'view2']

  const views = {
    view1: {
      query: ({ where }) => `theQuery ${where}`
    },
    view2: {
      query: ({ where }) => `theQuery2 ${where}`
    }

  }
  hgetall.withArgs('hash_theDb_table_ref')
    .returns({ theTable: JSON.stringify(view, null, 2) })

  await onInsert({
    db: 'theDb',
    table: 'theTable',
    id: 'theId',
    views
  })
  const args = [{
    db: 'theDb',
    table: 'view1',
    query: 'theQuery  WHERE child.id = ? ',
    id: 'theId'
  }]
  const args2 = [{
    db: 'theDb',
    table: 'view2',
    query: 'theQuery2  WHERE child.id = ? ',
    id: 'theId'
  }]
  t.deepEqual(args, insertAggregate.getCall(0).args)
  t.deepEqual(args2, insertAggregate.getCall(1).args)
})