const test = require('ava')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

test('#materialize/setTableRef', async (t) => {
  const objectToHash = sinon.spy()

  const dependencies = {
    '../../redisUtils/objectTohash': objectToHash
  }
  const setTableRef = proxyquire(
    '../../../../src/materialize/helpers/setTableRef',
    dependencies
  )
  const parentView = {
    tables: {
      child: { name: 'child' },
      parent: { name: 'parent', ref: ' child.parent_id ' }
    },
  }

  const childView = {
    name: 'childView',
    tables: {
      child: { name: 'child', ref: 'child.id' },
    }
  }
  const views = {
    childView,
    parentView
  }
  const res = await setTableRef({
    db: 'theDb',
    views,
  })
  const compareTo = {
    child: ['childView', 'parentView'],
    parent: ['parentView']
  }
  const args = [{ hash: 'hash_theDb_table_ref', obj: compareTo }]
  t.deepEqual(args, objectToHash.getCall(0).args)
  t.deepEqual(res, compareTo)
})


