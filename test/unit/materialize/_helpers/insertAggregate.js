const test = require('ava')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
require('../../../../src/materialize/helpers/insertAggregate')

test('#materialize/insertAggregate', async (t) => {
  const createAggregate = sinon.stub()
  const replace = sinon.spy()

  const dependencies = {
    './createAggregate': createAggregate,
    '../../mysqlUtils/replace': replace
  }
  createAggregate.withArgs({
    id: 'theId',
    query: 'theQuery'
  }).returns(['val'])
  const args = [{
    db: 'theDb',
    table: 'theTable',
    values: 'val'
  }]
  const insertAggregate = proxyquire(
    '../../../../src/materialize/helpers/insertAggregate',
    dependencies
  )
  await insertAggregate({
    db: 'theDb',
    id: 'theId',
    query: 'theQuery',
    table: 'theTable',
  })
  t.deepEqual(replace.getCall(0).args, args)
})


