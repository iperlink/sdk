const test = require('ava')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
require('../../../../src/materialize/helpers/createAggregate')

test('#materialize/createAggregate', async (t) => {
  const getServices = sinon.stub()
  const query = sinon.stub()
  const mysql = { query }
  getServices.returns({
    mysql
  })
  query.withArgs('theQuery', ['theId']).returns(['found'])
  const dependencies = {
    '../../serviceStarter': { getServices }
  }
  const createAggregate = proxyquire(
    '../../../../src/materialize/helpers/createAggregate',
    dependencies
  )
  const agg = await createAggregate({ query: 'theQuery', id: 'theId' })
  t.is(agg, 'found')
})


