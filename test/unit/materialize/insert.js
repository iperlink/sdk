const test = require('ava')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

test('#materialize/insertMaterialized', async (t) => {
  const insert = sinon.stub()
  const onInsert = sinon.spy()

  const dependencies = {
    '../mysqlUtils/insert': insert,
    './insert/onInsert': onInsert
  }
  const insertMaterialized = proxyquire(
    '../../../src/materialize/insert',
    dependencies
  )
  insert.withArgs({
    db: 'theDb',
    table: 'theTable',
    values: 'theValues'
  }).returns({ id: 1 })

  const res = await insertMaterialized({
    db: 'theDb',
    table: 'theTable',
    values: 'theValues',
    views: 'theViews'
  })
  const args = [{
    db: 'theDb',
    table: 'theTable',
    id: 1,
    views: 'theViews'
  }]
  t.deepEqual(args, onInsert.getCall(0).args)
})
