const test = require('ava')
const filterByInitial = require('../../../src/filters/filterByInitial')

test("#filterByInitial", t => {
  const options = ['abc', 'bcd', 'cde']
  const value = 'a'
  const filtered = filterByInitial({ options, value })
  t.deepEqual(filtered, ['abc'])
})