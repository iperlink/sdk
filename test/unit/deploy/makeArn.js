const test = require('ava')
const makeArn = require('../../../src/deploy/makeArn')

test("#makeArn", t => {
  const arn = makeArn({
    name: 'myFunction',
    stage: 'beta',
    type: 'lambda',
    service: 'appsync',
    AWS_REGION: 'us-east-1',
    AWS_ACCOUNT: '324168765416'
  })
  t.is(arn, 'arn:aws:lambda:us-east-1:324168765416:function:appsync-beta-myFunction')
})