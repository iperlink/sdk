const test = require('ava')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

test("#makeMappingTemplate", async t => {
  const fs = { writeFile: sinon.stub() }
  writeFile.withArgs({
    id: 'theId',
    query: 'theQuery'
  }).returns(['val'])
  const dependencies = {
    'fs': fs
  }
  const makeMappingTemplate = proxyquire('../../../src/deploy/makeMappingTemplate', dependencies)
  await makeMappingTemplate({
    insertPrivilege: 1,
    operation: 'createApp'
  })
  console.log()
  t.pass()
})