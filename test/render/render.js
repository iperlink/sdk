const { render } = require('react-dom')
const Component = require('./components.js')
const serviceStarter = require('../../src/htmlHelpers/serviceStarter')
serviceStarter(config)
render(
  Component({ name, dataSourceTemplate: config, propObj }),
  document.getElementById('app')
)
