const { createElement } = require('react')
const { render } = require('react-dom')
const fullApp = require('../../../src/reactComponents/fullApp')
const getConfig = require('../../../src/localStorage/getConfig')
const endpoint = STATIC_URL
const serviceStarter = require('../../../src/htmlHelpers/serviceStarter')

getConfig({ key: 'form.json ', endpoint, type: 'local' })
  .then(json => {
    const dataSourceTemplate = JSON.parse(json)
    serviceStarter(dataSourceTemplate)
    render(
      createElement(
        fullApp,
        { dataSourceTemplate },
        null),
      document.getElementById('app')
    )
  })
