const { createElement } = require('react')
const Spinner = require('react-spinkit')

module.exports = () => {
  const spin = createElement(
    Spinner,
    {
      name: 'double-bounce',
      style: {
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '60px',
        height: '60px',
        marginTop: '100px',
      }
    },
    null)
  const Loading = createElement(
    'div',
    {
      className: 'spinerCont'
    },
    spin)
  return Loading
}