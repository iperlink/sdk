const { createElement } = require('react')
const loader = require('./loader')
const { graphql } = require('react-apollo')
const tag = require('graphql-tag')

const query = tag`query english {
  english{
    name
  }
}`

const app = (props) => {
  const notYet = loader(props.data)
  if (notYet) {
    return notYet
  }
  if (!props.data.loading) {
    console.log(props)

    const nodes = props.data.english.map(prop => {
      return createElement('li', null, prop.name)
    }
    )
    const ul = createElement('ul', null, nodes)
    return ul
  }

}

module.exports = () => {
  const result = graphql(query, {
    options: {
      fetchPolicy: 'cache-and-network'
    },
    props: props => {
      return props
    }
  })(app)
  const div = createElement(
    result,
    {},
    null
  )
  return div
}
