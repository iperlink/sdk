const { createElement } = require('react')

module.exports = () => {
  const error = createElement(
    'div',
    {
    },
    'ERROR!')
  return error
}