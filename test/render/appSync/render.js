const { createElement } = require('react')
const { render } = require('react-dom')

const WithProvider = require('../../../src/appSync/withProvider')
const App = require('./app')()
const config = require('./AppSync.config')
render(
  createElement(
    WithProvider,
    { App, config },
    null),
  document.getElementById('app')
)
