const loadingComp = require('./loading')
const errorComp = require('./error')

module.exports = (props) => {
  const {
    loading,
    error
  } = props
  if (loading) {
    return loadingComp()
  }
  if (error) {
    return errorComp()
  }
  return false
}
