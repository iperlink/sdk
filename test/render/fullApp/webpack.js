const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: {
    app: './test/render/fullApp/render'
  },
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './test/render/public',
    historyApiFallback: true,
    noInfo: false,
    inline: true,
    port: 9000,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000, // How often check for changes (in milliseconds)
    },
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader?cacheDirectory=true']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },

  output: {
    filename: 'app.bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  },
}
