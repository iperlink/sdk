import React from 'react'
import ReactDom from 'react-dom'
import AppContainer from '../../../src/reactComponents/appContainer'


const content = {
  section1: ['sub1_1', 'sub1_2'],
  section2: ['sub2_1', 'sub2_2'],
}
ReactDom.render(
  < AppContainer
    content={content}
    mainId={'inside'}
  />,
  document.getElementById('app')
)
