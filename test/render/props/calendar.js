const validate = require('../../../src/reactComponents/formCreator/template.validate')

const f = {
  calendar: {
    invalidMsg: 'tis invalid',
    type: 'calendar',
    value: 0,
    finish: false
  },
  calendar1: {
    invalidMsg: 'tis invalid',
    type: 'calendar',
    value: 0,
    finish: true
  }
}

const fields = validate(f)
const onSubmit = (data) => {
  console.log(data)
}

module.exports = {
  className: 'formCentered',
  fields,
  id: 'form1',
  onSubmit,
}
