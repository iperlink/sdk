const getScreenRange = require('../../../src/htmlHelpers/getScreenRange')
const range = getScreenRange()

module.exports = {
  id: 'gallery',
  iterator: 1,
  list: [{
    subTitles: ['testSub'],
    title: 'testTitle',
    src: 'fmdyoeehhr'
  },
  {
    subTitles: ['testSub2'],
    title: 'testTitle2',
    src: 'fmdyoeehhr'
  }
],
  title: 'testTitle',
  className: 'galleryCard',
  range,
  
}
