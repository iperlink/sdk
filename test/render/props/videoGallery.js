const getScreenRange = require('../../../src/htmlHelpers/getScreenRange')
const range = getScreenRange()

module.exports = {
  id: 'gallery',
  iterator: 1,
  array: [{
    subTitles: ['testSub'],
    title: 'testTitle',
    src: 'fmdyoeehhr'
  },
  {
    subTitles: ['testSub2'],
    title: 'testTitle2',
    src: 'fmdyoeehhr'
  },
  {
    subTitles: ['testSub'],
    title: 'testTitle',
    src: 'fmdyoeehhr'
  },
  {
    subTitles: ['testSub2'],
    title: 'testTitle2',
    src: 'fmdyoeehhr'
  }],
  title: 'testTitle',
  className: 'galleryCard',
  range,
}