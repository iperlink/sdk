module.exports = {
  content: {
    section1: [{ title: 'sub1_1', path: '' }, { title: 'sub1_2', path: '' }],
    section2: [{ title: 'sub2_1', path: '' }, { title: 'sub2_2', path: '' }],
  },
  mainId: 'inside',
  inside: (props) => render({ type: 'div', props: {}, children: 'inside' }),
  endpoint: 'https://lcv6i8jai0.execute-api.us-east-1.amazonaws.com/dev/',
}
