const validate = require('../../../src/reactComponents/formCreator/template.validate')

const doc = require('./utils/doc')
const f = {
  tree: {
    id: `tree`,
    value: 1,
    options: doc,
    type: 'tree',
  },
  tree2: {
    id: `tree2`,
    value: 2,
    options: doc,
    type: 'tree',
  }
}


const fields = validate(f)
const onSubmit = (data) => {
  console.log(data)
}
module.exports = {
  className: 'formCentered',
  fields,
  id: 'form1',
  onSubmit,
}
