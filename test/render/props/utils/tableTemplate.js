const converter = require('number-to-words')

export const tableDefs = {
  first: {
    keys: {
      name: {
        title: "nombre",
        header: true
      },
      secondKey: {
        title: "dos",
      },
      description: {
        title: "descripcion",
        hideMobile: true
      },
      aprove: {
        title: "aprobar",
        specialRenderer: 'switch',
      }
    },
    styleObj: {
      screeLarge: {
        row4: "15% 10% 60% 15%",
        row5: "15% 10% 30% 30% 15%",
        sub: {}
      },
      screeReg: {
        row: "25% 25% 25% 25%",
        header: "25% 25% 25% 25%",
        sub: {}
      },
      celSmall: {
        row: "50% 50%",
        header: {
        },
        sub: {
        },
        compact: true
      }
    }
  }
}

export const makeCells = iterator => {
  const cells = {
    name: `test${iterator}`,
    secondKey: `second_${iterator}`,
    description: `this is a very log text that should not fit in a normal table cell, nevertheless we are going to try to jam it in there `,
    id: iterator,
    date: new Date().getTime(),
    tags: `tag${iterator}`
  }
  return cells
}

export const data = []
for (let i = 0; i < 2; i++) {
  data.push(makeCells(i))
}
