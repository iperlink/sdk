const converter = require('number-to-words')
const options = []
for (let i = 0; i < 8000; i++) {
  options.push(converter.toWords(i))
}
options.sort()

const selectOptions = []

for (let i = 0; i < 8; i++) {
  selectOptions.push({ label: converter.toWords(i), value: i })
}

module.exports = selectOptions