const { data, tableDefs } = require('./utils/tableTemplate')
const getScreenRange = require('../../../src/htmlHelpers/getScreenRange')
const inRange = getScreenRange()
const styleObjRange = tableDefs.first.styleObj[inRange]

module.exports = {
  data,
  id: 'tableBody',
  keys: tableDefs.first.keys,
  styleObjRange,
  inRange,
  className: 'simpleGrid',
}
