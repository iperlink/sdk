const { tableDefs, makeCells } = require('./utils/tableTemplate')
const getScreenRange = require('../../../src/htmlHelpers/getScreenRange')
const inRange = getScreenRange()
const keyBy = require('../../../src/objMapper/keyBy')
const button = require('../../../src/reactComponents/btn')


const createData = () => {
  const data = []
  // const intV = 10
  // const first = (intV * (page - 1)) + 1
  // const last = (intV * ((page - 1) + 1)) + 1
  for (let i = 0; i < 100; i++) {
    data.push(makeCells(i))
  }
  // const filtered = filterBy.tags
  //   ? data.filter(d => d.tags.split(',').includes(filterBy.tags))
  //   : data
  return data
}


module.exports = {
  template: tableDefs.first,
  id: 'tableHeader',
  data: createData(),
  inRange,
  paginate: true,
  className: 'simpleGrid'
}
