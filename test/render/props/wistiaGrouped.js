const getScreenRange = require('../../../src/htmlHelpers/getScreenRange')
const range = getScreenRange()

module.exports = {
  id: 'gallery',
  iterator: 1,
  obj: {
    top: [{
      subTitles: ['testSub'],
      title: 'testTitle',
      src: 'fmdyoeehhr'
    },
    {
      subTitles: ['testSub2'],
      title: 'testTitle2',
      src: 'fmdyoeehhr'
    }],
    bottom: [{
      subTitles: ['testSub'],
      title: 'testTitle',
      src: 'fmdyoeehhr'
    },
    {
      subTitles: ['testSub2'],
      title: 'testTitle2',
      src: 'fmdyoeehhr'
    }]
  },
  title: 'testTitle',
  className: 'galleryCard',
  range,
}
