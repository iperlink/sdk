const getScreenRange = require('../../../src/htmlHelpers/getScreenRange')
const range = getScreenRange()

module.exports = {
  id: 'wistia',
  iterator: 1,
  subTitles: ['testSub'],
  title: 'testTitle',
  className: 'wistia',
  range,
  src: 'fmdyoeehhr'
}