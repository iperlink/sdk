const validate = require('../../../src/reactComponents/formCreator/template.validate')
const options = require('./utils/numbers')

const f = {
  select1: {
    type: 'select',
    options,
    data: [
      { id: 1, name: 'uno' },
      { id: 2, name: 'dos' },
      { id: 3, name: 'tres' },
      { id: 4, name: 'cuatro' },
      { id: 5, name: 'cinco' },
    ],
    onChange: 'thisIsCustom',
  },
  select2: {
    type: 'select',
    options,
    data: [
      { id: 1, parent: 1 },
      { id: 2, parent: 2 },
      { id: 3, parent: 3 },
      { id: 4, parent: 4 },
      { id: 5, parent: 5 },
    ],
  }
}

const fields = validate(f)
const onSubmit = (data) => {
  console.log(data)
}
module.exports = {
  className: 'formCentered_horizontal',
  fields,
  id: 'form1',
  onSubmit,
}
