const fields = {
  text2: {
    id: `text2`,
    invalidMsg: 'tis invalid',
    name: `text`,
    type: 'text',
    value: 0,
  },
  select1: {
    type: 'kmlPoint',
    initialCords: {
      cords: { lat: -33, lng: -70 },
      zoom: 5,
    }
  },
  text: {
    id: `text`,
    invalidMsg: 'tis invalid',
    name: `text`,
    type: 'text',
    value: 0,
  },

}

const onSubmit = (data) => {
  alert(JSON.stringify(data, null, 2))
}
module.exports = {
  className: 'formCentered',
  fields,
  id: 'form1',
  onSubmit,
}
