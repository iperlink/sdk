const { data, tableDefs } = require('./utils/tableTemplate')
const getScreenRange = require('../../../src/htmlHelpers/getScreenRange')
const inRange = getScreenRange()
const styleObjRange = tableDefs.first.styleObj[inRange]

module.exports = {
  headerData: data[0],
  id: 'tableHeader',
  keys: tableDefs.first.keys,
  styleObjRange,
  tableClass: 'simpleGrid',
}
