const converter = require('number-to-words')
const validate = require('../../../src/reactComponents/formCreator/template.validate')

const options = []
for (let i = 0; i < 8000; i++) {
  options.push(converter.toWords(i))
}
options.sort()

const f = {
  typeAhead1: {
    id: `typeAhead1`,
    name: `typeAhead1`,
    type: 'typeAhead',
    options,
    value: '',
    onClick: (val) => {
      console.log('val', val)
    }
  }
}

const fields = validate(f)
const onSubmit = (data) => {
  console.log(data)
}
module.exports = {
  className: 'formCentered',
  fields,
  recursive: true,
  hideSent: true,
  onSubmit: (test) => { submit(test) },
  id: 'form1',
  onSubmit,
}
