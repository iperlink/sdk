const validate = require('../../../src/reactComponents/formCreator/template.validate')
const options = require('./utils/numbers')

const f = {
  checkbox1: {
    type: 'checkbox',
    options,
  }
}


const fields = validate(f)
const onSubmit = (data) => {
  console.log(data)
}
module.exports = {
  className: 'formCentered',
  fields,
  id: 'form1',
  onSubmit,
}
