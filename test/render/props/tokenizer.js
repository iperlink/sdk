const converter = require('number-to-words')
const validate = require('../../../src/reactComponents/formCreator/template.validate')

const options = []
for (let i = 0; i < 8000; i++) {
  options.push(converter.toWords(i))
}
options.sort()
const f = {
  tokenizer: {
    name: `tokenizer`,
    type: 'tokenizer',
    options,
    tokens: [],
    updater: ['tokens', 'value'],
    value: '',
  },
  text: {
    name: `nombre`,
    type: 'text',
    value: '',
  },
}

const fields = validate(f)
const onSubmit = (data) => {
  console.log(data)
}
module.exports = {
  className: 'formCentered',
  fields,
  id: 'form1',
  onSubmit,
}
