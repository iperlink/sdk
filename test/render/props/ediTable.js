const validate = require('../../../src/reactComponents/formCreator/template.validate')
const options = require('./utils/numbers')
const { tableDefs, makeCells } = require('./utils/tableTemplate')
const getScreenRange = require('../../../src/htmlHelpers/getScreenRange')
const inRange = getScreenRange()


const createData = (page) => {
  const data = []
  const intV = 10
  const first = (intV * (page - 1)) + 1
  const last = (intV * ((page - 1) + 1)) + 1
  for (let i = first; i < last; i++) {
    data.push(makeCells(i))
  }
  return data
}

const f = {
  name: {
    type: 'select',
    options,
  },
  secondKey: {
    type: 'text',
    value: 0,
  },
  description: {
    invalidMsg: 'tis invalid',
    type: 'text',
    value: 0,
  }
}

const fields = validate(f)
const onSubmit = (data) => {
  console.log(data)
}

module.exports = {
  form: {
    className: 'formCentered',
    fields,
    id: 'form1',
    onSubmit,
  },
  table: {
    template: tableDefs.first,
    id: 'tableHeader',
    getDataFrom: createData,
    inRange,
    paginate: true,
    className: 'simpleGrid',
    editPrivilege: 1
  }

}
