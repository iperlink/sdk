const validate = require('../../../src/reactComponents/formCreator/template.validate')
const options = require('./utils/numbers')
const { tableDefs, makeCells } = require('./utils/tableTemplate')
const getScreenRange = require('../../../src/htmlHelpers/getScreenRange')
const inRange = getScreenRange()


const createData = () => {
  const data = []
  // const intV = 10
  // const first = (intV * (page - 1)) + 1
  // const last = (intV * ((page - 1) + 1)) + 1
  for (let i = 0; i < 100; i++) {
    data.push(makeCells(i))
  }
  // const filtered = filterBy.tags
  //   ? data.filter(d => d.tags.split(',').includes(filterBy.tags))
  //   : data
  return data
}

const f = {
  name: {
    type: 'select',
    options,
  },
  secondKey: {
    type: 'text',
    value: 0,
  },
  description: {
    invalidMsg: 'tis invalid',
    type: 'text',
    value: 0,
  },

}

const fields = validate(f)
const onSubmit = (data) => {
  console.log(data)
}
module.exports = {
  id: 'testTable',
  form: {
    className: 'formCentered',
    fields,
    id: 'form1',
    onSubmit,
  },
  table: {
    template: tableDefs.first,
    id: 'tableHeader',
    data: createData(),
    inRange,
    paginate: true,
    className: 'simpleGrid',
    editPrivilege: 1
  }

}
