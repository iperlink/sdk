const validate = require('../../../src/reactComponents/formCreator/template.validate')

const f = {
  text: {
    invalidMsg: 'tis invalid',
    type: 'text',
    value: 0,
  },
  text2: {
    invalidMsg: 'tis invalid',
    type: 'text',
    value: 1,
  },
}

const fields = validate(f)
const onSubmit = (data) => {
  console.log(data)
}

module.exports = {
  className: 'formCentered_horizontal',
  fields,
  id: 'form1',
  onSubmit,
}
