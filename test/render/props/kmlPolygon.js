const options = require('./utils/numbers')

const fields = {
  text2: {
    name: `text`,
    type: 'select',
    options,
    data: [
      { id: 1, kmlData: { center: { lat: -33.4, lng: -70.6 }, zoom: 15 } },
      { id: 2, kmlData: { center: { lat: -36.8, lng: 174.5 }, zoom: 5 } },
      { id: 3, kmlData: { center: { lat: 37.8, lng: -122 }, zoom: 5 } },
    ],
    onChange: 'thisIsCustom',
  },
  area: {
    type: 'kmlPolygon',
    name: 'area',
    updater: ['initialCords', 'value'],
    initialCords: { center: { lat: -33.4, lng: -70.6 }, zoom: 5 },
  },
  text: {
    id: `text`,
    invalidMsg: 'tis invalid',
    name: `text`,
    type: 'text',
    value: 0,
  },

}

const onSubmit = (data) => {
  alert(JSON.stringify(data.area, null, 2))
}
module.exports = {
  className: 'formCentered',
  fields,
  id: 'form1',
  onSubmit,
}
