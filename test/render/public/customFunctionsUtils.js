
const keyBy = ({ array, key }) => {
  if (!array) throw new Error('array undefined at keyBy')
  const result = array.reduce((acc, current) => {
    acc[current[key]] = current
    return acc
  }, {})
  return result
}

customFunctionUtils = {
  getDataFromSelect: (changeProps) => {
    const { field, target } = changeProps
    const { data } = field
    const index = target.selectedIndex
    const selectedOption = target.options[index]
    const currentData = data[index]
    const currentId = currentData.id
    return {
      currentData,
      currentId,
      index,
      selectedOption,
    }
  },
  getDataFromIndex: ({ fields, targetName, sourceKey, currentId }) => {
    const secondField = fields[targetName]
    const keyedData = keyBy({ array: secondField.data, key: sourceKey })
    const reselected = keyedData[currentId]
    const reselectedId = reselected.id
    return {
      reselected,
      reselectedId
    }
  }
}