localStorageFunctions = {
  userOrgTree: () => {
    const user = localStorage.getItem('userData')
    const parsed = JSON.parse(user)
    const { nodes } = parsed
    const result = JSON.parse(nodes)
    return result
  },
  userTreeValue: (type) => {
    const user = localStorage.getItem('userData')
    const parsed = JSON.parse(user)
    const { privilegeObj } = parsed
    const firstKey = Object.keys(privilegeObj)[0]
    const index = type === 'geo'
      ? 3
      : 2
    const result = privilegeObj[firstKey][index]
    return index
  }
}