allowedFileExtensions = ['jpg', 'pdf']
map = {}
polygon = {}
USER_NODES = []
KEYED_USER_NODES = {}
customFunctions = {
  thisIsCustom: ({ changeProps, fields, originalProps }) => {
    const {
      currentData,
    } = customFunctionUtils.getDataFromSelect(changeProps)
    const area2 = Object.assign(
      {},
      fields.ubicacion,
      {
        initialCords: currentData.geodiv1KmlData,
      }
    )
    const newFields = Object.assign(
      {},
      fields,
      {
        ubicacion: area2,
      }
    )
    return newFields
  }
}