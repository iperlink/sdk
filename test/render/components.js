const render = require('../../src/renderer')
const proplist = require('./props')

const toggleClass = require('../../src/htmlHelpers/toggleClass')

const getType = (name) => {
  return require(`../../src/reactComponents/${name}`)
}

module.exports = ({ name, dataSourceTemplate, propObj }) => {
  const allComponents = {
    ...proplist,
    fullApp: { dataSourceTemplate },
  }
  const propsName = propObj || name
  const props = allComponents[propsName]
  if (!props) {
    throw new Error(`element ${propsName} is not configured in props`)
  }
  const type = getType(name)
  const withId = props.id
    ? props
    : {
      ...props,
      id: name,
    }
  const rendered = render({
    type,
    props: withId
  })

  return rendered;
}
