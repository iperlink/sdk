const path = require('path')
const webpack = require('webpack')
const config = require('./public/config.json')

module.exports = {
  entry: {
    app: './test/render/render.js'
  },
  plugins: [
    new webpack.DefinePlugin({
      'name': JSON.stringify(process.argv[7]),
      'propObj': JSON.stringify(process.argv[9]),
      'dev': 'true',
      'config': JSON.stringify(config),
    })
  ],

  // target: 'node', // in order to ignore built-in modules like path, fs, etc.
  target: 'web',
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './test/render/public',
    historyApiFallback: true,
    noInfo: false,
    inline: true,
    port: 9001,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000, // How often check for changes (in milliseconds)
    },
  },


  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader?cacheDirectory=true']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  output: {
    filename: 'app.bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  },
}
