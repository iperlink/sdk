require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  find,
  multiInsert,
  replace,
} = require('../../../../src/mysqlUtils')

test('#mysql/replace', async (t) => {
  await start()
  const template = {
    queryParams: {
      id: 'exact',
    },
    table: 'childView'
  }
  const queryObj = {
    id: [2]
  }
  
  const db = 'tester'
  const table = 'childView'
  await truncate({
    db,
    tables: [table]
  })
  const values = [
    {
      name: 'test',
    },
    {
      name: 'test2',
    },
    {
      name: 'test3',
    }
  ]
  const values4 = {
    name: 'replaced',
    id: 2
  }
  await multiInsert({ db, table, values })
  await replace({ db, table, values: values4 })
  const fields = ['name', 'created_at']
  const found = await find({ db: 'tester_materialized', template, fields, queryObj })
  t.is(found[0].name, 'replaced')
})