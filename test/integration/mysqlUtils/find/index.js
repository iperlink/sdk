require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  find,
  insert,
} = require('../../../../src/mysqlUtils')

test('#mysqlFind/', async (t) => {
  await start()
  const template = {
    queryParams: {
      id: 'exact',
      name: 'exact',
      createdAt: 'dateBetween'
    },
    table: 'child'
  }
  const queryObj = {
    id: [1],
    createdAt: { begin: '2018-01-01', end: '2018-12-31' }
  }
  const values = {
    name: 'test',
    parent_id: 1
  }
  const values2 = {
    name: 'test2',
    parent_id: 2
  }
  const values3 = {
    name: 'test3',
    parent_id: 3
  }
  const db = 'tester'
  const table = 'child'
  await truncate({
    db,
    tables: [table]
  })
  await insert({ db, table, values })
  await insert({ db, table, values: values2 })
  await insert({ db, table, values: values3 })

  const fields = ['name', 'created_at']
  const filtered = [1, 7]
  const found = await find({ db, template, fields, queryObj, filtered })
  t.is(found[0].name, 'test')
})

test.only('#mysqlFind minimal/', async (t) => {
  await start({})
  const values = {
    name: 'test',
    parent_id: 1
  }
  const values2 = {
    name: 'test2',
    parent_id: 2
  }
  const values3 = {
    name: 'test3',
    parent_id: 3
  }
  const db = 'tester'
  const table = 'child'
  await truncate({
    db,
    tables: [table]
  })
  await insert({ db, table, values })
  await insert({ db, table, values: values2 })
  await insert({ db, table, values: values3 })
  const found = await find({ db, table })
  console.log(found)
  t.is(found[0].name, 'test')
})
