require('dotenv').config()
const test = require('ava')
const {
  assembleWhere,
} = require('../../../../../../src/mysqlUtils')
const start = require('../../../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../../../src/serviceStarter')

test.skip('#mysql assemble where/', async (t) => {
  await start(['mysql'])
  const { mysql } = getServices()
  const queryParams = {
    id: 'exact',
    name: 'exact',
    date: 'dateBetween'
  }
  const queryObj = {
    id: [7, 29],
    date: { begin: '2018-01-01', end: '2018-01-05' }
  }
  const { assembledString, values } = assembleWhere({ queryObj, queryParams })
  const query = ` find * from tester where ${assembledString}`
  const formated = mysql.format(query, values)
  t.is(
    formated,
    ' find * from tester where  id in (7, 29) and  date between 1514775600 and 1515121200 '
  )
})
