require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const {
  truncate
} = require('../../../../src/mysqlUtils')

test('#truncate sucess/', async (t) => {
  await start(['mysql'])
  const truncated = await truncate({ tables: ['child'], db: 'tester' })
  t.is(truncated, 0)
})

test('#truncate error/', async (t) => {
  await start(['mysql'])
  const truncated = await truncate({ tables: ['childx'], db: 'tester' })
  t.is(truncated.code, 'tableNotExist')
})
