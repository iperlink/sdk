require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  insert,
  archive,
} = require('../../../../src/mysqlUtils')

test('#update sucess minimal/', async (t) => {
  await start()
  const db = 'tester'
  const table = 'child'
  const values = {
    name: 'test',
    parent_id: 1
  }
  await truncate({
    db,
    tables: [table]
  })
  const updater = {
    name: 'updated'
  }
  await insert({ db, table, values })
  const archived = await archive({ db, table, id: 1, comment: 'test' })
  const parsed = JSON.parse(archived.content)
  const name = parsed.name
  t.is(name, 'test')
})
