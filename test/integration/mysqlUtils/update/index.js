require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  insert,
  update,
} = require('../../../../src/mysqlUtils')

test('#update sucess minimal/', async (t) => {
  await start()
  const db = 'tester'
  const table = 'child'
  const values = {
    name: 'test',
    parent_id: 1
  }
  await truncate({
    db,
    tables: [table]
  })
  const updater = {
    name: 'updated'
  }
  await insert({ db, table, values })
  const updated = await update({ db, table, values: updater, id: 1 })
  t.is(updated, 1)
})

test('#update forbiden/', async (t) => {
  await start()
  const db = 'tester'
  const table = 'child'
  const values = {
    name: 'test',
    parent_id: 1
  }
  await truncate({
    db,
    tables: [table]
  })
  const updater = {
    name: 'updated'
  }
  const forbiden = ['name']
  await insert({ db, table, values })
  const updated = await update({ db, table, values: updater, id: 1, forbiden })
  t.is(updated.code, 'cantUpdateParam')
})

test('#update no id/', async (t) => {
  await start()
  const db = 'tester'
  const table = 'child'
  const values = {
    name: 'test',
    parent_id: 1
  }
  await truncate({
    db,
    tables: [table]
  })
  const updater = {
    name: 'updated'
  }
  await insert({ db, table, values })
  const updated = await update({ db, table, values: updater, id: 3 })
  t.is(updated.code, 'noUpdateID')
})