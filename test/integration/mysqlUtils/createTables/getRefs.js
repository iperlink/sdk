require('dotenv').config()
const test = require('ava')
const createRefs = require('../../../../src/mysqlUtils/createTables/createRefs')

test('#createRefs error missing table/', (t) => {
  const tables = {
    left: {
      fields: {
        id: 'idS',
        name: 'char50'
      }
    },
    left_rightd: 'ref',
    right: {
      fields: {
        id: 'idS',
        name: 'char50'
      }
    }
  }
  const error = t.throws(() => createRefs({ tables, db: 'tester' }))
  t.is(error.message, ' left_rightd.rightd references a missing table ')
})

test('#createRefs ok/', (t) => {
  const tables = {
    left: {
      fields:
      {
        id: 'idS',
        name: 'char50'
      }
    },
    left_right: 'ref',
    right: {
      fields:
      {
        id: 'idS',
        name: 'char50'
      }

    }
  }
  const withRefs = createRefs({ tables, db: 'test' })
  const compareTo = { id: 'int', left_id: 'S', right_id: 'S' }
  t.deepEqual(withRefs.left_right.fields, compareTo)
})

test('#createRefs ok2/', (t) => {
  const tables = {
    left: {
      fields:
      {
        id: 'idS',
        name: 'char50'
      }
    },
    left_right_middle: {
      fields: {
        id: 'idS'
      },
      refs: ['left', 'right']
    },
    right: {
      fields:
      {
        id: 'idS',
        name: 'char50'
      }
    },
    middle: {
      fields:
      {
        id: 'idS',
        name: 'char50'
      }
    }
  }
  const withRefs = createRefs({ tables, db: 'test' })
  const compareTo = {
    fields: { id: 'int', left_id: 'S', right_id: 'S' },
    fks:
      ['ADD FOREIGN KEY (left_id) REFERENCES test.left(id)',
        'ADD FOREIGN KEY (right_id) REFERENCES test.right(id)'],
    refs: ['left', 'right']
  }
  t.deepEqual(withRefs.left_right_middle, compareTo)
})

test.only('#createRefs just ref array/', (t) => {
  const tables = {
    left: {
      fields:
      {
        id: 'idS',
        name: 'char50'
      }
    },
    target: {
      fields: {
        id: 'idS'
      },
      refs: ['left', 'right']
    },
    right: {
      fields:
      {
        id: 'idS',
        name: 'char50'
      }
    },
    middle: {
      fields:
      {
        id: 'idS',
        name: 'char50'
      }
    }
  }
  const withRefs = createRefs({ tables, db: 'test' })
  const compareTo = {
    fields: { id: 'int', left_id: 'S', right_id: 'S' },
    fks:
      ['ADD FOREIGN KEY (left_id) REFERENCES test.left(id)',
        'ADD FOREIGN KEY (right_id) REFERENCES test.right(id)'],
    refs: ['left', 'right']
  }
  t.deepEqual(withRefs.target, compareTo)
})