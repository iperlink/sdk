require('dotenv').config()
const test = require('ava')
const createTables = require('../../../../src/mysqlUtils/createTables')
const start = require('../../../../src/serviceStarter/tester')
const tables = require('../../../../src/identicorp/structure')

test('#create Table error invalid type/', async (t) => {
  await start()
  const tables = {
    left: {
      fields:
      {
        id: 'idS',
        name: 'varchar50'
      }

    }
  }
  const error = await t.throws(createTables({ db: 'tester', tables }))
  t.is(error.message, ' varchar50 is not a valid type ')
})

test.only('#create Tables/', async (t) => {
  await start()
  const base = {
    created_at: 'Int',
    updated_at: 'Int',
    version: 'T',
    status: 'T'
  }
  await createTables({
    db: 'shared', tables, base
  })
  t.pass()
})