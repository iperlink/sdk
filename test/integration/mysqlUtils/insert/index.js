require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  insert,
} = require('../../../../src/mysqlUtils')

test('#insert sucess minimal/', async (t) => {
  await start()
  const db = 'tester'
  const table = 'child'
  const values = {
    name: 'test',
  }
  await truncate({
    db,
    tables: [table]
  })
  const inserted = await insert({ db, table, values })
  t.is(inserted.name, 'test')
  t.is(inserted.id, 1)
  t.is(inserted.version, 1)
})

test('#insert error ref/', async (t) => {
  await start()
  await truncate({
    db,
    tables: ['child', 'parent']
  })
  const db = 'tester'
  await insert({ db, table: 'parent', values: { name: 'test' } })
  const values = {
    name: 'test',
    parent_id: 2
  }
  const table = 'child'
  const refs = ['parent']
  const error = await insert({ db, table, values, refs })
  t.is(error.code, 'noRef')
})

test('#insert error required/', async (t) => {
  await start()
  await truncate({
    db,
    tables: ['child', 'parent']
  })
  const db = 'tester'
  await insert({ db, table: 'parent', values: { name: 'test' } })
  const values = {
    parent_id: 1
  }
  const requiredKeys = ['name']
  const table = 'child'
  await t.throws(insert({ db, table, values, requiredKeys }))
})