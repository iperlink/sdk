require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  multiInsert,
} = require('../../../../src/mysqlUtils')

test('#insert sucess minimal/', async (t) => {
  await start()
  const db = 'tester'
  const table = 'child'
  const values = [{
    name: 'test',
  }, {
    name: 'test2',
  }]

  await truncate({
    db,
    tables: [table]
  })
  const inserted = await multiInsert({ db, table, values })
  t.is(inserted, 2)
})
