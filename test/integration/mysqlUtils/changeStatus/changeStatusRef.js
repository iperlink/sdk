require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  insert,
  changeStatusRef,
  multiInsert,
} = require('../../../../src/mysqlUtils')


test('#changeStatusRef sucess minimal/', async (t) => {
  await start()
  const db = 'tester'
  const table = 'first'
  const values = {
    name: 'test',
    status: 1
  }
  await truncate({
    db,
    tables: [table]
  })
  await multiInsert({ db, table: 'status', values: [{ name: 'test' }, { name: 'test2' }] })
  await insert({ db, table, values })
  const updated = await changeStatusRef({
    db,
    table,
    status: 2,
    ref: { id: 1, name: 'test' },
  })
  t.is(updated, 1)
})

