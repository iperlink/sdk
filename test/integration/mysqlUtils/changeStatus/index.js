require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  insert,
  changeStatus,
} = require('../../../../src/mysqlUtils')


test('#changeStatus sucess minimal/', async (t) => {
  await start()
  const db = 'tester'
  const table = 'first'
  const values = {
    name: 'test',
  }
  await truncate({
    db,
    tables: [table]
  })
  await insert({ db, table, values })
  const updated = await changeStatus({ db, table, status: 2, id: 1, statusList: [1, 2, 3] })
  t.is(updated, 1)
})

test('#changeStatus error /', async (t) => {
  await start()
  const db = 'tester'
  const table = 'first'
  const values = {
    name: 'test',
  }
  await truncate({
    db,
    tables: [table]
  })
  await insert({ db, table, values })
  const error = await t.throws(changeStatus({ db, table, status: 2, id: 1, statusList: [1, 3] }))
  t.is(error.message, 'status is not defined')
})