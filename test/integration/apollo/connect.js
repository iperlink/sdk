const test = require('ava')
const getMappedQuerys = require('../../../src/apolloPreprocessor/mappedQuerys')
const createQueryObj = require('../../../src/apolloPreprocessor/createQueryObj')
const connect = require('../../../src/apolloPreprocessor/connect')
const getStatic = require('../../../src/localStorage/getStaticData')
const component = require('./helpers/componentDef')
const obj = require('./helpers/mainData')

test('#getMappedQuerys', async t => {
  const dataSources = createQueryObj(obj)
  const querys = getMappedQuerys(dataSources)
  const connected = connect({ querys, component })
  t.is(connected.displayName, 'Apollo(Component)')
})
