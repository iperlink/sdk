module.exports = {
  english: {
    params: ['count'],
    fields: ['id', 'name']
  },
  spanish: {
    fields: ['id', 'name']
  },
  form: {
    spanish: 'spanish',
    english: 'english'
  }
}