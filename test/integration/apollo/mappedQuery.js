const test = require('ava')

const getMappedQuerys = require('../../../src/apolloPreprocessor/mappedQuerys')
const createQueryObj = require('../../../src/apolloPreprocessor/createQueryObj')
const obj = require('./helpers/mainData')


test('#getMappedQuerys', t => {
  const dataSources = createQueryObj(obj)
  const mapped = getMappedQuerys(dataSources)
  console.log(mapped.pilotList)
  console.log(mapped.pilotList.P)
  t.deepEqual(mapped.pilotList.P.name, 'pilots')
  t.deepEqual(mapped.pilotList.Q, 'query { pilots: pilots{name,_id,token,email,contract}}')
  t.pass()
})
