require('dotenv').config()
const test = require('ava')
const appendToHash = require('../../../src/redisUtils/appendToHash')
const start = require('../../../src/serviceStarter/tester')
const { getServices } = require('../../../src/serviceStarter')
const flush = require('../../../src/redisUtils/flush')

test('#redisUtils/appendToHash', async (t) => {
  await start()
  const { redis } = getServices()
  flush()
  await redis.hset('htest', 'test', JSON.stringify(['val']))
  await appendToHash({ redis, hash: 'htest', key: 'test', val: 'val2' })
  const appended = await redis.hget('htest', 'test')
  const compareTo = JSON.stringify(["val", "val2"])
  t.deepEqual(appended, compareTo)
})

test('#redisUtils/appendToHash error', async (t) => {
  await start()
  const { redis } = getServices()
  flush()
  await redis.hset('htest', 'test', JSON.stringify('val'))
  const error = await t.throws(
    appendToHash({ hash: 'htest', key: 'test', val: 'val2' })
  )
  const compareTo = 'key test in hash htest is not an Array'
  t.deepEqual(error.message, compareTo)
})
