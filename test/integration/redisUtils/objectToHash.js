require('dotenv').config()
const test = require('ava')
const start = require('../../../src/serviceStarter/tester')
const { getServices } = require('../../../src/serviceStarter')
const flush = require('../../../src/redisUtils/flush')
const objectTohash = require('../../../src/redisUtils/objectTohash')

test('#redisUtils/objectTohash', async (t) => {
  await start()
  const { redis } = getServices()
  flush()
  const obj = {
    key: 'val'
  }
  await objectTohash({ hash: 'htest', obj })
  const appended = await redis.hget('htest', 'key')
  const compareTo = JSON.stringify('val')
  t.deepEqual(appended, compareTo)
})
