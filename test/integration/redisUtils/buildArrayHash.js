require('dotenv').config()
const test = require('ava')
const start = require('../../../src/serviceStarter/tester')
const { getServices } = require('../../../src/serviceStarter')
const createPrivilege = require('../../../src/identicorp/privileges/createPrivilege')
const createApp = require('../../../src/identicorp/app/createApp')
const createTree = require('../../../src/identicorp/tree/createTree')
const createCompany = require('../../../src/identicorp/company/createCompany')
const createPrivilegeTree = require('../../../src/identicorp/privilege_tree/createPrivilegeTree')
const flush = require('../../../src/redisUtils/flush')
const createStatus = require('../../../src/identicorp/status/createStatus')
const buildArrayHash = require('../../../src/redisUtils/buildArrayHash')
const { createAppPrivTree, truncate } = require('../../helpers')

test('#getPrivilegeTree/', async (t) => {
  await start(['mysql', 'redis'])
  const { redis } = getServices()
  flush()
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree'
    ]
  })
  await createAppPrivTree()
  const privilegeTree = { tree_id: 1, privilege_id: 1 }
  const privilegeTree2 = { tree_id: 2, privilege_id: 1 }
  await createPrivilegeTree(privilegeTree)
  await createPrivilegeTree(privilegeTree2)
  await buildArrayHash({
    db: 'shared',
    table: 'privilege_tree',
    key: 'privilege_id',
    extract: 'tree_id'
  })
  const hash = await redis.hgetall('hash_privilege_tree')
  console.log(hash)
  t.pass()
})
