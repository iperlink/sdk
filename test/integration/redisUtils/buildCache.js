require('dotenv').config()
const test = require('ava')
const multiInsert = require('../../../src/mysqlUtils/insert/multiInsert')
const start = require('../../../src/serviceStarter/tester')
const { getServices } = require('../../../src/serviceStarter')
const flush = require('../../../src/redisUtils/flush')
const { truncate } = require('../../helpers')
const buildCache = require('../../../src/redisUtils/buildCache')

test('#buildCache/', async (t) => {
  await start(['redis', 'mysql'])
  const { redis } = getServices()
  flush()
  const values = []
  for (let i = 0; i < 10; i++) {
    values.push({
      name: `test_${i}`,
    })
  }

  const db = 'tester'
  const table = 'parent'
  await truncate({
    db,
    tables: ['parent', 'child', 'tester_materialized']
  })
  await multiInsert({
    table: 'parent',
    db: 'tester',
    values
  })
  console.time('test')
  await buildCache({ db, table })
  console.timeEnd('test')
  const found = await redis.hgetall(`hash_${table}`)
  const parsed = JSON.parse(found['8'])
  t.is(parsed.name, 'test_7')
})
