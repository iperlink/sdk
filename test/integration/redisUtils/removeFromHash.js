require('dotenv').config()
const test = require('ava')
const removeFromHash = require('../../../src/redisUtils/removeFromHash')
const start = require('../../../src/serviceStarter/tester')
const { getServices } = require('../../../src/serviceStarter')
const flush = require('../../../src/redisUtils/flush')

test('#redisUtils/removeFromHash', async (t) => {
  await start()
  const { redis } = getServices()
  flush()
  await redis.hset('htest', 'test', JSON.stringify(['val', 'val2']))
  await removeFromHash({ hash: 'htest', key: 'test', val: 'val' })
  const appended = await redis.hget('htest', 'test')
  const compareTo = JSON.stringify(["val2"])
  t.deepEqual(appended, compareTo)
})

test.only('#redisUtils/removeFromHash error', async (t) => {
  await start()
  const { redis } = getServices()
  flush()
  await redis.hset('htest', 'test', JSON.stringify('val'))
  const error = await t.throws(
    removeFromHash({ hash: 'htest', key: 'test', val: 'val2' })
  )
  const compareTo = 'key test in hash htest is not an Array'
  t.deepEqual(error.message, compareTo)
})
