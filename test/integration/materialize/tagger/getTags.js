require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  insert,
} = require('../../../../src/mysqlUtils')
const views = require('../helpers/views')
const onInsert = require('../../../../src/materialize/insert/onInsert')
const getTags = require('../../../../src/materialize/tagger/getTags')
const flush = require('../../../../src/redisUtils/flush')

test('#materialize/onInsert', async (t) => {
  const parent = {
    name: 'parent1'
  }
  const child = {
    name: 'child1',
    parent_id: 1
  }

  const parent2 = {
    name: 'parent1'
  }
  const child2 = {
    name: 'child2',
    parent_id: 2
  }

  await start(['redis', 'mysql'])
  const db = 'tester'
  const table = 'child'
  await truncate({
    db,
    tables: ['parent', 'child']
  })
  await truncate({
    db:'tester_materialized',
    tables: ['tag']
  })
  await insert({ db, table: 'parent', values: parent })
  await insert({ db, table: 'child', values: child })
  await insert({ db, table: 'parent', values: parent2 })
  await insert({ db, table: 'child', values: child2 })
  flush()
  console.time('test')
  await onInsert({ db, table, id: 1, views })
  await onInsert({ db, table, id: 2, views })
  console.time('0test')
  const tags = await getTags({ db: 'tester', table: 'parentView', ids: [1] })
  t.deepEqual(tags, ['PARENT1', 'CHILD1'])
  console.timeEnd('0test')
  console.log(tags)
  t.pass()
})

