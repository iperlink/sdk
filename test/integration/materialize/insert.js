require('dotenv').config()
const test = require('ava')
const start = require('../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  find,
  insert,
} = require('../../../src/mysqlUtils')
const views = require('./helpers/views')
const {
  setTableRef,
  insertMaterialized
} = require('../../../src/materialize')

test('#materialize/onInsert', async (t) => {
  const parent = {
    name: 'parent new1'
  }
  const parent2 = {
    name: 'parent2'
  }
  const child = {
    name: 'child1',
    parent_id: 1
  }

  await start()
  const db = 'tester'
  const table = 'child'
  await truncate({
    db,
    tables: ['parent', table]
  })
  await truncate({
    db:'tester_materialized',
    tables: ['childView', 'parentView']
  })
  await insert({ db, table: 'parent', values: parent })
  await insert({ db, table: 'parent', values: parent2 })
  await setTableRef({ views, db })
  await insertMaterialized({ db, table, values: child, views })
  console.time('test')
  const found = await find({ db: 'tester_materialized', table: 'parentView', queryObj: { id: 1 } })
  console.timeEnd('test')
  t.deepEqual(found[0].parent, 'parent new1')
  t.pass()
})

