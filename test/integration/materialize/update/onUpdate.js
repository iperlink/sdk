require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  insert,
  update
} = require('../../../../src/mysqlUtils')
const views = require('../helpers/views')
const setTableRef = require('../../../../src/materialize/helpers/setTableRef')
const onUpdate = require('../../../../src/materialize/update/onUpdate')

test('#materialize/onUpdate', async (t) => {
  const parent = {
    name: 'parent1'
  }
  const child = {
    name: 'child1',
    parent_id: 1
  }
  const updater = {
    name: 'updated'
  }

  await start()
  const db = 'tester'
  const table = 'child'
  await truncate({
    db,
    tables: ['parent', table]
  })
  await insert({ db, table: 'parent', values: parent })
  await insert({ db, table: 'child', values: child })
  await update({ db, table: 'parent', values: child })
  const tables = await setTableRef({ views, db })
  console.time('test')
  await update({ db, table: 'parent', values: updater, id: 1 })
  await onUpdate({ db, table: 'parent', id: 1, views })
  console.timeEnd('test')
  t.deepEqual(tables.child, ['parentView', 'childView'])
  t.pass()
})

