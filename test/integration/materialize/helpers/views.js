const parentView = {
  tables: {
    child: { name: 'child' },
    parent: { name: 'parent', ref: ' child.parent_id ' }
  },
  query: ({ tables, db, where = '' }) => `
  SELECT
    ${db}.parent.id,
    ${db}.parent.name as parent,
    ${db}.child.name,
    CONCAT(${db}.parent.name,'|',${db}.child.name) as tags
    FROM ${db}.${tables.child.name}
    inner join ${db}.${tables.parent.name} on ${db}.${tables.parent.name}.id = ${db}.${tables.parent.ref}
    ${where}
  `
}

const childView = {
  name: 'childView',
  tables: {
    child: { name: 'child', ref: 'child.id' },
  },
  query: ({ tables, db, where = '' }) => `
  SELECT
    ${db}.child.id,
    ${db}.child.name
    FROM ${db}.${tables.child.name}
    ${where}
  `
}


module.exports = {
  parentView,
  childView
}