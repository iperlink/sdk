require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  insert,
  find
} = require('../../../../src/mysqlUtils')
const views = require('../helpers/views')
const onInsert = require('../../../../src/materialize/insert/onInsert')
const flush = require('../../../../src/redisUtils/flush')

test('#materialize/onInsert', async (t) => {
  const parent = {
    name: 'parent1'
  }
  const child = {
    name: 'child1',
    parent_id: 1
  }

  await start()
  const db = 'tester'
  const table = 'child'
  await truncate({
    db,
    tables: ['parent', 'child']
  })
  await insert({ db, table: 'parent', values: parent })
  await insert({ db, table: 'child', values: child })
  flush()
  console.time('test')
  await onInsert({ db, table, id: 1, views })
  const found = await find({
    db: 'tester_materialized',
    table: 'parentView'
  })
  console.timeEnd('test')
  t.is(found[0].name, 'child1')
  t.is(found[0].parent, 'parent1')
  t.pass()
})

