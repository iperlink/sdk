require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const views = require('../helpers/views')
const setTableRef = require('../../../../src/materialize/helpers/setTableRef')

test('#createTable/index', async (t) => {
  await start(['redis'])
  const tables = await setTableRef({ views, db: 'tester' })
  t.deepEqual(tables.child, ['parentView', 'childView'])
  t.pass()
})

