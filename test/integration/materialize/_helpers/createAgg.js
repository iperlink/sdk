require('dotenv').config()
const test = require('ava')
const {
  insert,
  truncate,
} = require('../../../../src/mysqlUtils')
const start = require('../../../../src/serviceStarter/tester')
const createAgg = require('../../../../src/materialize/helpers/createAggregate')

test('#/', async (t) => {
  await start(['mysql'])
  const values = {
    name: 'test'
  }
  const values2 = {
    name: 'test2'
  }
  const values3 = {
    name: 'test3'
  }
  const db = 'tester'
  const table = 'child'
  await truncate({ table, db })
  await insert({ db, table, values })
  await insert({ db, table, values: values2 })
  await insert({ db, table, values: values3 })
  const query = 'SELECT * from tester.child where id= ?'
  const found = await createAgg({ query, id: 3 })
  t.is(found[0].name, 'test3')
})