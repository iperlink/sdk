require('dotenv').config()
const test = require('ava')
const start = require('../../../src/serviceStarter/tester')
const { truncate } = require('../../helpers')
const {
  insert,
  find,
} = require('../../../src/mysqlUtils')
const views = require('./helpers/views')
const {
  setTableRef,
  updateMaterialized
} = require('../../../src/materialize')

test('#materialize/onUpdate', async (t) => {
  const parent = {
    name: 'parent new1'
  }
  const parent2 = {
    name: 'parent2'
  }
  const child = {
    name: 'child1',
    parent_id: 1
  }
  const updater = {
    name: 'updated'
  }

  await start()
  const db = 'tester'
  const table = 'child'
  await truncate({
    db,
    tables: ['parent', table]
  })
  await truncate({
    db:'tester_materialized',
    tables: ['childView', 'parentView']
  })
  await insert({ db, table: 'parent', values: parent })
  await insert({ db, table: 'parent', values: parent2 })
  await insert({ db, table: 'child', values: child })
  await setTableRef({ views, db })
  await updateMaterialized({ db, table: 'parent', values: updater, views, id: 1 })
  const found = await find({ db: 'tester_materialized', table: 'parentView', queryObj: { id: 1 } })
  t.is(found[0].parent, 'updated')
})

