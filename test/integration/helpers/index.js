module.exports = {
  createNodes: require('./createNodes'),
  createAppCompany: require('./createAppCompany'),
  createStatus: require('../../../src/identicorp/status/createStatus'),
  truncate: require('./truncate'),
  createApp: require('./createApp'),
  createCompany: require('./createCompany'),
  createAppPrivTree: require('./createAppPrivTree'),
  createuser: require('./createUser'),
  fullIdenticorp: require('./fullIdenticorp'),
}