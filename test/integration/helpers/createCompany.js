const createCompany = require('../../../src/identicorp/company/createCompany')


module.exports = async () => {
  const company = {
    name: 'new.co; truncate table shared.app',
    rut: '69.999.666.0',
    status: 1
  }
  await createCompany(company)
}