const createUser = require('../../../src/identicorp/user/createUser')
const deleteCognitoUser = require('../../../src/identicorp/user/deleteCognito')


module.exports = async () => {
  const user = {
    name: 'test',
    lastName: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  await deleteCognitoUser(user)
  const privileges = [
    { privilege_id: 1, node_ids: [2, 11] },
    { privilege_id: 2, node_ids: [2, 11] },
  ]
  const nodes = [1, 2]
  await createUser({ user, privileges, nodes })
}