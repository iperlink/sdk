const createUser = require('./createUser')
const createAppPrivTree = require('./createAppPrivTree')
const createNodes = require('./createNodes')
const setNodes = require('../../../src/identicorp/node/setNodes')


module.exports = async () => {
  await createAppPrivTree()
  await createNodes()
  await setNodes()
  await createUser()
}