const createApp = require('../../../src/identicorp/app/createApp')
const createPrivilege = require('../../../src/identicorp/privileges/createPrivilege')
const createAppCompany = require('../../../src/identicorp/app_company/createAppCompany')
const createCompany = require('../../../src/identicorp/company/createCompany')
const createStatus = require('../../../src/identicorp/status/createStatus')
const createTree = require('../../../src/identicorp/tree/createTree')

module.exports = async () => {
  const company = {
    name: 'new.co',
    rut: '69.999.666.0',
    status: 1
  }
  const status = [{ name: 'active' }, { name: 'cancelled' }, { name: 'terminated' }]
  const privilege = { name: 'create data', description: 'create dat', app_id: '1' }
  const privilege2 = { name: 'create data2', description: 'create dat', app_id: '1' }
  const tree = { name: 'geografico', description: 'create dat', company_id: '1' }
  const tree2 = { name: 'centro de costos', description: 'create dat', company_id: '1' }
  await Promise.all([
    createStatus(status),
    createCompany(company),
    createApp({ name: 'app1' }),
    createAppCompany({ company, app: 1 }),
    createPrivilege(privilege),
    createPrivilege(privilege2),
    createTree(tree),
    createTree(tree2),
  ])
}