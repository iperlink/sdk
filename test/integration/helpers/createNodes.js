const {
  truncate,
  multiInsert,
  insert,
} = require('../../../src/mysqlUtils')
const createNode = require('../../../src/identicorp/node/createNode')

module.exports = async () => {
  const db = 'shared'
  const table = 'node'
  await truncate({ db, table })
  await createNode({ tree_id: 1, name: 'chile' })
  await createNode({ tree_id: 2, name: 'gerencia general' })//id2

  await createNode({ tree_id: 1, name: 'rm', parent_node: 1 }) //id3
  await createNode({ tree_id: 1, name: 'santiago', parent_node: 3 }) //id4
  await createNode({ tree_id: 1, name: 'melipilla', parent_node: 3 }) //id5
  await createNode({ tree_id: 1, name: 'v', parent_node: 1 }) //id6
  await createNode({ tree_id: 1, name: 'valparaiso', parent_node: 6 })//id7
  await createNode({ tree_id: 1, name: 'quilpue', parent_node: 7 })//id8
  await createNode({ tree_id: 1, name: 'cerro alegre', parent_node: 7 })//id9
  await createNode({ tree_id: 1, name: 'quilicura', parent_node: 4 })//id10
  await createNode({ tree_id: 1, name: 'renca', parent_node: 4 })//id11
  await createNode({ tree_id: 2, name: 'rrhh', parent_node: 2 })//id12
  await createNode({ tree_id: 2, name: 'operaciones', parent_node: 2 })//id13
  await createNode({ tree_id: 2, name: 'vigilancia', parent_node: 13 })//id14
  await createNode({ tree_id: 2, name: 'medioambiente', parent_node: 13 })//id15

}