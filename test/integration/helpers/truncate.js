const truncate = require('../../../src/mysqlUtils/truncate')

module.exports = async ({ db, tables }) => {
  const promiseArray = tables.map(table => {
    return truncate({ db, table })
  })
  await Promise.all(promiseArray)
}