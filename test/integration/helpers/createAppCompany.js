const createAppCompany = require('../../../src/identicorp/app_company/createAppCompany')
const createCompany = require('../../../src/identicorp/company/createCompany')
const createApp = require('../../../src/identicorp/app/createApp')


module.exports = async () => {
  const company = {
    name: 'new.co; truncate table shared.app',
    rut: '69.999.666.0',
    status: 1
  }
  await createCompany(company)
  await createApp({ name: 'app1' })
  await createAppCompany({ company, app: 1 })
}