const test = require('ava')
const uploadConfig = require('../../../../src/deploy/uploadConfig')
const { start } = require('../../../../src/serviceStarter')
const AWS = require('aws-sdk')
const path = require('path')
require('dotenv').config()

test('#getMappedQuerys', async t => {
  await start({ dependencies: { AWS }, servicesList: ['s3', 'awsCreds'] })
  const containerFolder = path.parse(__filename).dir + '/helpers'
  const functs = [
    'querys', 'menu', 'aws', 'childRoutes', 'masterRoute'
  ]
  await uploadConfig({ functs, bucket: 'paneldecontrolcl', name: 'mainData', containerFolder })
})
