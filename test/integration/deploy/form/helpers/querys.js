const createQueryObj = require('../../../../src/apolloPreprocessor/createQueryObj')

module.exports = () => ({
  querys: createQueryObj({
    form: {
      english: {
        fields: [
          'id',
          'name',
          'value'
        ]
      },
      spanish: {
        fields: [
          'id',
          'name'
        ]
      }
    }
  })
})