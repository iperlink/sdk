const mapValues = require('../../../../src/objMapper/mapValues')

const templates = {
  material: {
    textInput: {
      containerCL: 'gridCont',
      inputCl: 'inputCl',
      labelCl: 'labelCl'
    },
    checkbox: {
      containerCL: 'checkboxCont',
      inputCl: 'checkBoxContCl',
      labelCl: 'labelCl'
    }
  }
}

module.exports = ({ extra, template, type }) => {
  const mapped = mapValues({
    obj: templates[template][type],
    fn: val => {
      return `${val} ${extra}`
    }
  })
  return mapped
}