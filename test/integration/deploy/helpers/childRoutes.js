module.exports = () => ({
  childs: [
    {
      path: '/one',
      component: {
        type: 'plain',
        props: {
          name: 'one',
          id: 'one',
        },
        queryName: 'english'
      }
    },
    {
      path: '/two/:paramfromRoute',
      component: {
        type: 'dummyCLick',
        props: {
          name: 'two',
          id: 'two'
        },
        queryName: 'addChild',
      },
    },
    {
      path: '/two',
      component: {
        type: 'dummyCLick',
        props: {
          name: 'two',
          id: 'two'
        },
        queryName: 'addChild'
      },
    },
    {
      path: '/three',
      component: {
        type: 'form',
        props: {
          name: 'one',
          id: 'basic',
        },
        queryName: 'form'
      }
    }
  ]
})