const createQueryObj = require('../../../../src/apolloPreprocessor/createQueryObj')

module.exports = () => ({
  querys: createQueryObj({
    english: {
      fields: [
        'name',
        'id'
      ],
      type: 'query'
    },
    spanish: {
      fields: [
        'name',
        'id'
      ],
      type: 'query'
    },
    getChild: {
      fields: [
        'name',
        'id'
      ],
      type: 'query'
    },
    addChild: {
      fields: [
        'name',
        'id'
      ],
      params: [
        { name: 'name', type: 'String!' },
        { name: 'nodeId', type: 'String!' },
        { name: 'parent_id', type: 'Int!' }
      ],
      updatedQuerys: ['getChild'],
      type: 'mutation'
    },
    form: {
      english: 'english',
      spanish: 'spanish',
      initial: 'initial'
    }
  })
})