module.exports = () => ({
  master: {
    path: '/',
    component: {
      type: 'plain',
      props: {
        name: 'three',
        id: 'three',
      },
      queryName: 'english'
    },
  }
})
