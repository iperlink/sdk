module.exports = () => ({
  menu: {
    section1: [{ title: 'sub1_1', path: 'one' }, { title: 'sub1_2', path: 'two' }],
    section2: [{ title: 'sub2_1', path: 'three' }]
  }
})
