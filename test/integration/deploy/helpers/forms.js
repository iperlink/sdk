const validate = require('../../../../src/reactComponents/formCreator/template.validate')
const classes = require('./formUtils')

const basic = {
  select: {
    classes: classes({ template: 'material', type: 'textInput', extra: 'ClNN', itemsCl: 'itemsCl' }),
    id: `select1`,
    invalidMsg: 'tis invalid',
    name: `select1`,
    options: { from: 'english', label: 'name', value: 'id' },
    type: 'select',
    value: 4,
  },
  select2: {
    classes: classes({ template: 'material', type: 'textInput', extra: 'ClNN', itemsCl: 'itemsCl' }),
    id: `select2`,
    invalidMsg: 'tis invalid',
    name: `select2`,
    options: { from: 'spanish', label: 'name', value: 'id' },
    type: 'typeAhead',
    value: ''
  }
}

module.exports = () => ({
  forms: {
    basic: validate(basic)
  }
})
