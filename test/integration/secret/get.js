require('dotenv').config()
const test = require('ava')
const get = require('../../../src/secret/get')
const AWS = require('aws-sdk')

test('#secret/get', async (t) => {
  new AWS.EnvironmentCredentials('AWS')
  console.time('test')
  const secret = await get('supersecret')
  console.timeEnd('test')
  const val = JSON.parse(secret).supersecretkey
  t.is(val, 'supersecretvalue')
})
