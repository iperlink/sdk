require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')
const createPrivilegeTree = require('../../../../src/identicorp/privilege_tree/createPrivilegeTree')
const flush = require('../../../../src/redisUtils/flush')
const getPrivilegeTree = require('../../../../src/identicorp/privilege_tree/getPrivilegeTree')
const rebuildPrivilegeTree = require('../../../../src/identicorp/privilege_tree/rebuildPrivilegeTree')

test('#getPrivilegeTree/', async (t) => {
  await start()
  const db = 'shared'
  const { redis } = getServices()
  flush()
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company']
  })
  await createAppPrivTree()
  const privilegeTree = { tree_id: 1, privilege_id: 1 }
  const privilegeTree2 = { tree_id: 1, privilege_id: 2 }
  await createPrivilegeTree(privilegeTree)
  await createPrivilegeTree(privilegeTree2)
  flush()
  await rebuildPrivilegeTree()
  await redis.hgetall('hash_privilege_tree')
  const hash = await getPrivilegeTree(1)
  t.is(hash[0], 1)
})