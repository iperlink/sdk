require('dotenv').config()
const test = require('ava')
const {
  find
} = require('../../../../src/mysqlUtils')
const { truncate, createAppPrivTree } = require('../../helpers')
const { getServices } = require('../../../../src/serviceStarter')
const start = require('../../../../src/serviceStarter/tester')
const createPrivilegeTree = require('../../../../src/identicorp/privilege_tree/createPrivilegeTree')
const cancellPrivilegeTree = require('../../../../src/identicorp/privilege_tree/cancellPrivilegeTree')
const flush = require('../../../../src/redisUtils/flush')

test('#cancellPrivilegeTree/', async (t) => {
  await start()
  const db = 'shared'
  flush()
  const { redis } = getServices()
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company']
  })
  await createAppPrivTree()
  const privilegeTree = { tree_id: 1, privilege_id: 1 }
  await createPrivilegeTree(privilegeTree)
  await cancellPrivilegeTree({ privilege_id: 1, tree_id: 1 })
  const priv = await find({ table: 'privilege_tree', db: 'shared' })
  const res = priv[0].status
  const hash = await redis.hgetall('hash_privilegeTree')
  t.is(hash['1'], '[]')
  t.is(res, 2)
})

test('#cancellPrivilegeTree. error/', async (t) => {
  await start()
  const db = 'shared'
  flush()
  const { redis } = getServices()
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company']
  })
  await createAppPrivTree()
  const privilegeTree = { tree_id: 1, privilege_id: 1 }
  await createPrivilegeTree(privilegeTree)
  const error = await t.throws(cancellPrivilegeTree({ privilege_id: 1, tree_id: 10 }))
  t.is(error.message, 'error updated privilege_tree Ref does not exist')
})
