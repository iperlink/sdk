require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')
const createPrivilegeTree = require('../../../../src/identicorp/privilege_tree/createPrivilegeTree')
const flush = require('../../../../src/redisUtils/flush')
const getPrivilegeTree = require('../../../../src/identicorp/privilege_tree/getPrivilegeTree')

test('#getPrivilegeTree/', async (t) => {
  await start()
  const db = 'shared'
  flush()
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company']
  })
  await createAppPrivTree()
  const privilegeTree = { tree_id: 1, privilege_id: 1 }
  await createPrivilegeTree(privilegeTree)
  const hash = await getPrivilegeTree(1)
  t.is(hash[0], 1)
})
