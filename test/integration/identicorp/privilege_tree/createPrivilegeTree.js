require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')
const createPrivilegeTree = require('../../../../src/identicorp/privilege_tree/createPrivilegeTree')
const flush = require('../../../../src/redisUtils/flush')

test('#createPrivilegeTree/', async (t) => {
  await start()
  const db = 'shared'
  const { redis } = getServices()
  flush()
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company']
  })
  await createAppPrivTree()
  const privilegeTree = { tree_id: 1, privilege_id: 1 }
  const res = await createPrivilegeTree(privilegeTree)
  const hash = await redis.hgetall('hash_privilegeTree')
  t.is(hash['1'], '[1]')
  t.is(res.id, 1)
})

test('#createPrivilegeTree error missing ref', async (t) => {
  await start()
  const db = 'shared'
  flush()
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company']
  })
  await createAppPrivTree()
  const privilegeTree = { tree_id: 1, privilege_id: 3 }
  const error = await t.throws(createPrivilegeTree(privilegeTree))
  t.is(error.message, 'error creating privilege_tree noRef')
})

test('#createPrivilege error duplicateEntry', async (t) => {
  await start()
  const db = 'shared'
  flush()
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company']
  })
  await createAppPrivTree()
  const privilegeTree = { tree_id: 1, privilege_id: 1 }
  await createPrivilegeTree(privilegeTree)
  const error = await t.throws(createPrivilegeTree(privilegeTree))
  t.is(error.message, 'error creating privilege_tree duplicateEntry')
})
