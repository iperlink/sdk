require('dotenv').config()
const test = require('ava')
const {
  multiInsert,
} = require('../../../../src/mysqlUtils')
const { truncate, createAppPrivTree } = require('../../helpers')
const createNode = require('../../../../src/identicorp/node/createNode')
const start = require('../../../../src/serviceStarter/tester')
const getContent = require('../../../../src/identicorp/content/getContent')

test('#getContent finds/', async (t) => {
  await start()
  await truncate({
    db: 'tester',
    tables: ['content_node']
  })
  await truncate({
    db: 'shared',
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'user',
      'node_user_privilege',
    ]
  })
  await multiInsert({
    db: 'tester',
    table: 'content_node',
    values: [{
      privilege_id: 1,
      content_id: 1,
      node_id: 1
    }, {
      privilege_id: 1,
      content_id: 2,
      node_id: 1
    }, {
      privilege_id: 1,
      content_id: 1,
      node_id: 2
    }
    ]
  })
  await createAppPrivTree()

  const cont = await getContent({ userNodes: { nodes: [1, 2], trees: 2 }, db: 'tester', privilegeId: 1 })
  const compareTo = [1]
  t.deepEqual(compareTo, cont)
})

test('#getContent does not find/', async (t) => {
  await start()
  await truncate({
    db: 'tester',
    tables: ['content_node']
  })
  await truncate({
    db: 'shared',
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'user',
      'node_user_privilege',
    ]
  })
  await multiInsert({
    db: 'tester',
    table: 'content_node',
    values: [{
      privilege_id: 1,
      content_id: 1,
      node_id: 1
    }, {
      privilege_id: 1,
      content_id: 2,
      node_id: 1
    }, {
      privilege_id: 1,
      content_id: 1,
      node_id: 2
    }
    ]
  })
  await createAppPrivTree()
  const node = { tree_id: 1, name: 'chile' }
  await createNode(node)
  const cont = await getContent({ userNodes: { nodes: [1, 4], trees: 2 }, db: 'tester', privilegeId: 1 })
  const compareTo = []
  t.deepEqual(compareTo, cont)
})
