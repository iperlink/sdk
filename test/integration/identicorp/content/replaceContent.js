require('dotenv').config()
const test = require('ava')
const {
  truncate,
} = require('../../../../src/mysqlUtils')
const replaceContent = require('../../../../src/identicorp/content/replaceContent')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')

test('#replace content/', async (t) => {
  process.env.DB_NAME = 'tester'
  await start()
  const { mysql } = getServices()
  await truncate({ db: 'tester', table: 'content_node' })
  const content_node = {
    privilege_id: 14,
    node_id: '15,21',
    content_id: 20
  }
  const content_node2 = {
    privilege_id: 14,
    node_id: '12,20',
    content_id: 20
  }
  await replaceContent(content_node)
  await replaceContent(content_node2)
  const [found] = await mysql.execute('SELECT * from tester.content_node')
  const nodeIds = found.map(x => x.node_id)
  t.deepEqual(nodeIds, [12, 20])
})

test.only('#replace content/', async (t) => {
  process.env.DB_NAME = 'tester'
  await start()
  const { mysql } = getServices()
  await truncate({ db: 'tester', table: 'content_node' })
  const content_node = {
    privilege_id: 14,
    node_id: '15,21',
    content_id: 20
  }
  const content_node2 = {
    privilege_id: 14,
    node_id: '12,20',
    content_id: 22
  }
  await replaceContent(content_node)
  await replaceContent(content_node2)
  const [found] = await mysql.execute('SELECT * from tester.content_node')
  const nodeIds = found.map(x => x.node_id)
  t.deepEqual(nodeIds, [15, 21, 12, 20])
})
