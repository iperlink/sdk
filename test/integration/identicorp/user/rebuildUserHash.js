require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')
const createUser = require('../../../../src/identicorp/user/createUser')
const createNode = require('../../../../src/identicorp/node/createNode')
const flush = require('../../../../src/redisUtils/flush')
const deleteCognitoUser = require('../../../../src/identicorp/user/deleteCognito')
const getUser = require('../../../../src/identicorp/user/getUser')

test('#updateUser/', async (t) => {
  await start()
  const db = 'shared'
  const { redis } = getServices()
  flush()
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'status',
      'user',
      'node_user_privilege',
    ]
  })
  await createAppPrivTree()
  const node = { tree_id: 1, name: 'chile' }
  await createNode(node)

  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges = [
    { privilege_id: 1, node_ids: [1] },
    { privilege_id: 2, node_ids: [1] },
  ]
  await deleteCognitoUser(user)
  await createUser({ user, privileges })
  flush()
  const found = await getUser('test@test.com')
  const compareTo = {
    id: 1,
    email: 'test@test.com',
    name: 'test',
    lastName: 'user',
    privilegeArray: [1, 2],
    privilegeObj: { '1': [1], '2': [1] }
  }

  t.deepEqual(found, compareTo)
})
