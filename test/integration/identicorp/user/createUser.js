require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree, createNodes } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')
const deleteCognitoUser = require('../../../../src/identicorp/user/deleteCognito')
const flush = require('../../../../src/redisUtils/flush')
const createUser = require('../../../../src/identicorp/user/createUser')
const createPrivilegeTree = require('../../../../src/identicorp/privilege_tree/createPrivilegeTree')
const getNodesSelect = require('../../../../src/identicorp/node/getNodesSelect')

test.only('#createUser/', async (t) => {
  await start({})
  const db = 'shared'
  const { redis } = getServices()
  // flush()
  await truncate({
    db,
    tables: [
      'app',
      'app_company',
      'company',
      // 'node',
      'node_user_privilege',
      'privilege',
      'privilege_tree',
      'tree',
      'status',
      'user',
    ]
  })
  await createAppPrivTree()
  // await createNodes()
  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges2 = [
    { privilege_id: 1, node_ids: [1, 2] },
    { privilege_id: 2, node_ids: [1, 2] },
  ]
  await deleteCognitoUser(user)
  await createPrivilegeTree({ tree_id: 1, privilege_id: 1 })
  await createPrivilegeTree({ tree_id: 1, privilege_id: 2 })
  await createPrivilegeTree({ tree_id: 2, privilege_id: 1 })
  await createPrivilegeTree({ tree_id: 2, privilege_id: 2 })
  const privileges = [1, 2]
  const nodes = [1, 2]
  const res = await createUser({ user, privileges, nodes })
  const nodesSelectRaw = await getNodesSelect()
  const nodesSelect = JSON.parse(nodesSelectRaw)
  const priv = await redis.hgetall('hash_user')
  const userPriv = JSON.parse(priv['test@test.com'])
  console.log(userPriv)
  t.is(userPriv.name, 'test')
  t.is(res.id, 1)
})

test('#createUser fail invalid email/', async (t) => {
  await start({})
  const db = 'shared'
  const { redis } = getServices()
  flush()
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'user',
      'node_user_privilege',
    ]
  })
  await createAppPrivTree()
  await createNodes()
  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@testcom',
    company_id: 1
  }
  const privileges = [
    { privilege_id: 1, node_ids: [1] },
    { privilege_id: 2, node_ids: [1] },
  ]
  await deleteCognitoUser(user)
  const error = await t.throws(createUser({ user, privileges }))
  t.is(error.message, 'email is invalid')
})

test('#createUser fail cognito/', async (t) => {
  await start({})
  const db = 'shared'
  const { redis } = getServices()
  flush()
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'user',
      'node_user_privilege',
    ]
  })
  await createAppPrivTree()
  await createNodes()
  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges = [
    { privilege_id: 1, node_ids: [1] },
    { privilege_id: 2, node_ids: [1] },
  ]
  await deleteCognitoUser(user)
  process.env.USER_POOL_ID = 'asdas'
  await t.throws(createUser({ user, privileges }))
  const priv = await redis.hgetall('hash_user')
  t.deepEqual(priv, {})
})

