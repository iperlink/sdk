require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree } = require('../../helpers')
const { find } = require('../../../../src/mysqlUtils')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')
const createUser = require('../../../../src/identicorp/user/createUser')
const updateUser = require('../../../../src/identicorp/user/updateUser')
const createNode = require('../../../../src/identicorp/node/createNode')
const flush = require('../../../../src/redisUtils/flush')
const deleteCognitoUser = require('../../../../src/identicorp/user/deleteCognito')

test('#updateUser/', async (t) => {
  await start()
  const db = 'shared'
  const { redis } = getServices()
  flush()
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'user',
      'node_user_privilege',
    ]
  })
  await createAppPrivTree()
  const node = { tree_id: 1, name: 'chile' }
  await createNode(node)

  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges = [
    { privilege_id: 1, node_ids: [1] },
    { privilege_id: 2, node_ids: [1] },
  ]
  await deleteCognitoUser(user)
  const res = await createUser({ user, privileges })
  const priv = await redis.hgetall('hash_user')
  const userPriv = JSON.parse(priv['test@test.com'])
  const user2 = {
    name: 'testUpdated',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges2 = [
    { privilege_id: 1, node_ids: [1] },
  ]
  const foundPre = await find({
    db: 'shared',
    table: 'node_user_privilege'
  })
  t.is(foundPre.length, 2)
  console.time('test')
  await updateUser({ user: user2, id: 1, privileges: privileges2, app_id: 1 })
  console.timeEnd('test')
  const found = await find({
    db: 'shared',
    table: 'node_user_privilege'
  })
  const foundUser = await find({
    db: 'shared',
    table: 'user'
  })
  t.is(foundUser[0].name, 'testUpdated')
  t.is(found.length, 1)
  t.is(found[0].id, 3)
  t.is(userPriv.name, 'test')
  t.is(res.id, 1)
})
