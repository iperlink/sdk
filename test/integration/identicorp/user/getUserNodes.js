require('dotenv').config()
const test = require('ava')
const { truncate, createNodes, createAppPrivTree } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const getUserNodes = require('../../../../src/identicorp/user/getUserNodes')
const getUserSingleNode = require('../../../../src/identicorp/user/getUserSingleNode')
const getTree = require('../../../../src/identicorp/tree/getTree')
const deleteCognitoUser = require('../../../../src/identicorp/user/deleteCognito')
const setNodes = require('../../../../src/identicorp/node/setNodes')
const flush = require('../../../../src/redisUtils/flush')
const createUser = require('../../../../src/identicorp/user/createUser')
const createPrivilegeTree = require('../../../../src/identicorp/privilege_tree/createPrivilegeTree')


test.only('#getUserNodes/', async (t) => {
  await start({})
  const db = 'shared'
  flush()
  await truncate({
    db,
    tables: [
      'app',
      'app_company',
      'company',
      // 'node',
      'node_user_privilege',
      'privilege',
      'privilege_tree',
      'tree',
      'status',
      'user',
    ]
  })
  await createAppPrivTree()
  await createNodes()
  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges2 = [
    { privilege_id: 1, node_ids: [1, 2] },
    { privilege_id: 2, node_ids: [1, 2] },
  ]
  const privileges = [1, 2]
  const nodes = [1, 2]
  await deleteCognitoUser(user)
  await createPrivilegeTree({ tree_id: 1, privilege_id: 1 })
  await createPrivilegeTree({ tree_id: 1, privilege_id: 2 })
  await createPrivilegeTree({ tree_id: 2, privilege_id: 1 })
  await createPrivilegeTree({ tree_id: 2, privilege_id: 2 })
  await createUser({ user, privileges, nodes })
  const found = await getUserNodes({ user: user.email, privilege_id: null })
  console.log(found)
  const n = await getUserSingleNode({ user: user.email, index: 0 })
  console.log(n)
  //const compareTo = { nodes: [1, 3, 6, 4, 5, 7, 8, 9, 10, 11], trees: 2 }
  t.pass()
})

test('#getUserNodes error no tree/', async (t) => {
  await start()
  flush()
  const db = 'shared'
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'user',
      'node_user_privilege',
    ]
  })
  await createAppPrivTree()
  await createNodes()
  await setNodes()
  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges = [
    { privilege_id: 1, node_ids: [1] },
    { privilege_id: 2, node_ids: [1] },
  ]
  await deleteCognitoUser(user)
  await createUser({ user, privileges })


  const error = await t.throws(
    getUserNodes({ userEmail: 'test@test.com', privilege_id: 1 })
  )

  t.is(error.message, 'privilege 1 has no tree')
})
