require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const createUser = require('../../../../src/identicorp/user/createUser')
const createNode = require('../../../../src/identicorp/node/createNode')
const flush = require('../../../../src/redisUtils/flush')
const deleteCognitoUser = require('../../../../src/identicorp/user/deleteCognito')
const getUserListFromNode = require('../../../../src/identicorp/user/getUserListFromNode')

test('#getUser/', async (t) => {
  await start()
  const db = 'shared'
  flush()
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'status',
      'user',
      'node_user_privilege',
    ]
  })
  await createAppPrivTree()
  const node = { tree_id: 1, name: 'chile' }
  await createNode(node)
  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges = [
    { privilege_id: 1, node_ids: [1] },
    { privilege_id: 2, node_ids: [1] },
  ]
  await deleteCognitoUser(user)
  await createUser({ user, privileges })
  const users = await getUserListFromNode({ privilege_id: 1, node_id: 1 })
  t.deepEqual(users[0].name, 'test')
})

test('#getUser not found/', async (t) => {
  await start()
  const db = 'shared'
  flush()
  await truncate({
    db,
    tables: [
      'app',
      'app_company',
      'company',
      'node',
      'node_user_privilege',
      'privilege',
      'privilege_tree',
      'status',
      'tree',
      'user',
    ]
  })
  await createAppPrivTree()
  const node = { tree_id: 1, name: 'chile' }
  await createNode(node)
  const user = {
    company_id: 1,
    email: 'test@test.com',
    lastname: 'user',
    name: 'test',
  }
  const privileges = [
    { privilege_id: 1, node_ids: [1] },
    { privilege_id: 2, node_ids: [1] },
  ]
  await deleteCognitoUser(user)
  await createUser({ user, privileges })
  const users = await getUserListFromNode({ privilege_id: 1, node_id: 2 })
  t.deepEqual(users.length, 0)
})
