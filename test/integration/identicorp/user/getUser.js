require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')
const createUser = require('../../../../src/identicorp/user/createUser')
const getUser = require('../../../../src/identicorp/user/getUser')
const createNode = require('../../../../src/identicorp/node/createNode')
const flush = require('../../../../src/redisUtils/flush')
const deleteCognitoUser = require('../../../../src/identicorp/user/deleteCognito')

test.only('#getUser/', async (t) => {
  await start({})
  const db = 'shared'
  const { redis } = getServices()
  flush()
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'user',
      'node_user_privilege',
    ]
  })
  await createAppPrivTree()
  const node = { tree_id: 1, name: 'chile' }
  await createNode(node)
  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges = [
    { privilege_id: 1, node_ids: [1] },
    { privilege_id: 2, node_ids: [1] },
  ]
  await deleteCognitoUser(user)
  await createUser({ user, privileges })
  const first = await getUser('test@test.com')
  const hash = await redis.hgetall('hash_user')
  const parsed = JSON.parse(hash['test@test.com'])
  t.is(parsed.email, 'test@test.com')
  t.is(first.email, 'test@test.com')
  t.deepEqual(first.privilegeArray, [1, 2])
})

test('#getUser not found/', async (t) => {
  await start()
  const db = 'shared'
  flush()
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'user',
      'node_user_privilege',
    ]
  })
  await createAppPrivTree()
  const node = { tree_id: 1, name: 'chile' }
  await createNode(node)
  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges = [
    { privilege_id: 1, node_ids: [1] },
    { privilege_id: 2, node_ids: [1] },
  ]
  await deleteCognitoUser(user)
  await createUser({ user, privileges })
  const first = await getUser('missing@test.com')
  t.is(first, null)
})
