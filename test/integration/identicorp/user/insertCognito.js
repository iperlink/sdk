require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const createCognitoUser = require('../../../../src/identicorp/user/createUser/insertCognito')
const deleteCognitoUser = require('../../../../src/identicorp/user/deleteCognito')


test('#createUser/', async (t) => {
  await start()
  const user = {
    id: 1,
    email: 'andres@gmail.com'
  }
  await deleteCognitoUser(user)
  await createCognitoUser(user)
  t.pass()
})
