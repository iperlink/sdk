require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const createUser = require('../../../../src/identicorp/user/createUser')
const userCan = require('../../../../src/identicorp/user/userCan')
const createNode = require('../../../../src/identicorp/node/createNode')
const flush = require('../../../../src/redisUtils/flush')
const deleteCognitoUser = require('../../../../src/identicorp/user/deleteCognito')

test('#userCan/', async (t) => {
  await start()
  const db = 'shared'
  flush()
  await truncate({
    db,
    tables: [
      'app',
      'app_company',
      'company',
      'node',
      'node_user_privilege',
      'privilege',
      'privilege_tree',
      'status',
      'tree',
      'user',
    ]
  })
  await createAppPrivTree()
  const node = { tree_id: 1, name: 'chile' }
  await createNode(node)

  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges = [
    { privilege_id: 1, node_ids: [1] },
    { privilege_id: 2, node_ids: [1] },
  ]
  await deleteCognitoUser(user)
  await createUser({ user, privileges })
  console.log('called')
  flush()
  console.time('test')
  const found = await userCan({ userEmail: 'test@test.com', privilege: 1 })
  console.log(found)
  console.timeEnd('test')
  t.true(found)
})
