require('dotenv').config()
const test = require('ava')
const {
  truncate
} = require('../../../../src/mysqlUtils')
const start = require('../../../../src/serviceStarter/tester')
const createApp = require('../../../../src/identicorp/app/createApp')

test('#createApp/', async (t) => {
  await start()
  await truncate({ db: 'shared', table: 'app' })
  const created = await createApp('appName')
  t.is(created.id, 1)
})

test.only('#createApp error/', async (t) => {
  await start({})
  await truncate({ db: 'shared', table: 'app' })
  await createApp('appName')
  try {
    await createApp('appName')
  } catch (e) {
    console.log(e)
  } //const error = await t.throws(createApp('appName'))
  //
  //t.is(error.message, 'error creating app duplicateEntry')
})
