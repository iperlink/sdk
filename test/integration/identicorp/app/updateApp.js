require('dotenv').config()
const test = require('ava')
const {
  truncate,
  find
} = require('../../../../src/mysqlUtils')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')
const updateApp = require('../../../../src/identicorp/app/updateApp')

test('#updateApp/', async (t) => {
  await start(['mysql'])
  const { mysql } = getServices()
  const app = { name: 'appName2' }
  await truncate({ db: 'shared', table: 'app' })
  await mysql.query('insert into shared.app set ?', [{ name: 'appName' }])
  const updated = await updateApp({ app, id: 1 })
  const found = await find({ db: 'shared', table: 'app', queryObj: { id: 1 } })
  t.is(found[0].name, 'appName2')
  t.is(updated, 1)
})

test('#updateApp error/', async (t) => {
  await start(['mysql'])
  const { mysql } = getServices()
  const app = { name: 'appName2' }
  await truncate({ db: 'shared', table: 'app' })
  await mysql.query('insert into shared.app set ?', [{ name: 'appName' }])
  const error = await t.throws(updateApp({ app, id: 2 }))
  t.is(error.message, 'error updating app Ref does not exist')
  const found = await find({ db: 'shared', table: 'app', queryObj: { id: 1 } })
  t.is(found[0].name, 'appName')
})