require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const createNode = require('../../../../src/identicorp/node/createNode')

test('#createNode/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company', 'node']
  })
  await createAppPrivTree()
  const node = { tree_id: 1, name: 'chile' }
  const res = await createNode(node)
  t.is(res.id, 1)
})

test('#createNode error duplicateEntry', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company', 'node']
  })
  await createAppPrivTree()
  const node = { tree_id: 1, name: 'chile' }
  await createNode(node)
  const error = await t.throws(createNode(node))
  t.is(error.message, 'error creating node duplicateEntry')
})
