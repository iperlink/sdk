require('dotenv').config()
const test = require('ava')
const { truncate, createNodes, createAppPrivTree } = require('../../helpers')
const flush = require('../../../../src/redisUtils/flush')
const start = require('../../../../src/serviceStarter/tester')
const getNodesSelect = require('../../../../src/identicorp/node/getNodesSelect')
const changeNodeParent = require('../../../../src/identicorp/node/changeNodeParent')
const setNodes = require('../../../../src/identicorp/node/setNodes')

test('#changeNodeParent/', async (t) => {
  await start()
  flush()
  const db = 'shared'
  await truncate({
    db,
    tables: ['company', 'tree', 'node', 'app', 'app_company', 'status']
  })
  await createAppPrivTree()
  await createNodes()
  await setNodes()
  await changeNodeParent({
    parent: 3,
    id: 7
  })
  const nodeHash = await getNodesSelect()
  const parsed = JSON.parse(nodeHash)
  const compareTo = {
    id: 3,
    direct:
      [{ id: 4, name: 'santiago' },
      { id: 5, name: 'melipilla' },
      { id: 7, name: 'valparaiso' }]
  }
  t.deepEqual(parsed['3'], compareTo)
})

test('#createNode error noUpdateID', async (t) => {
  await start()
  flush()
  const db = 'shared'
  await truncate({
    db,
    tables: ['company', 'tree', 'node', 'app', 'app_company', 'status']
  })
  await createAppPrivTree()
  await createNodes()
  await setNodes()
  const error = await t.throws(changeNodeParent({
    parent: 30,
    id: 7
  })
  )
  t.is(error.message, 'cant find parent node, parent: 30, node: 7')
})
