require('dotenv').config()
const test = require('ava')
const { truncate, createNodes, createAppPrivTree } = require('../../helpers')
const flush = require('../../../../src/redisUtils/flush')
const start = require('../../../../src/serviceStarter/tester')
const getNodesSelect = require('../../../../src/identicorp/node/getNodesSelect')
const changeNodeName = require('../../../../src/identicorp/node/changeNodeName')
const setNodes = require('../../../../src/identicorp/node/setNodes')

test('#changeNodeName/', async (t) => {
  await start()
  flush()
  const db = 'shared'
  await truncate({
    db,
    tables: ['company', 'tree', 'node', 'app', 'app_company', 'status']
  })
  await createAppPrivTree()
  await createNodes()
  await setNodes()
  await changeNodeName({
    name: 'metropolitana',
    id: 3
  })
  const nodeHash = await getNodesSelect()
  const parsed = JSON.parse(nodeHash)
  const compareTo = {
    id: 1,
    direct: [{ id: 3, name: 'metropolitana' }, { id: 6, name: 'v' }]
  }
  const compareTo2 = {
    id: 13,
    direct:
      [{ id: 14, name: 'vigilancia' },
      { id: 15, name: 'medioambiente' }]
  }
  t.deepEqual(parsed['1'], compareTo)
  t.deepEqual(parsed['13'], compareTo2)
})

test('#changeNodeName noUpdateID', async (t) => {
  await start()
  flush()
  const db = 'shared'
  await truncate({
    db,
    tables: ['company', 'tree', 'node', 'app', 'app_company', 'status']
  })
  await createAppPrivTree()
  await createNodes()
  await setNodes()
  const error = await t.throws(changeNodeName({
    name: 'metropolitana',
    id: 35
  })
  )
  t.is(error.message, 'error updating node Ref does not exist')
})
