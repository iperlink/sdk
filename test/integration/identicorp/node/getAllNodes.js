require('dotenv').config()
const test = require('ava')
const { truncate, createNodes, createAppPrivTree } = require('../../helpers')
const getAllNodes = require('../../../../src/identicorp/node/getAllNodes')
const setNodes = require('../../../../src/identicorp/node/setNodes')
const flush = require('../../../../src/redisUtils/flush')
const start = require('../../../../src/serviceStarter/tester')

test('#getNodes/', async (t) => {
  await start()
  flush()
  const db = 'shared'
  await truncate({
    db,
    tables: ['company', 'tree', 'node', 'app', 'app_company']
  })
  await createAppPrivTree()
  await createNodes()
  await setNodes()
  const nodes = await getAllNodes()
  const compareTo = {
    "1": [
      1,
      3,
      6,
      4,
      5,
      7,
      8,
      9,
      10,
      11
    ],
    "2": [
      2,
      12,
      13,
      14,
      15
    ],
    "3": [
      3,
      4,
      5,
      10,
      11
    ],
    "4": [
      4,
      10,
      11
    ],
    "5": [
      5
    ],
    "6": [
      6,
      7,
      8,
      9
    ],
    "7": [
      7,
      8,
      9
    ],
    "8": [
      8
    ],
    "9": [
      9
    ],
    "10": [
      10
    ],
    "11": [
      11
    ],
    "12": [
      12
    ],
    "13": [
      13,
      14,
      15
    ],
    "14": [
      14
    ],
    "15": [
      15
    ]
  }


  t.deepEqual(nodes, compareTo)
})

