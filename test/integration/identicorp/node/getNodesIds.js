require('dotenv').config()
const test = require('ava')
const { truncate, createNodes, createAppPrivTree } = require('../../helpers')
const getNodesIds = require('../../../../src/identicorp/node/getNodesIds')
const setNodes = require('../../../../src/identicorp/node/setNodes')
const flush = require('../../../../src/redisUtils/flush')
const start = require('../../../../src/serviceStarter/tester')


test('#getNodesIDs/', async (t) => {
  await start()
  flush()
  const db = 'shared'
  await truncate({
    db,
    tables: ['company', 'tree', 'node', 'app', 'app_company']
  })
  await createAppPrivTree()
  await createNodes()
  await setNodes()
  const nodesId = await getNodesIds([3, 13])
  const compareTo = [3, 4, 5, 10, 11, 13, 14, 15]
  t.deepEqual(compareTo, nodesId)
})

