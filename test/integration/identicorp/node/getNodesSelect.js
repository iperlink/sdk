require('dotenv').config()
const test = require('ava')
const { truncate, createNodes, createAppPrivTree } = require('../../helpers')
const getNodesSelect = require('../../../../src/identicorp/node/getNodesSelect')
const setNodes = require('../../../../src/identicorp/node/setNodes')
const flush = require('../../../../src/redisUtils/flush')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')

test('#getNodesIDs/', async (t) => {
  await start()
  const { redis } = await getServices()
  flush()
  const db = 'shared'
  await truncate({
    db,
    tables: ['company', 'tree', 'node', 'app', 'app_company']
  })
  await createAppPrivTree()
  await createNodes()
  await setNodes() // this is the function
  const nodeHash = await getNodesSelect()
  const parsed = JSON.parse(nodeHash)
  const compareTo = {
    id: 1,
    direct: [{ id: 3, name: 'rm' }, { id: 6, name: 'v' }]
  }
  const compareTo2 = {
    id: 13,
    direct:
      [{ id: 14, name: 'vigilancia' },
      { id: 15, name: 'medioambiente' }]
  }
  t.deepEqual(parsed['1'], compareTo)
  t.deepEqual(parsed['13'], compareTo2)
})
