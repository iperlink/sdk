require('dotenv').config()
const test = require('ava')
const { truncate, createNodes, createAppPrivTree } = require('../../../helpers')
const start = require('../../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../../src/serviceStarter')
const flush = require('../../../../../src/redisUtils/flush')
const setNodes = require('../../../../../src/identicorp/node/setNodes')

test('#setNodes/', async (t) => {
  await start()
  const { redis } = await getServices()
  flush()
  const db = 'shared'
  await truncate({
    db,
    tables: ['company', 'tree', 'node', 'app', 'app_company']
  })
  await createAppPrivTree()
  await createNodes()
  await setNodes() // this is the function

  const hashNode = await redis.hget('hash_node_id', '3')
  const nodeHash = await redis.get('node_structure')
  const parsed = JSON.parse(nodeHash)
  const compareTo = {
    id: 1,
    direct: [{ id: 3, name: 'rm' }, { id: 6, name: 'v' }]
  }
  const compareTo2 = {
    id: 13,
    direct:
      [{ id: 14, name: 'vigilancia' },
      { id: 15, name: 'medioambiente' }]
  }
  const compareTo3 = [3, 4, 5, 10, 11]
  t.deepEqual(parsed['1'], compareTo)
  t.deepEqual(parsed['13'], compareTo2)
  t.deepEqual(JSON.parse(hashNode), compareTo3)
  t.pass()

})

