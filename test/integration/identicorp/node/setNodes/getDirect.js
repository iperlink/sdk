require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree, createNodes } = require('../../../helpers')
const start = require('../../../../../src/serviceStarter/tester')
const getDirect = require('../../../../../src/identicorp/node/setNodes/getDirect')

test('#getDirect/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company', 'node']
  })
  await createAppPrivTree()
  await createNodes()
  const desc = await getDirect(1)
  const ids = desc.map(d => d.id)
  t.deepEqual(ids, [3, 6])
})
