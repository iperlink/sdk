require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree, createNodes } = require('../../../helpers')
const start = require('../../../../../src/serviceStarter/tester')
const setNodeData = require('../../../../../src/identicorp/node/setNodes/setNodeData')
const flush = require('../../../../../src/redisUtils/flush')
const { getServices } = require('../../../../../src/serviceStarter')


test('#setNodeData/', async (t) => {
  await start()
  flush()
  const { redis } = await getServices()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company', 'node']
  })
  await createAppPrivTree()
  await createNodes()
  const res = { id: 1, name: 'chile', tree_id: 1 }
  const desc = await setNodeData(res)
  const hashNode = await redis.hget('hash_node_id', '1')
  const compareTo = '[1,3,6,4,5,7,8,9,10,11]'
  t.deepEqual(hashNode, compareTo)
})

