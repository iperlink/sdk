require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree, createNodes } = require('../../../helpers')
const start = require('../../../../../src/serviceStarter/tester')
const getDesc = require('../../../../../src/identicorp/node/setNodes/getDesc')

test('#getDesc/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app', 'privilege', 'company', 'tree', 'privilege_tree', 'app_company', 'node']
  })
  await createAppPrivTree()
  await createNodes()
  const res = { id: 1, name: 'chile', tree_id: 1 }
  const desc = await getDesc(res)
  const ids = desc.map(d => d.id)
  t.deepEqual([1, 3, 6, 4, 5, 7, 8, 9, 10, 11], ids)
})

