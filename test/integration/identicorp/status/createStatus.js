require('dotenv').config()
const test = require('ava')
const {
  truncate
} = require('../../../../src/mysqlUtils')
const start = require('../../../../src/serviceStarter/tester')
const createStatus = require('../../../../src/identicorp/status/createStatus')

test('#createStatus/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['status']
  })
  const status = [{ name: 'active' }, { name: 'cancelled' }, { name: 'terminated' }]
  const created = await createStatus(status)
  t.is(created, 3)
})
