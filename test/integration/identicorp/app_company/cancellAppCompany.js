require('dotenv').config()
const test = require('ava')
const { truncate, createAppCompany, createStatus } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')
const cancellAppCompany = require('../../../../src/identicorp/app_company/cancellAppCompany')

test('#cancellAppCompany error/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app_company', 'app', 'company', 'status']
  })
  await createAppCompany()
  await createStatus(['active', 'cancelled', 'terminated'])
  const error = await t.throws(cancellAppCompany({ company_id: 2, app_id: 1 }))
  t.is(error.message, 'error updating app_company Ref does not exist')
})

test('#cancellAppCompany/', async (t) => {
  await start()
  const { mysql } = getServices()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app_company', 'app', 'company', 'status']
  })
  await createAppCompany()
  await createStatus(['active', 'cancelled', 'terminated'])
  const update = await cancellAppCompany({ company_id: 1, app_id: 1, status: 2 })
  const [updated] = await mysql.execute('SELECT status from shared.app_company where id =1')
  t.is(updated[0].status, 2)
  t.is(update, 1)
})