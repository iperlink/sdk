require('dotenv').config()
const test = require('ava')
const { truncate, createApp, createCompany } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const createAppCompany = require('../../../../src/identicorp/app_company/createAppCompany')

test('#createAppCompany create with existant company/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app_company', 'app', 'company']
  })
  await createCompany()
  await createApp()
  const company = {
    name: 'new.co; truncate table shared.app',
    rut: '69.999.666.0',
    status: 1
  }
  const created = await createAppCompany({ company, app: 1 })
  t.is(created.id, 1)
})

test('#createAppCompany create with new company/', async (t) => {
  await start()
  const company = {
    name: 'new.co',
    rut: '69.999.666-1'
  }
  const db = 'shared'
  await truncate({
    db,
    tables: ['app_company', 'app', 'company']
  })
  await createApp()
  const created = await createAppCompany({ company, app: 1 })
  t.is(created.id, 1)
})

test('#createAppCompany error/', async (t) => {
  await start(['mysql'])
  const company = {
    name: 'new.co',
    rut: '69.999.666-1'
  }
  const db = 'shared'
  await truncate({
    db,
    tables: ['app_company', 'app', 'company']
  })
  const error = await t.throws(createAppCompany({ company, app: 1 }))
  t.is(error.message, 'error creating app_company noRef')
})