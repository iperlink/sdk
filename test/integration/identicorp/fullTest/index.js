require('dotenv').config()
const test = require('ava')
const { truncate, createAppPrivTree, createNodes } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const createUser = require('../../../../src/identicorp/user/createUser')
const getUserNodes = require('../../../../src/identicorp/user/getUserNodes')
const flush = require('../../../../src/redisUtils/flush')
const createNode = require('../../../../src/identicorp/node/createNode')
const setNodes = require('../../../../src/identicorp/node/setNodes')
const insertContent = require('../../../../src/identicorp/content/replaceContent')
const getContent = require('../../../../src/identicorp/content/getContent')
const deleteCognitoUser = require('../../../../src/identicorp/user/deleteCognito')

test('#fullTest/', async (t) => {
  await start({})
  flush()
  const db = 'shared'
  await truncate({
    db,
    tables: [
      'app',
      'app_company',
      'company',
      'node',
      'node_user_privilege',
      'privilege',
      'privilege_tree',
      'tree',
      'status',
      'user',
    ]
  })
  await createAppPrivTree()

  await setNodes()
  const node = { tree_id: 1, name: 'chile' }
  const node2 = { tree_id: 2, name: 'gerencia general' }
  await createNode(node)
  await createNode(node2)

  const user = {
    name: 'test',
    lastname: 'user',
    email: 'test@test.com',
    company_id: 1
  }
  const privileges = [
    { privilege_id: 1, node_ids: [1, 2] },
    { privilege_id: 2, node_ids: [1, 2] },
  ]
  const nodes = [1, 2]
  await deleteCognitoUser(user)
  const res = await createUser({ user, privileges, nodes })

  const content_node = {
    privilege_id: 1,
    node_id: '9,12',
    content_id: 2
  }
  process.env.DB_NAME = 'tester'
  await multiInsert({
    table: 'child',
    db: 'tester',
    values: [
      { name: 'test' },
      { name: 'test2' },
      { name: 'test3' },
      { name: 'test4' },
    ]
  })
  await insertContent(content_node)
  console.time('tesxt')
  const userNodes = await getUserNodes({ userEmail: 'test@test.com', privilege_id: 1 })
  const filtered = await getContent({ userNodes, db: 'tester', privilegeId: 1 })
  const found = await find({
    table: 'child',
    filtered,
    db: 'tester'
  })
  console.timeEnd('tesxt')
  console.log(found)
  t.deepEqual(filtered, [2])
  t.is(found[0].id, 2)
  t.is(found.length, 1)
})
