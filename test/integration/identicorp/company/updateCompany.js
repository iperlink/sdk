require('dotenv').config()
const test = require('ava')
const { truncate, createCompany } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const { getServices } = require('../../../../src/serviceStarter')
const updateCompany = require('../../../../src/identicorp/company/updateCompany')

test('#updateCompany error/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['company']
  })
  await createCompany()
  const updatedData = {
    name: 'new.c2',
    rut: '69.999.666.0'
  }
  const error = await t.throws(updateCompany({ company: updatedData, id: 20 }))
  t.is(error.message, 'error updating company Ref does not exist')
})

test('#updateCompany/', async (t) => {
  await start()
  const { mysql } = getServices()
  const db = 'shared'
  await truncate({
    db,
    tables: ['company']
  })
  await createCompany()
  const updatedData = {
    name: 'new.c2',
    rut: '69.999.666.0'
  }
  const updated = await updateCompany({ company: updatedData, id: 1 })
  const readQuery = `SELECT name FROM shared.company where id = 1`
  const [companies] = await mysql.execute(readQuery)
  const res = companies[0].name
  t.is(updated, 1)
  t.is(res, 'new.c2')
})
