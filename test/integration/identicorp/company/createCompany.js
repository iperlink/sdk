require('dotenv').config()
const test = require('ava')
const { truncate } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const createCompany = require('../../../../src/identicorp/company/createCompany')

test('#createCompany success/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app_company', 'app', 'company']
  })
  const company = {
    name: 'new.co; truncate table shared.app',
    rut: '69.999.666.0',
    status: 1
  }
  const created = await createCompany(company)
  t.is(created.id, 1)
})


test('#createCompany duplicate/', async (t) => {
  await start()
  const company = {
    name: 'new.co',
    rut: '69.999.666.0'
  }
  const db = 'shared'
  await truncate({
    db,
    tables: ['company']
  })
  await createCompany(company)
  const error = await t.throws(createCompany(company))
  t.is(error.message, 'error creating company duplicateEntry')
})