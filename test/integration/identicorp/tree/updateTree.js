require('dotenv').config()
const test = require('ava')
const { truncate, createCompany } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const createTree = require('../../../../src/identicorp/tree/createTree')
const updateTree = require('../../../../src/identicorp/tree/updateTree')

test('#updateTree error/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['tree', 'company']
  })
  await createCompany()
  const tree = { name: 'centro de costos', description: 'create dat', company_id: '1' }
  await createTree(tree)
  const error = await t.throws(updateTree({ tree: { name: 'updated tree' }, id: 20 }))
  t.is(error.message, 'error updating tree Ref does not exist')
})

test('#updateTree ', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['tree', 'company']
  })
  await createCompany()
  const tree = { name: 'centro de costos', description: 'create dat', company_id: '1' }
  await createTree(tree)
  const updatedData = { name: 'updated tree' }
  const updated = await updateTree({ tree: updatedData, id: 1 })
  t.is(updated, 1)
})
