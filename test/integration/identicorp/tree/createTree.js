require('dotenv').config()
const test = require('ava')
const { truncate, createCompany } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const createTree = require('../../../../src/identicorp/tree/createTree')

test('#createTree/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['tree', 'company']
  })
  await createCompany()
  const tree = { name: 'centro de costos', description: 'create dat', company_id: '1' }
  const created = await createTree(tree)
  t.is(created.id, 1)
})

test('#createTree error missing ref', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['tree', 'company']
  })
  const tree = { name: 'centro de costos', description: 'create dat', company_id: '1' }
  const error = await t.throws(createTree(tree))
  t.is(error.message, 'error creating tree noRef')
})

test('#createTree duplicate/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['tree', 'company']
  })
  await createCompany()
  const tree = { name: 'centro de costos', description: 'create dat', company_id: '1' }
  await createTree(tree)
  const error = await t.throws(createTree(tree))
  t.is(error.message, 'error creating tree duplicateEntry')
})
