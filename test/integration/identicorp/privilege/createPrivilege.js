require('dotenv').config()
const test = require('ava')
const { truncate, createApp } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const createPrivilege = require('../../../../src/identicorp/privileges/createPrivilege')


test('#createPrivilege/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app', 'privilege']
  })
  await createApp()
  const res = await createPrivilege({
    name: 'create data',
    description: 'create dat',
    app_id: '1'
  })
  t.is(res.id, 1)
})

test('#createPrivilege error missing ref', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app', 'privilege']
  })
  const error = await t.throws(createPrivilege({
    name: 'create data',
    description: 'create dat',
    app_id: '1'
  }))
  t.is(error.message, 'error creating privilege noRef')
})

test('#createPrivilege duplicate/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app', 'privilege']
  })
  await createApp()
  const privilege = { name: 'create data', description: 'create dat', app_id: '1' }
  await createPrivilege(privilege)
  const error = await t.throws(createPrivilege(privilege))
  t.is(error.message, 'error creating privilege duplicateEntry')
})
