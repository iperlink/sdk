require('dotenv').config()
const test = require('ava')
const {
  find
} = require('../../../../src/mysqlUtils')
const { truncate, createApp } = require('../../helpers')
const start = require('../../../../src/serviceStarter/tester')
const createPrivilege = require('../../../../src/identicorp/privileges/createPrivilege')
const updatePrivilege = require('../../../../src/identicorp/privileges/updatePrivilege')

test('#updatePrivilege error/', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app', 'privilege']
  })
  await createApp()
  const privilege = { name: 'create data', description: 'create dat', app_id: '1' }
  await createPrivilege(privilege)
  const updatedData = { name: 'updated data2' }
  const error = await t.throws(updatePrivilege({ privilege: updatedData, id: 20 }))
  t.is(error.message, `error updating privilege Ref does not exist`)
})

test('#updatePrivilege ', async (t) => {
  await start()
  const db = 'shared'
  await truncate({
    db,
    tables: ['app', 'privilege']
  })
  await createApp()
  const privilege = { name: 'create data', description: 'create dat', app_id: '1' }
  await createPrivilege(privilege)
  const updatedData = { name: 'updated data2' }
  const updated = await updatePrivilege({ privilege: updatedData, id: 1 })
  const priv = await find({ db: 'shared', table: 'privilege' })
  const res = priv[0].name
  t.is(res, 'updated data2')
  t.is(updated, 1)
})
