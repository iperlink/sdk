require('dotenv').config()
const test = require('ava')
const uploadWistia = require('../../../src/wistia/upload')

test('#wistia/upload', async (t) => {
  const data = await uploadWistia('https://s3.amazonaws.com/paneldecontrolcl/004+Part+1+-+Architecture+Discussion+and+Lab+Preparation.mp4')
  console.log(data)
  t.pass()
})
