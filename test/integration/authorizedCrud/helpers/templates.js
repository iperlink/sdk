module.exports = {
  main: {
    queryParams: {
      id: 'exact',
      name: 'exact',
      createdAt: 'dateBetween'
    },
    table: 'child_view'
  }
}