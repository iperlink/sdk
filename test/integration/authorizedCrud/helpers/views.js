const parent_view = {
  tables: {
    child: { name: 'child' },
    parent: { name: 'parent', ref: ' child.parent_id ' }
  },
  query: ({ tables, db, where = '' }) => `
  SELECT
    ${db}.parent.id,
    ${db}.parent.name as parent,
    ${db}.child.name,
    ${db}.child.tags,
    ${db}.child.created_at
    FROM ${db}.${tables.child.name}
    inner join ${db}.${tables.parent.name} on ${db}.${tables.parent.name}.id = ${db}.${tables.parent.ref}
    ${where}
  `
}

const child_view = {
  tables: {
    child: { name: 'child', ref: 'child.id' },
  },
  tags: ['name'],
  query: ({ tables, db, where = '' }) => `
  SELECT
    ${db}.child.id,
    ${db}.child.name,
    ${db}.child.created_at,
    ${db}.child.name as tags
    FROM ${db}.${tables.child.name}
    ${where}
  `
}


module.exports = {
  parent_view,
  child_view
}