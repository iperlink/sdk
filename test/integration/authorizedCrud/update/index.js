require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const {
  insert,
  truncate,
  find
} = require('../../../../src/mysqlUtils')
const views = require('../helpers/views')
const setTableRef = require('../../../../src/materialize/helpers/setTableRef')
const authInsert = require('../../../../src/authorizedCrud/insert')
const authUpdate = require('../../../../src/authorizedCrud/update')
const deleteCognitoUser = require('../../../../src/identicorp/user/deleteCognito')


test('#authorizedCrud/Update', async (t) => {
  const parent = {
    name: 'parent from full test'
  }
  const parent2 = {
    name: 'parent2'
  }
  const child = {
    name: 'child1 from full test',
    parent_id: 1
  }
  const child2 = {
    name: 'child updated from full test',
    parent_id: 1
  }
  await start({ views,findTemplates })
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'user',
      'node_user_privilege',
      'parent',
      'child'
    ]
  })
  await fullIdenticorp()

  await setTableRef({ views, db })
  await insert({ db, table: 'parent', values: parent })
  await insert({ db, table: 'parent', values: parent2 })
  await authInsert({
    db,
    table: 'child',
    values: child,
    views,
    contentPrivilege: 2,
    insertPrivilege: 1,
    nodeId: '2',
    userEmail: user.email
  })
  await authUpdate({
    db,
    table: 'child',
    values: child2,
    views,
    insertPrivilege: 1,
    userEmail: user.email,
    id: 1
  })
  const foundChild = await find({
    db,
    table: 'child'
  })
  const foundMaterialized = await find({
    db: 'tester_materialized',
    table: 'parentView'
  })
  const foundShared = await find({
    db: 'tester',
    table: 'content_node'
  })
  t.is(foundChild[0].name, 'child updated from full test')
  t.is(foundMaterialized[0].name, 'child updated from full test')
  t.is(foundMaterialized[0].parent, 'parent from full test')
  t.is(foundShared[0].content_id, 1)
  t.is(foundShared[0].node_id, 2)
  t.pass()
})

test.only('#authorizedCrud/Update with node', async (t) => {
  const parent = {
    name: 'parent from full test'
  }
  const parent2 = {
    name: 'parent2'
  }
  const child = {
    name: 'child1 from full test',
    parent_id: 1
  }
  const child2 = {
    name: 'child updated from full test',
    parent_id: 1
  }
  await start({ views,findTemplates })
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'user',
      'node_user_privilege',
      'parent',
      'child'
    ]
  })
  await fullIdenticorp()
  await insert({ db, table: 'parent', values: parent })
  await insert({ db, table: 'parent', values: parent2 })
  await setTableRef({ views, db })
  await authInsert({
    db,
    table: 'child',
    values: child,
    views,
    contentPrivilege: 2,
    insertPrivilege: 1,
    nodeId: '2,20',
    userEmail: user.email
  })
  await authUpdate({
    db,
    table: 'child',
    values: child2,
    views,
    insertPrivilege: 1,
    userEmail: user.email,
    insertPrivilege: 1,
    nodeId: '8,20',
    id: 1
  })
  const foundChild = await find({
    db,
    table: 'child'
  })
  const foundMaterialized = await find({
    db: 'tester_materialized',
    table: 'parentView'
  })
  const foundShared = await find({
    db: 'tester',
    table: 'content_node'
  })
  t.is(foundChild[0].name, 'child updated from full test')
  t.is(foundMaterialized[0].name, 'child updated from full test')
  t.is(foundMaterialized[0].parent, 'parent from full test')
  t.is(foundShared[0].content_id, 1)
  t.is(foundShared[0].node_id, 8)
  t.pass()
})
