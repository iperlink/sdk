require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate, fullIdenticorp } = require('../../helpers')
const {
  insert,
  find
} = require('../../../../src/mysqlUtils')
const views = require('../helpers/views')
const findTemplates = require('../helpers/templates')
const setTableRef = require('../../../../src/materialize/helpers/setTableRef')
const authInsert = require('../../../../src/authorizedCrud/insert')

test('#authorizedCrud/Insert', async (t) => {
  await start({ views,findTemplates })
  await truncate({
    db,
    tables: [
      'app',
      'privilege',
      'company',
      'tree',
      'privilege_tree',
      'app_company',
      'node',
      'user',
      'node_user_privilege',
      'parent',
      'child'
    ]
  })
  await fullIdenticorp()
  await insert({ db, table: 'parent', values: parent })
  await insert({ db, table: 'parent', values: parent2 })
  await setTableRef({ views, db })
  await authInsert({
    db,
    table: 'child',
    values: child,
    views,
    contentPrivilege: 2,
    insertPrivilege: 1,
    nodeId: '2',
    userEmail: user.email
  })
  const foundChild = await find({
    db,
    table: 'child'
  })
  const foundMaterialized = await find({
    db: 'tester_materialized',
    table: 'parentView'
  })
  const foundShared = await find({
    db: 'tester',
    table: 'content_node'
  })
  t.is(foundChild[0].name, 'child1 from full test')
  t.is(foundMaterialized[0].name, 'child1 from full test')
  t.is(foundMaterialized[0].parent, 'parent from full test')
  t.is(foundShared[0].content_id, 1)
  t.is(foundShared[0].node_id, 2)
  t.pass()
})
