require('dotenv').config()
const test = require('ava')
const start = require('../../../../src/serviceStarter/tester')
const { truncate, fullIdenticorp } = require('../../helpers')
const {
  insert,
} = require('../../../../src/mysqlUtils')
const views = require('../helpers/views')
const setTableRef = require('../../../../src/materialize/helpers/setTableRef')
const authFind = require('../../../../src/authorizedCrud/find')
const authInsert = require('../../../../src/authorizedCrud/insert')
const findTemplates = require('../helpers/templates')



test('#authorizedCrud/Find', async (t) => {
  await start({ views, findTemplates })
  const db2 = 'shared'
  await truncate({
    db: db2,
    tables: [
      'app',
      'app_company',
      'company',
      'node',
      'node_user_privilege',
      'privilege',
      'privilege_tree',
      'tree',
      'status',
      'user',
    ]
  })
  await fullIdenticorp()
  const parent = {
    name: 'parent from full test'
  }
  const parent2 = {
    name: 'parent2'
  }
  const child = {
    name: 'child1 from full test',
    parent_id: 1,
    tags: 'tag1,tag2'
  }

  const db = 'tester'
  process.env.db = db
  await truncate({
    db,
    tables: ['parent', 'child']
  })
  await truncate({
    db: 'tester_materialized',
    tables: ['child_view', 'parent_view']
  })

  await insert({ db, table: 'parent', values: parent })
  await insert({ db, table: 'parent', values: parent2 })
  await setTableRef({ views, db })
  const user = { email: 'test@test.com' }
  const inserted = await authInsert({
    table: 'child',
    values: child,
    insertPrivilege: 1,
    nodeId: '12,2', // nodes must belong to user nodes = [ 2, 12, 13, 14, 15, 11 ]
    userName: user.email
  })
  const queryObj = {
    id: [1],
    createdAt: { begin: '2018-01-01', end: '2019-12-21' }
  }
  const found = await authFind({
    db,
    templateName: 'main',
    queryObj,
    privilegeId: 2,
    userName: user.email,
    view: 'parent_view',
  })
  console.log(found)
  process.exit()
  t.is(found[0].name, 'child1 from full test')
  t.pass()
})
